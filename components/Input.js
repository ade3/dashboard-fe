import PropTypes from 'prop-types'
export default function Input(props) {
  const {
    size = 'm'
  } = props
  let padding, fontSize = ''

  if (size === 'm') {
    padding = '8px 12px'
    fontSize = 16
  } else if (size === 's') {
    padding = '6px 12px'
    fontSize = 14
  }

  return (
    <input
      {...props}
      className={['border border-gray-300', props.className || ''].join(' ')}
      style={{
        ...props.style || {},
        borderRadius: 4,
        padding, fontSize
      }}
    />
  )
}
Input.propTypes = {
  size: PropTypes.oneOf(['m', 's'])
}
