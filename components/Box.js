export default function (props) {
  return (
    <div
      {...props}
      style={{
        ...props.style || {},
        height: props.height || 150,
        width: props.width || 150
      }}
    >{props.children}</div>
  )
}
