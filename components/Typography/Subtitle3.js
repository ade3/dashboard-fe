export default function (props) {
  return (
    <div
      className={['', props.className].join(' ')}
      style={{
        ...props.style,
        fontSize: 16,
        fontWeight: 'normal',
        letterSpacing: 0.15
      }}
    >
      {props.children}
    </div>
  )
}
