export default function (props) {
  return (
    <div
      className={['', props.className].join(' ')}
      style={{
        ...props.style,
        fontSize: 14,
        fontWeight: 500,
        letterSpacing: 0.1
      }}
    >
      {props.children}
    </div>
  )
}
