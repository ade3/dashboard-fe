export default function (props) {
  return (
    <div
      className={['', props.className].join(' ')}
      style={{
        ...props.style,
        fontSize: 15,
        fontWeight: 400,
        letterSpacing: 0.25
      }}
    >
      {props.children}
    </div>
  )
}
