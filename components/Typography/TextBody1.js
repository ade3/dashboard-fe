export default function (props) {
  return (
    <div
      className={['', props.className].join(' ')}
      style={{
        ...props.style,
        fontSize: 16,
        fontWeight: 400,
        letterSpacing: 0.5
      }}
    >
      {props.children}
    </div>
  )
}
