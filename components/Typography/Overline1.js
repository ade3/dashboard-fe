export default function (props) {
  return (
    <div
      className={['', props.className].join(' ')}
      style={{
        ...props.style,
        fontSize: 10
      }}
    >
      {props.children}
    </div>
  )
}
