export default function (props) {
  return (
    <div
      className={['', props.className].join(' ')}
      style={{
        ...props.style,
        fontSize: 15,
        fontWeight: 'normal',
        letterSpacing: 1.25
      }}
    >
      {props.children}
    </div>
  )
}
