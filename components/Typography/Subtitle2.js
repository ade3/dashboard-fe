export default function (props) {
  return (
    <div
      className={['', props.className].join(' ')}
      style={{
        ...props.style,
        fontSize: 18,
        fontWeight: 'bold',
        letterSpacing: 0.15
      }}
    >
      {props.children}
    </div>
  )
}
