export default function (props) {
  return (
    <div
      className={['', props.className].join(' ')}
      style={{
        ...props.style,
        fontSize: 12,
        fontWeight: 400,
        letterSpacing: 0.4
      }}
    >
      {props.children}
    </div>
  )
}
