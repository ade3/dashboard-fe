export default function (props) {
  return (
    <div
      className={['', props.className].join(' ')}
      style={{
        ...props.style,
        fontSize: 16,
        fontWeight: 700,
        letterSpacing: 1.25
      }}
    >
      {props.children}
    </div>
  )
}
