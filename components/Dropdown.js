import PropTypes from 'prop-types' 
import { useState, useRef, useEffect } from 'react'

const Dropdown = ({ ...props }) => {
  const [active, setActive] = useState(false)

  const useOutsideAlerter = (ref) => {
    useEffect(() => {
      function handleClickOutside(event) {
        if (ref.current && !ref.current.contains(event.target)) {
          setActive(false)
        }
      }
     
      document.addEventListener('mousedown', handleClickOutside)
      return () => { 
        document.removeEventListener('mousedown', handleClickOutside)
      }
    }, [ref])
  }

  const wrapperRef = useRef(null)
  useOutsideAlerter(wrapperRef)

  return (
    <div className={`dropdown ${props.className}`} ref={wrapperRef}>
      <div onClick={(e) => setActive(!active)}>{props.title}</div>

      <div className={`mt-2 dropdown-menu dropdown-left dropdown-right ${active ? 'show': ''}`} onClick={(e) => setActive(false)}>
        {props.children}
      </div>
    </div>
  ) 
}

Dropdown.defaultProps = { 
  className: ''
}

Dropdown.propTypes = {    
  className: PropTypes.string
}

export default Dropdown
