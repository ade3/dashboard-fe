export default function Middle(props) {
  const _className = ['flex items-center justify-center']
  if (props.className) {
    _className.push(...String(props.className).split(' '))
  }

  return (
    <div
      {...props}
      className={_className.join(' ')}
    >
      {props.children}
    </div>
  )
}
