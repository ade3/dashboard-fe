import PropTypes from 'prop-types'
import TextButton1 from './Typography/TextButton1'
import TextButton2 from './Typography/TextButton2'
import TextButton3 from './Typography/TextButton3'

export default function Button(props) {
  const {
    size = 'm',
    color = 'primary',
    rounded = false,
    isDelete = false
  } = props
  let classColor = ''
  let padding, borderRadius = ''

  if (!isDelete) {
    if (color === 'primary') {
      classColor = 'bg-blue text-white hover:bg-blue-navy2 disabled:bg-blue-disabled'
    } else if (color === 'secondary') {
      classColor = 'bg-white text-blue border border-blue hover:bg-blue-light2'
    } else if (color === 'tertiary') {
      classColor = 'bg-white text-blue-navy3 hover:bg-gray-200'
    }
  } else if (color === 'primary') {
    classColor = 'bg-red-700 text-white hover:bg-red-800 disabled:bg-red-200'
  } else if (color === 'secondary') {
    classColor = 'bg-white text-red-700 border border-red-500 hover:bg-red-100'
  } else if (color === 'tertiary') {
    classColor = 'bg-white text-red-700 hover:bg-gray-200'
  } 

  if (size === 'xl') {
    padding = 18
  } else if (size === 'l') {
    padding = 12
  } else if (size === 'm') {
    padding = 8
  } else if (size === 's') {
    padding = 7
  } else if (size === 'xs') {
    padding = 4
  }

  if(rounded) {
    borderRadius = '4px'
  }

  const className = [classColor, props.className, 'w-full' || ''].join(' ')
  const style = {
    ...props.style || {},
    padding,
    borderRadius
  }
  // console.log(className)
  return (
    <button
      {...props}
      style={style}
      className={className}
    >
      {(() => {
        if (size === 'xl') {
          return (
            <TextButton1
              // {...props}
            >
              {props.children}
            </TextButton1>
          )
        } if (size === 'l') {
          return (
            <TextButton1
              // {...props}
            >
              {props.children}
            </TextButton1>
          )
        } if (size === 's') {
          return (
            <TextButton3
              // {...props}
            >
              {props.children}
            </TextButton3>
          )
        } if (size === 'xs') {
          return (
            <TextButton3
              // {...props}
            >
              {props.children}
            </TextButton3>
          )
        } if (size === 'm') {
          return (
            <TextButton2
              // {...props}
            >
              {props.children}
            </TextButton2>
          )
        }
      })()}
    </button>
  )
}

Button.propTypes = {
  color: PropTypes.oneOf(['primary', 'secondary', 'tertiary']),
  size: PropTypes.oneOf(['l', 'xl', 'm', 's', 'xs']),
}
