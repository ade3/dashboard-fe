export default function Dot(props) {
  return (
    <div
      className={'bg-orange rounded-full'}
      style={{
        height: 8,
        width: 8
      }}
    />
  )
}
