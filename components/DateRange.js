import DatePicker from 'react-datepicker'
import { useState } from 'react'

import styles from '../styles/components/datepicker.module.css'
import 'react-datepicker/dist/react-datepicker.min.css'

// callback get value
export default function (props) {
  const { callback = () => {} } = props
  const [startDate, setStartDate] = useState(new Date())
  const [endDate, setEndDate] = useState(null)
  const onChange = (dates) => {
    // sometimes the default datepicker is behind 1 day -_-
    if (props.isRange) {
      const [start, end] = dates
      callback(start, end)
      setStartDate(start)
      setEndDate(end)
    } else {
      console.log(dates)
      callback(dates)
      setStartDate(dates)
    }
  }

  return (
    <>
      <div className={`relative`}>
        <DatePicker
            {...props}
          selected={startDate}
          onChange={onChange}
          startDate={startDate}
          endDate={endDate}
          selectsRange={props.isRange}
          className={styles.customInput}
        />
        <div className={`absolute ${styles.iconCalendar}`}>
          <i className={'fa fa-calendar-o'} />
        </div>
      </div>
    </>
  )
}
