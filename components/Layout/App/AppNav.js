import styles from './app.module.css'
import Subtitle4 from '../../Typography/Subtitle4'
import menus from '../../../libs/menus'
import menusAdmin from '../../../libs/menus-admin'
import menusPm from '../../../libs/menus-pm'
import menusClient from '../../../libs/menus-client'
import Menu from '../../Menu'
const AppNav = ({ user, ...props }) => {
  if(user !== null) {
    return (
      user.role == 1 ? (
        <div className={`${styles.appNav}`}>
          {menus.map((item, index) => {
            return (<Menu {...item} key={index} />)
          })}
          <div className="">
            Footer
          </div>
        </div>
      ) : user.role == 2? (<div className={`${styles.appNav}`}>
          {menusAdmin.map((item, index) => {
            return (<Menu {...item} key={index} />)
          })}
          <div className="">
            Footer
          </div>
        </div>
      ) : user.role == 3? (<div className={`${styles.appNav}`}>
        {menusPm.map((item, index) => {
          return (<Menu {...item} key={index} />)
        })}
        <div className="">
          Footer
        </div>
      </div>
      ) : (<div className={`${styles.appNav}`}>
        {menusClient.map((item, index) => {
          return (<Menu {...item} key={index} />)
        })}
        <div className="">
          Footer
        </div>
      </div>
      )
    ) 
  } else {
    return (<div></div>)
  }
}

export default AppNav
