// import { useRouter } from 'next/router' 
import { useState, useEffect } from 'react'
import Head from 'next/head'
import styles from './app.module.css'
import AppHeader from './AppHeader'
import AppNav from './AppNav'
import AuthService from "../../../services/auth.service";

const AppLayout = (props) => {
  // const [isLoading, setIsLoading] = useState(true)
  // const router = useRouter()
  const [user, setUser] = useState("")
  useEffect(() => {
    setUser(AuthService.getCurrentUser())
  }, []);

  return (
    <>
      <Head>
        <title>Dashboard Project Badr Interactive</title>
      </Head> 

      <AppHeader user={user} />
        
      <div className={`${styles.appMain} relative`}>
        <AppNav user={user} />
        <div className={`${styles.mainContent}`}>
          <div className="container mx-auto p-2">
            <div style={{ marginBottom: 16 }} className="title p-2 static text-center">
              {props.pageTitle && <h5 className="">{props.pageTitle}</h5>}
            </div>
            {props.children}
          </div>
        </div>
      </div>
      <div className={`${styles.copyRight}`}>
        <div className="py-10 text-center" style={{fontSize:"12px"}}>&copy; 2022 PT Badr Interactive</div>
      </div>
    </>
  )
}

export default AppLayout
