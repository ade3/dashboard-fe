import React, {useEffect, useState} from 'react'
import {useRouter} from "next/router"
import Notifications from "react-notifications-menu";

// import useTranslation from '../../../utils/hooks/useTranslation'
import styles from './app.module.css'
import Button from '../../Button'
import Dropdown from '../../Dropdown'
import AuthService from "../../../services/auth.service";
import UserService from "../../../services/user.service";
import Link from 'next/link';

const AppHeader = ({user, ...props}) => {
    const constant = ''
    const router = useRouter()
    const [listNotifications, setListNotifications] = useState([])

    const handleLogout = (e) => {
        e.preventDefault();
        AuthService.logout();
        return router.push("/login");
    };

    useEffect(() => {
        const user = AuthService.getCurrentUser();
        UserService.getNotifications().then(
            (response) => {
                let list = []
                response.data.slice().reverse()
                    .forEach(function(notif) {
                        list.push({
                            id: notif.id,
                            image:
                                "https://cutshort-data.s3.amazonaws.com/cloudfront/public/companies/5809d1d8af3059ed5b346ed1/logo-1615367026425-logo-v6.png",
                            receivedTime: new Date(notif.createdAt).toLocaleDateString(),
                            message:
                                (
                                    <div className='flex'>
                                        <div>
                                            <p>{notif.message}</p>
                                        </div>
                                        <div>
                                            <img width={22} height={22}
                                                 src={'/icon/delete-icon.svg'}
                                                 onClick={() => {
                                                     setListNotifications((oldList) => oldList.filter((item) => item.id !== notif.id));
                                                     UserService.deleteNotificationById(notif.id)
                                                 }}
                                            />
                                        </div>

                                    </div>
                                )

                        })
                    });
                setListNotifications(list);
            },
            (error) => {
                const resMessage =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString();
            }
        );

    }, []);

    return (
        <div className={`px-6 ${styles.appHeader}`}>
            <div className={`${styles.appHeaderUser}`}>
                <Dropdown title={(
                    <button href="#" className="px-10 border-l-2 rounded-none border-solid border-opacity-25 p-4">
                        {user ? user.name : "username"}
                    </button>
                )}>
                    <Link href="/account-info">
                        <Button color="tertiary" size="m" block>
                            Information
                        </Button>
                    </Link>
                    <Button onClick={handleLogout} color="tertiary" size="m" block>
                        Logout
                    </Button>
                </Dropdown>
            </div>
            <div style={{ float: 'right'}}>
                  {/* Notification */}
                <div className='mt-3 mr-2'> 
                <Notifications
                    data={listNotifications}
                    header={{
                        title: "Notifications",
                        option: {
                            text: "Remove All",
                            onClick: () => {
                                setListNotifications([])
                                UserService.deleteNotifications()
                            }
                        }
                    }}
                    // cardOption={data => console.log(data.image)}
                    // markAsRead={(data) => {console.log(data);}}
                />
                </div>
            </div>
            <div className="flex flex-row">
                
                <div>
                    <div className="text-sm font-bold p-4" style={{color: '#787878'}}>Badr Interactive</div>
                </div>
            </div>
        </div>
    )
}

export default AppHeader
