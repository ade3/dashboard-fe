export default function (props) {
  return (
    <>
      <div
        className={'container mx-auto pb-28'}
      >
        {props.children}
      </div>
    </>
  )
}
