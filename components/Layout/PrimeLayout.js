import MainLayout from './MainLayout';
import Middle from '../Middle';
import Overline2 from '../Typography/Overline2';

export default function (props) {
  return (
    <>
      <div
        className={'background-blur'}
        style={{
          height: 300,
        }}
      >
        <Middle className={'h-full relative'}>
          <h3 className={'text-white'}>{props.title || ''}</h3>
          {!!props.subtitle && (
            <Overline2 className={'absolute right-4 bottom-4 text-white'}>{props.subtitle}</Overline2>
          )}
        </Middle>
      </div>
      <MainLayout>
        {props.children}
      </MainLayout>
    </>
  )
}
