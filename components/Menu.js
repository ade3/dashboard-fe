import styles from './Layout/App/app.module.css'
import Subtitle4 from './Typography/Subtitle4'
import { useState } from 'react'

const Link = (props) => {
  const {
    url,
    title,
    Icon
  } = props
  return (
    <a href={url} className={`w-full block ${styles.appNavItem}`}>
      {!!Icon ? <Icon /> : ''}
      <Subtitle4 className="inline-flex m-2">{title}</Subtitle4>
    </a>
  )
}
const ButtonParent = (props) => {
  const {
    handleOpenChild,
    Icon,
    title
  } = props
  return (
    <div onClick={handleOpenChild} className={`w-full block ${styles.appNavItem} ${styles.appNavItemButton}`}>
      {!!Icon ? <Icon /> : ''}
      <Subtitle4 className="inline-flex m-2">{title}</Subtitle4>
    </div>
  )
}

const Child = (props) => (
  <div style={{ paddingRight: 8, paddingLeft: 8 }} className={'w-full hover:bg-blue-navy hover:text-white text-gray-400'}>
    <Link {...props} />
  </div>
)

export default function (props) {
  const { isParent, child = [] } = props
  const [openChild, setOpenChild] = useState(false)

  const handleOpenChild = () => setOpenChild(!openChild)

  return (
    <>
      <div style={{ paddingRight: 8 }} className={'flex flex-row w-full hover:bg-blue-navy hover:text-white text-gray-400'}>
        <div className={'w-full'}>
          {isParent ? (
            <ButtonParent
              {...props}
              handleOpenChild={handleOpenChild}
            />
          ) : (
            <Link {...props} />
          )}
        </div>
        {isParent && (
          <div className={'text-gray-400 my-auto font-bold'}>
            {!openChild ? <i className={'fa fa-angle-down'} /> : <i className={'fa fa-angle-up'} />}
          </div>
        )}
      </div>
      {(isParent && openChild) && (child.map(item => (<Child {...item} />)))}
    </>
  )
}
