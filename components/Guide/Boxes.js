import Box from '../Box'

export default function (props) {
  let boxClass = 'rounded p-2 pt-6 text-black text-center'
  return (
    <>
      <h2 className="mb-2">Shadow</h2>
      <div className="flex flex-row space-x-3">
        <Box className={`shadow ${boxClass}`}>
          .shadow
        </Box>
        <Box className={`shadow-md ${boxClass}`}>
          .shadow-md
        </Box>
        <Box className={`shadow-lg ${boxClass}`}>
          .shadow-lg
        </Box>
      </div>
      <style jsx>
        {`
        th, td {
          border: none;
        }
      `}
      </style>
    </>
  )
}
