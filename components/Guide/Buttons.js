import Button from '../Button';

export default function (props) {
  return (
    <>
      <h2>Button</h2>
      <table>
        <thead>
        </thead>
        <tbody>
        <tr>
          <td colSpan={2}>
            <h5>Primary</h5>
          </td>
        </tr>
        <tr>
          <td>
            <Button size={'xl'} color={'primary'} rounded="true">Button xl</Button>
          </td>
          <td>
            <Button isDelete size={'xl'} color={'primary'} rounded="true">Button xl</Button>
          </td>
        </tr>
        <tr>
          <td>
            <Button size={'l'} color={'primary'} rounded="true">Button l</Button>
          </td>
          <td>
            <Button isDelete size={'l'} color={'primary'} rounded="true">Button l</Button>
          </td>
        </tr>
        <tr>
          <td>
            <Button color={'primary'} rounded="true">Button m</Button>
          </td>
          <td>
            <Button isDelete color={'primary'} rounded="true">Button m</Button>
          </td>
        </tr>
        <tr>
          <td>
            <Button size={'s'} color={'primary'} rounded="true">Button s</Button>
          </td>
          <td>
            <Button isDelete size={'s'} color={'primary'} rounded="true">Button s</Button>
          </td>
        </tr>
        <tr>
          <td>
            <Button size={'xs'} color={'primary'} rounded="true">Button xs</Button>
          </td>
          <td>
            <Button isDelete size={'xs'} color={'primary'} rounded="true">Button xs</Button>
          </td>
        </tr>
        <tr>
          <td colSpan={2}>
            <h5>Secondary</h5>
          </td>
        </tr>
        <tr>
          <td>
            <Button size={'xl'} color={'secondary'} rounded="true">Button xl</Button>
          </td>
          <td>
            <Button size={'xl'}  isDelete color={'secondary'} rounded="true">Button xl</Button>
          </td>
        </tr>
        <tr>
          <td>
            <Button size={'l'} color={'secondary'} rounded="true">Button l</Button>
          </td>
          <td>
            <Button size={'l'}  isDelete color={'secondary'} rounded="true">Button l</Button>
          </td>
        </tr>
        <tr>
          <td>
            <Button color={'secondary'} rounded="true">Button m</Button>
          </td>
          <td>
            <Button isDelete color={'secondary'} rounded="true">Button m</Button>
          </td>
        </tr>
        <tr>
          <td>
            <Button size={'s'} color={'secondary'} rounded="true">Button s</Button>
          </td>
          <td>
            <Button size={'s'}  isDelete color={'secondary'} rounded="true">Button s</Button>
          </td>
        </tr>
        <tr>
          <td>
            <Button size={'xs'} color={'secondary'} rounded="true">Button xs</Button>
          </td>
          <td>
            <Button size={'xs'}  isDelete color={'secondary'} rounded="true">Button xs</Button>
          </td>
        </tr>
        <tr>
          <td colSpan={2}>
            <h5>Tertiary</h5>
          </td>
        </tr>
        <tr>
          <td>
            <Button size={'xl'} color={'tertiary'} rounded="true">Button xl</Button>
          </td>
          <td>
            <Button size={'xl'} isDelete color={'tertiary'} rounded="true">Button xl</Button>
          </td>
        </tr>
        <tr>
          <td>
            <Button size={'l'} color={'tertiary'} rounded="true">Button l</Button>
          </td>
          <td>
            <Button size={'l'} isDelete color={'tertiary'} rounded="true">Button l</Button>
          </td>
        </tr>
        <tr>
          <td>
            <Button color={'tertiary'} rounded="true">Button m</Button>
          </td>
          <td>
            <Button isDelete color={'tertiary'} rounded="true">Button m</Button>
          </td>
        </tr>
        <tr>
          <td>
            <Button size={'s'} color={'tertiary'} rounded="true">Button s</Button>
          </td>
          <td>
            <Button size={'s'} isDelete color={'tertiary'} rounded="true">Button s</Button>
          </td>
        </tr>
        <tr>
          <td>
            <Button size={'xs'} color={'tertiary'} rounded="true">Button xs</Button>
          </td>
          <td>
            <Button size={'xs'} isDelete color={'tertiary'} rounded="true">Button xs</Button>
          </td>
        </tr>
        </tbody>
      </table>
      <style jsx>{`
        th, td {
          border: none;
        }
      `}</style>
    </>
  )
}
