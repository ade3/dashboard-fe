import { typographs } from '../../libs/mock-data';

export default function (props) {
  return (
    <>
      <h2>Typography</h2>
      <table>
        <thead>
        <tr>
          <th>Scale Category</th>
          <th>Typeface</th>
          <th>Weight</th>
          <th>Size</th>
          <th>Example</th>
          <th>Line Height</th>
          <th>Letter Spacing</th>
          <th>Usable For</th>
          <th>React Component</th>
        </tr>
        </thead>
        <tbody>
        {typographs.map(item => {
          return (
            <tr>
              {item.map(Item => {
                return (
                  <td><Item /></td>
                )
              })}
            </tr>
          )
        })}
        </tbody>
      </table>
    </>
  )
}
