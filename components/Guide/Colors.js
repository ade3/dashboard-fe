import Box from '../Box';
import PrimeLayout from '../Layout/PrimeLayout';

export default function (props) {
  return (
    <>
      <h3
        className={'cursor-pointer'}
        onClick={() => router.back()}
      >&larr;</h3>
      <div>
        <h2>Color</h2>
        <h5>Primary color</h5>
      </div>
      <div className="flex flex-row space-x-3">
        <Box
          className={'bg-orange rounded p-2 pt-6'}
        >
          (orange)
          <br />
          bg-orange
          <br />
          text-orange
        </Box>
        <Box
          className={'bg-black rounded p-2 pt-6 text-white'}
        >
          (black)
          <br />
          bg-black
          <br />
          text-black
        </Box>
        <Box
          className={'bg-blue rounded p-2 pt-6 text-white'}
        >
          (blue)
          <br />
          bg-blue
          <br />
          text-blue
        </Box>
        <Box
          className={'bg-white rounded p-2 pt-6 border'}
        >
          (white)
          <br />
          bg-white
          <br />
          text-white
        </Box>
      </div>

      <div className="mt-4">
        <h5>Secondary Color</h5>
      </div>
      <div className="flex flex-row space-x-3">
        <Box
          className={'bg-blue-navy rounded p-2 pt-6 text-white'}
          width={200}
        >
          (navy blue hover)
          <br />
          bg-blue-navy
          <br />
          text-blue-navy
        </Box>
        <Box
          className={'bg-blue-light rounded p-2 pt-6 text-black border'}
          width={200}
        >
          (light blue hover)
          <br />
          bg-blue-light
          <br />
          text-blue-light
        </Box>
        <Box
          className={'bg-grey rounded p-2 pt-6 text-white'}
        >
          (Grey)
          <br />
          bg-grey
          <br />
          text-grey
        </Box>
        <Box
          className={'bg-grey-2 rounded p-2 pt-6 text-white'}
        >
          (Grey 2)
          <br />
          bg-grey-2
          <br />
          text-grey-2
        </Box>
        <Box
          className={'bg-grey-3 rounded p-2 pt-6 text-white'}
        >
          (Grey 3)
          <br />
          bg-grey-3
          <br />
          text-grey-3
        </Box>
        <Box
          className={'bg-grey-4 rounded p-2 pt-6 text-black'}
        >
          (Grey 4)
          <br />
          bg-grey-4
          <br />
          text-grey-4
        </Box>
        <Box
          className={'bg-grey-5 rounded p-2 pt-6 text-black'}
        >
          (Grey 5)
          <br />
          bg-grey-5
          <br />
          text-grey-5
        </Box>
        <Box
          className={'bg-grey-blue rounded p-2 pt-6 text-black'}
        >
          (Grey blue)
          <br />
          bg-grey-blue
          <br />
          text-grey-blue
        </Box>
      </div>

      <div className="mt-4">
        <h5>Status color</h5>
      </div>
      <div className="flex flex-row space-x-3">
        <Box
          className={'bg-success rounded p-2 pt-6 text-white'}
        >
          (Success)
          <br />
          bg-success
          <br />
          text-success
        </Box>
        <Box
          className={'bg-alert rounded p-2 pt-6 text-white'}
        >
          (Alert)
          <br />
          bg-alert
          <br />
          text-alert
        </Box>
        <Box
          className={'bg-warning rounded p-2 pt-6 text-white'}
        >
          (Warning)
          <br />
          bg-warning
          <br />
          text-warning
        </Box>
        <Box
          className={'bg-info rounded p-2 pt-6 text-white'}
        >
          (Info blue- export & import)
          <br />
          bg-info
          <br />
          text-info
        </Box>
      </div>

      <div className="mt-4">
        <h5>Opacity</h5>
        <h6>opacity-<span className={'italic'}>number</span></h6>
        <h6>0, 5, 10, 20, 25, 30, 40, 50, 60, 70, 75, 80, 90, 95, 100</h6>
      </div>
      <div className="flex flex-row space-x-3">
        <Box
          className={'bg-black opacity-40 rounded p-2 pt-6 text-white'}
        >
          (OP 40%)
          <br />
          bg-black opacity-40
          <br />
          text-black opacity-40
        </Box>
        <Box
          className={'bg-black opacity-20 rounded p-2 pt-6 text-white'}
        >
          (OP 20%)
          <br />
          bg-black opacity-20
          <br />
          text-black opacity-20
        </Box>
      </div>
    </>
  )
}
