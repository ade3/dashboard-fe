import styles from '../../../styles/components/dashboard.module.css'
import Subtitle1 from '../../Typography/Subtitle1'

const summeries = [
  {
    title: 'Average Sell',
    Icon: () => (<i style={{ fontSize: 33 }} className={'fa fa-dashboard'} />),
  },
  {
    title: 'Gross Profit',
    Icon: () => (<i style={{ fontSize: 33 }} className={'fa fa-dashboard'} />),
  },
  {
    title: 'Total Customer',
    Icon: () => (<i style={{ fontSize: 33 }} className={'fa fa-dashboard'} />),
  }
]

const Item = (props) => {
  const { title, Icon } = props
  return (
    <div className={'p-2'}>
      <div className={'bg-white w-full rounded'}>
        <div className={styles.summaryItem}>
          <Icon />
          <Subtitle1 className={'inline mx-2 my-auto text-grey-3'}>{title}</Subtitle1>
        </div>
      </div>
    </div>
  )
}

export default function (props) {
  return (
    <div className={styles.container}>
      {summeries.map(item => (<Item {...item} />))}
    </div>
  )
}
