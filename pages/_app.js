import 'tailwindcss/tailwind.css'
import '../styles/globals.css'
import Meta from '../components/Meta'
import AuthService from "../services/auth.service";
import AuthVerify from "../common/auth-verify";
import { useRouter } from "next/router"

function MyApp({ Component, pageProps }) {

  const router = useRouter()
  const logOut = () => {
    AuthService.logout();
    return router.push("/login");
  };

  return (
    <>
      <Meta />
      <Component {...pageProps} />
      <AuthVerify logOut={logOut}/>
    </>
  )
}

export default MyApp
