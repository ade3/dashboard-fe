import Head from 'next/head'
import Form from "react-validation/build/form";
import React, { useState, useRef, useEffect } from "react";
import { useRouter } from "next/router"
import { isEmail } from "validator";
import {Alert} from "@mui/material";

import Subtitle1 from '../components/Typography/Subtitle1'
import Dot from '../components/Dot'
import TextButton2 from '../components/Typography/TextButton2'
import Middle from '../components/Middle'
import Subtitle3 from '../components/Typography/Subtitle3'
import Input from '../components/Input'
import Button from '../components/Button'
import TextButton1 from "../components/Typography/TextButton1";
import AuthService from "../services/auth.service";

export default function Login(props) {
  const form = useRef();
  const checkBtn = useRef();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState("");
  const [emailValid, setEmailValid] = useState(true);
  const [emailCheck, setEmailCheck] = useState("");

  const onChangeEmail = (e) => {
    const inputEmail = e.target.value;
    setEmail(inputEmail);
    if (inputEmail != "" && !isEmail(inputEmail)) {
      setEmailCheck("This is not a valid email")
      setEmailValid(false)
    } else {
      setEmailCheck("")
      setEmailValid(true)
    }
  };
  const onChangePassword = (e) => {
    const password = e.target.value;
    setPassword(password); 
  };
  const router = useRouter()
  useEffect(() => {
    const user = AuthService.getCurrentUser();
    if (user !== null){
      return router.push("/")
    }
  }, []);

  const handleLogin = (e) => {
    e.preventDefault();
    setMessage("");
    setLoading(true);
    form.current.validateAll();
    if (emailValid) {
      AuthService.login(email, password).then(
        () => {
          router.push("/");
        },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
          setLoading(false);
          setMessage(resMessage);
        }
      );
    }
  };


  
  return (
    <>
      <Head>
        <title>Login</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      
      <div className="grid lg:grid-cols-2 xl:grid-cols-2 h-screen">
        <div
          className="bg-blue-navy background-blur-2"
        >
          <div className="px-40">
            <div className="flex flex-row space-x-2 mt-8">
              <Middle><Dot /></Middle>
              <TextButton2 className="text-white">Badr Interactive</TextButton2>
            </div>
            <div
              className="flex flex-col"
              style={{
                justifyContent: 'space-between'
              }}
            >
              <h2 style={{ marginTop: '10vh' }} className="text-white">
                <span className="text-blue">Welcome to</span>
                {' '}
                Dashboard Project!
              </h2>
              {/* <Subtitle1 className="text-white">
                
              </Subtitle1> */}
              <Subtitle1 style={{ marginTop: '30vh' }} className="text-white">
                by Badr Interactive
              </Subtitle1>
            </div>
          </div>
        </div>
        <div
          className="bg-white"
          style={{
            padding: '80px 0px'
          }}
        >
          
          {/* <Middle> */}
          <div className='w-96 m-auto'>
            <div>
              <h2>Login</h2>
              <Form onSubmit={handleLogin} ref={form}>
                
              <div style={{ margin: '24px 0px' }} />
                {message? (<Alert severity="error">{message}</Alert>)
                : null}
              <div style={{ margin: '16px 0px' }} />
              <div className="w-full">
                <Subtitle3>
                  Email
                  <span className="text-red-500">*</span>
                </Subtitle3>
                <Input
                  className="w-full"
                  placeholder="Input your email"
                  type="text"
                  name="email"
                  value={email}
                  onChange={onChangeEmail}
                  required
                />
                <div className="form-group" style={{ margin: '20px 0px auto', color: 'red' }}>
                  {emailCheck}
                </div>
              </div>
              <div style={{ margin: '20px 0px' }} />
              <div className="w-full">
                <Subtitle3>
                  Password
                  <span className="text-red-500">*</span>
                </Subtitle3>
                <Input
                  className="w-full"
                  placeholder="Input your password"
                  type="password"
                  name="password"
                  value={password}
                  onChange={onChangePassword}
                  required
                />
              </div>
              <div style={{ margin: '25px 0px' }} />
              <a href={"/accounts/reset-password"}>
                  <TextButton1>Forgot your password?</TextButton1>
              </a>
              <div style={{ margin: '80px 0px' }} />
              <div className="w-full">
                <Button type="submit" className="w-full" ref={checkBtn}>Login</Button>
              </div>
                <div className={"mb-2"} />
                
              </Form>
            </div>
          </div>
          {/* </Middle> */}
        </div>
      </div>
    </>
  )
}
