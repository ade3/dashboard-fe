import Head from 'next/head'
import * as React from "react";
import {useEffect, useState, useRef} from "react";
import {useRouter} from "next/router"
import Link from 'next/link'
import {Alert} from "@mui/material";

import AppLayout from '../../../components/Layout/App'
import Button from '../../../components/Button'
import Subtitle4 from '../../../components/Typography/Subtitle4'
import Subtitle3 from '../../../components/Typography/Subtitle3'
import AuthService from "../../../services/auth.service";
import ChangeRequestService from '../../../services/changeRequest.service';
import { BACKEND_URL } from "../../../config/index"

function DetailChangeRequest() {
    const router = useRouter() 
    const { pid } = router.query
    const [currentUser, setCurrentUser] = useState(null);

    const [changeRequest, setChangeRequest] = useState("")
    const [issueDate, setIssueDate] = useState("");
    const [file, setFile] = useState("")

    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState("");
    const [errorMessage, setErrorMessage] = useState("");

    // const downloadFile = () => {
    //     ChangeRequestService.download(changeRequest.id).then(
    //         (response) => {
    //             console.log(response)
    //             // 2. Create blob link to download
    //             const url = window.URL.createObjectURL(new Blob([response.body], { type: response.result?.fileType }));
    //             const link = document.createElement('a');
    //             link.href = url;
    //             link.setAttribute('download', `sample.${response.body}`);
    //             // 3. Append to html page
    //             document.body.appendChild(link);
    //             // 4. Force download
    //             link.click();
    //             // 5. Clean up and remove the link
    //             link.parentNode.removeChild(link);
    //         },
    //         (error) => {
    //             console.log(error)
    //             setErrorMessage(error);
    //             setLoading(false)
    //         }
    //     )
    // };

    useEffect(() => {
        if(!router.isReady) return;

        const user = AuthService.getCurrentUser()
        if (user == null) {
            router.push('/login')
        } else {
            setCurrentUser(user)
            console.log(user);
        }
        ChangeRequestService.getChangeRequest(pid).then(
            (response) => {
                setChangeRequest(response.data)
                setIssueDate(response.data.createdAt.substr(0, 10))
            },
            (error) => {
                console.log(error)
                setErrorMessage(error);
                setLoading(false)
            }
        );


    }, [router.isReady]);

    // if (errorMessage != "") {
    //     return <Error errorResponse={errorMessage?.response} redirectTo={`/change-requests`} />
    // }
    if (currentUser == null) {
        return <></>
    }

    const updateStatus = (id) => {
        console.log(id)
        setLoading(true);
        return ChangeRequestService.updateStatus(changeRequest.id, id).then(
            (response) => {
                setChangeRequest(response.data)
                setIssueDate(response.data.createdAt.substr(0, 10))
            },
            (error) => {
                console.log(error)
                setErrorMessage(error.response.data);
                setLoading(false)
            }
        );
    }
    

    return (
        <>
            <Head>
                <title>Change Request Detail</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <AppLayout pageTitle={'Change Request Detail'}>
                <div className="w-3/4 p-1 m-auto bg-white rounded-lg">
                    <div className='w-5/6 m-auto color-white'>
                        {errorMessage? 
                            <div style={{margin: 'auto', width: '50%'}}>
                                <div style={{ margin: '16px 0px' }} />
                                <br/>
                                <Alert severity="error">{errorMessage}</Alert>
                            </div>
                            : null
                        }
                        <div className="grid grid-cols-2 gap-2">
                            <div>
                                <div className="grid grid-cols-2 gap-2">
                                    <div style={{fontWeight: "bold", fontSize: 16}}>
                                        <br></br>
                                        <p>ID</p>
                                        <br></br>
                                        <p>Title</p>
                                        <br></br>
                                        <p>Issue Date</p>
                                        <br></br>
                                        <p>Status</p>
                                        <br></br>
                                        <p>Project</p>
                                        <br></br>
                                        <p>Document</p>
                                    </div>
                                    <div style={{fontSize: 16}}>
                                        <br></br>
                                        <p>: {changeRequest.id}</p>
                                        <br></br>
                                        <p>: {changeRequest.title}</p>
                                        <br></br>
                                        <p>: {issueDate}</p>
                                        <br></br>
                                        <p>: {changeRequest.status?.status}</p>
                                        <br></br>
                                        <p>: {changeRequest.project?.title}</p>
                                        <br></br>
                                        {changeRequest.file != null ? 
                                        <div style={{width: "110px"}}>
                                            <Link rel="noopener noreferrer" href={`${BACKEND_URL}/api/file/download/file-cr/${changeRequest.file?.id}`} target="_blank">
                                                <Button color={'primary'} size={'s'} rounded="true">Download</Button>
                                            </Link>
                                        </div>
                                        : <p>: no file uploaded</p>}
                                    </div>
                                </div>
                            </div>
                            <div>
                                <br></br>
                                <p style={{fontWeight: "bold", fontSize: 16}}>Description</p>
                                <br></br>
                                <div>
                                    {changeRequest.description}
                                </div>
                            </div>
                        </div>

                        <br />
                        <hr />
                        <br />

                        <br/>
                        <div className='grid gap-2 w-40 m-left' style={{ float: 'left' }}>
                            <div>
                                <Link href="/change-requests">
                                    <Button color={'secondary'} size={'s'} rounded="true">Back</Button>
                                </Link>
                            </div>
                        </div>  
                        {currentUser?.role == 3? 
                            <>
                            {changeRequest.status?.id == 1? 
                            <div className='w-50 m-left' style={{ float: 'right' , display: 'flex'}}>
                                <div className='grid gap-2 w-40 m-right' style={{ marginRight: 20}}>
                                    <div>
                                        <Button onClick={() => updateStatus(3)} color={'primary'} size={'s'} className= 'red' isDelete={true} rounded="true">Reject</Button>
                                    </div>
                                </div>
                                <div className='grid gap-2 w-40 m-right'>
                                    <div>
                                        <Button onClick={() => updateStatus(2)} size={'s'} className='navy' rounded="true">Accept</Button>
                                    </div>
                                </div> 
                                
                                
                            </div>
                            : null}
                            </>
                        : null}

                    </div>
                    <div style={{ margin: '80px 0px' }} />
                </div>
            
            </AppLayout>
        </>
    )
}

export default DetailChangeRequest