import Head from 'next/head'
import Link from 'next/link'
import * as React from "react";
import {useEffect, useState} from "react";
import {useRouter} from "next/router"
import {DataGrid} from '@mui/x-data-grid';
import {Alert, Snackbar} from "@mui/material";

import AppLayout from '../../components/Layout/App'
import Button from '../../components/Button'
import AuthService from '../../services/auth.service'
import ChangeRequestService from '../../services/changeRequest.service'


function createData(id, title, createdAt, project, status) {
    let timestamp = createdAt
    let issueDate = timestamp.substr(0,10)
    return {
        id,
        title,
        issueDate,
        project,
        status
    };
}

function getColumns(idRole) {
    let columns = [
        {field: 'id', headerName: 'ID', width: 100, headerClassName: 'bg-slate-300'},
        {field: 'title', headerName: 'Title', width: 300},
        {field: 'issueDate', headerName: "Issue Date", width: 150},
        {field: 'project', headerName: "Project", width: 170},
        {field: 'status', headerName: 'Status', width: 150}
    ]
    // exclude admin
    if (idRole != 2) {
        columns.push(
            {
                field: 'action', 
                headerName: 'Action', 
                width: 100, 
                sortable: false,
                renderCell: renderActionButton,
                disableClickEventBubbling: true
            }
        )
    }
    return columns
}

const renderActionButton = (params) => {
    return (
        <div className='flex'>
            <Link href={`/change-requests/${encodeURIComponent(params.row.id)}`}>
                <Button size={'s'} color={'primary'} className={'navy'} rounded="true">Detail</Button>
            </Link>
        </div>
    )
}

function getRows(listCr) {
    let arr = []
    if (listCr != null) {
        for (const cr of listCr) {
            arr.push(createData(cr.id, cr.title, cr.createdAt, cr.project.title, cr.status.status))
        }
    }
    return arr
}

function ListChangeRequests() {
    const router = useRouter()
    const [currentUser, setCurrentUser] = useState(null);

    const [listChangeRequests, setListChangeRequests] = useState(null);

    const [loading, setLoading] = useState(false);
    const [openMessage, setOpenMessage] = useState(false);
    const [message, setMessage] = useState({
        value: "",
        severity: ""
    });
    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenMessage(false);
    };

    useEffect(() => {
        if(!router.isReady) return;

        setMessage("");
        setLoading(true);

        ChangeRequestService.getAllChangeRequests().then(
            (response) => {
                if(Array.isArray(response.data)) {
                    setListChangeRequests(response.data)
                } else {
                    setMessage({
                        value: response?.data,
                        severity: "error"
                    })
                    setOpenMessage(true);
                }
            },
            (error) => {
                setLoading(false);
                if (error.response == undefined) {
                    router.push('/503')
                }

                setMessage({
                    value: error.response?.data,
                    severity: "error"
                })
                setOpenMessage(true);

            }
            
        );

        const user = AuthService.getCurrentUser()
        if (user == null) {
            router.push('/login')
        } else if (user.role == 2) {
            router.push("/401")
        } else {
            setCurrentUser(user)
        }
    }, [router.isReady]);

    if (currentUser == null) return <></>

    return (
        <>
            <Head>
                <title>Change Requests</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            <AppLayout pageTitle={'List Change Requests, ' + currentUser?.name}>
                <div style={{height: 500, width: '90%', margin: 'auto', backgroundColor: "white" }}>
                    
                    {/* {listChangeRequests?  */}
                        <DataGrid
                            style={{float: 'center', paddingLeft: "1em", fontSize: '14px', fontWeight: "bold"}}
                            rows={getRows(listChangeRequests)}
                            columns={getColumns(currentUser?.role)}
                            pageSize={10}
                            // rowsPerPageOptions={[10]}
                            disableSelectionOnClick
                            disableMultipleSelection={true}
                            // checkboxSelection
                            className="bg-white rounded-lg"
                        />
                    {/* :   null } */}
                        {/* // <div style={{margin: 'auto', width: '50%'}}>
                        //     <div style={{ margin: '24px 0px' }} />
                        //     <br/>
                        //     <Alert severity="error">{errorMessage}</Alert>
                        // </div>} */}
                        
                    
                </div>
                {(message.value != "") && (
                    <Snackbar
                        anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                        open={openMessage}
                        onClose={handleCloseMessage}
                        autoHideDuration={2000}>
                        <Alert onClose={handleCloseMessage} variant="filled" severity={message.severity}>
                            {message.value}
                        </Alert>
                    </Snackbar>
                )}
            </AppLayout>
        </>
    )
}


export default ListChangeRequests
