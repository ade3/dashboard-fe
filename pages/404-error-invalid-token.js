import { useRouter } from "next/router"
import {useEffect} from "react";
export default function Error() {
    const router = useRouter()
    setTimeout(() => {
        router.push("/login")
    }, 2000); //will call the function after 2 secs.

    return (
        <>
            <h3 className="m-auto" style={{ textAlign:'center', paddingTop:'200px'}}>
                Invalid token: Try login again or reset your password
            </h3>
            <h4 className="loading" style={{ textAlign:'center'}}>Redirecting</h4>
        </>
    )

}
