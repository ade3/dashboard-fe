import Head from 'next/head'
import * as React from "react";
import {useEffect, useState, useRef} from "react";
import {useRouter} from "next/router"
import {IconButton, Alert, Snackbar} from "@mui/material";
import {isEmail} from "validator";

import styles from '../../styles/components/dashboard.module.css'
import AppLayout from '../../components/Layout/App'
import AuthService from "../../services/auth.service";
import UserService from "../../services/user.service";
import RoleService from "../../services/role.service";
import Input from '../../components/Input'
import Button from '../../components/Button'
import Subtitle4 from '../../components/Typography/Subtitle4'

export default function CreateAccount(props) {
    const form = useRef();
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [role, setRole] = useState(2);
    const [loading, setLoading] = useState(false);
    const [nameCheck, setNameCheck] = useState("");
    const [nameValid, setNameValid] = useState("");
    const [emailCheck, setEmailCheck] = useState("");
    const [passwordShown, setPasswordShown] = useState(false);
    const [confirmPasswordShown, setConfirmPasswordShown] = useState(false);
    const [passwordCheck, setPasswordCheck] = useState("");
    const [passwordMatch, setPasswordMatch] = useState("");
    const [passwordValid, setPasswordValid] = useState(true);
    const [matchValid, setMatchValid] = useState(true);
    const [emailValid, setEmailValid] = useState(true);

    const [roleData, setRoleData] = useState([]);
    const [authorization, setAuthorization] = useState("");
    const [openMessage, setOpenMessage] = useState(false);
    const [message, setMessage] = useState({
        value: "",
        severity: ""
    });
    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenMessage(false);
    };


    //mengubah value button show & hide password
    const togglePassword = () => {
        setPasswordShown(!passwordShown);
    };
    const toggleConfirmPassword = () => {
        setConfirmPasswordShown(!confirmPasswordShown);
    };

    const router = useRouter()
    useEffect(() => {
        const user = AuthService.getCurrentUser();
        if (user === null) {
            router.push("/login")
        } else if (user.role > 2) {
            router.push("/401")
        } else {
            return RoleService.getAllRoles()
                .then((response) => {
                    setRoleData(response.data.result);
                    setAuthorization(user.role);
                    //mengubah default value dropdown input role
                    if (user.role == 2) {
                        setRole(3);
                    }
                },
                (error) => {
                    if (error.response == undefined) {
                        router.push('/503')
                    }
                    router.push('/404')
                });
        }
    }, []);

    const handleSubmit = (e) => {
        e.preventDefault();
        setMessage("");
        setLoading(true);
        if (emailValid && passwordValid && nameValid && matchValid) {
            let account = {
                email: email,
                name: name,
                password: password,
                role: role
            }
            UserService.createAccount(account)
                .then((response) => {
                    AuthService.requestResetPassword(email)

                    setMessage({
                        value: response.data?.message,
                        severity: "success"
                    })
                    setOpenMessage(true);
                    setName("");
                    setEmail("");
                    setPassword("");
                    setConfirmPassword("");
                },
                (error) => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString();
                    setLoading(false);
                    setMessage({
                        value: resMessage,
                        severity: "error"
                    })
                    setOpenMessage(true)
                });
        }

    };

    const onChangeName = (e) => {
        const inputName = e.target.value;
        setName(inputName);
        if (inputName != "" && inputName.length > 50) {
            setNameCheck("Name should consist <= 50 character")
            setNameValid(false)
        } else {
            setNameValid(true)
            setNameCheck("")
        }

    };

    const onChangeEmail = (e) => {
        const inputEmail = e.target.value
        setEmail(inputEmail);
        if (inputEmail != "" && !isEmail(inputEmail)) {
            setEmailCheck("This is not a valid email")
            setEmailValid(false)
        } else if (inputEmail != "" && inputEmail.length > 50) {
            setEmailCheck("Email should consist <= 50 character")
            setEmailValid(false)
        } else {
            setEmailCheck("")
            setEmailValid(true)
        }
    };

    const onChangePassword = (e) => {
        const pw = e.target.value;
        setPassword(pw);
        if (pw != "" && pw.length < 5) {
            setPasswordCheck("Password should consist of >5 character")
            setPasswordValid(false)
        } else if (pw != "" && pw.length > 50) {
            setPasswordCheck("Password should consist <= 50 character")
            setPasswordValid(false)
        } else {
            setPasswordValid(true)
            setPasswordCheck("")
        }
        if (confirmPassword !== "") {
            if (confirmPassword !== pw) {
                setPasswordMatch("Password should match")
                setMatchValid(false)
            } else {
                setMatchValid(true)
                setPasswordMatch("")
            }
        } else {
            setPasswordMatch("")
        }
    };

    const onChangeConfirmPassword = (e) => {
        const confirmPw = e.target.value;
        setConfirmPassword(confirmPw);
        if (password !== "") {
            if (confirmPw !== password) {
                setPasswordMatch("Password should match")
                setMatchValid(false)
            } else {
                setMatchValid(true)
                setPasswordMatch("")
            }
        } else {
            setPasswordMatch("")
        }

    };

    const onChangeRole = (e) => {
        setRole(e.target.value);
    };

    return (
        <>
            <Head>
                <title>Accounts</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            <AppLayout pageTitle="Create Account">
                <div className="w-2/3 p-1 m-auto bg-white rounded-lg">
                    <div className='w-3/4 m-auto color-white'>
                        <form onSubmit={handleSubmit} ref={form}>
                            <div style={{margin: '24px 0px'}}/>
                            <div className="w-full">
                                <Subtitle4>
                                    Name
                                    <span className="text-red-500">*</span>
                                </Subtitle4>
                                <Input size="s"
                                       className="w-full"
                                       placeholder="Input name"
                                       type="text"
                                       name="name"
                                       value={name}
                                       onChange={onChangeName}
                                       required
                                />
                                {nameCheck && (
                                    <div style={{margin: '8px 0px', color: 'red', fontSize: 12}}>
                                        {nameCheck}
                                    </div>
                                )}
                            </div>
                            <div style={{margin: '16px 0px'}}/>
                            <div className="w-full">
                                <Subtitle4>
                                    Email
                                    <span className="text-red-500">*</span>
                                </Subtitle4>
                                <Input size="s"
                                       className="w-full"
                                       placeholder="Input email"
                                       type="text"
                                       name="email"
                                       value={email}
                                       onChange={onChangeEmail}
                                       required
                                />
                                {emailCheck && (
                                    <div style={{margin: '8px 0px', color: 'red', fontSize: 12}}>
                                        {emailCheck}
                                    </div>
                                )}
                            </div>
                            <div style={{margin: '16px 0px'}}/>
                            <div className="w-full">
                                <Subtitle4>
                                    Password
                                    <span className="text-red-500">*</span>
                                </Subtitle4>
                                <Input size="s"
                                       className="w-full"
                                       placeholder="Input password"
                                       type={passwordShown ? "text" : "password"}
                                       name="password"
                                       value={password}
                                       onChange={onChangePassword}
                                       required
                                />
                                <IconButton onClick={togglePassword}
                                            style={{fontSize: 12, position: 'absolute', right: '22%', padding: 8}}>
                                    {passwordShown ? "hide" : "show"}</IconButton>
                                {passwordCheck && (
                                    <div style={{margin: '8px 0px', color: 'red', fontSize: 12}}>
                                        {passwordCheck}
                                    </div>
                                )}
                            </div>
                            <div style={{margin: '16px 0px'}}/>
                            <div className="w-full">
                                <Subtitle4>
                                    Confirm Password
                                    <span className="text-red-500">*</span>
                                </Subtitle4>
                                <Input size="s"
                                       className="w-full"
                                       placeholder="Input confirmation password"
                                       type={confirmPasswordShown ? "text" : "password"}
                                       name="confirmPassword"
                                       value={confirmPassword}
                                       onChange={onChangeConfirmPassword}
                                       required
                                />
                                <IconButton onClick={toggleConfirmPassword}
                                            style={{fontSize: 12, position: 'absolute', right: '22%', padding: 8}}>
                                    {confirmPasswordShown ? "hide" : "show"}</IconButton>
                                {passwordMatch && (
                                    <div style={{margin: '8px 0px', color: 'red', fontSize: 12}}>
                                        {passwordMatch}
                                    </div>
                                )}
                            </div>
                            <div style={{margin: '16px 0px'}}/>
                            <div className="w-full">
                                <Subtitle4>
                                    Role
                                    <span className="text-red-500">*</span>
                                </Subtitle4>
                                {authorization == 1 ?
                                    (<select value={role} onChange={onChangeRole}
                                             className='border border-gray-300 w-full'
                                             style={{borderRadius: 4, padding: '6px 12px', fontSize: 14}}>
                                        {roleData.map(roles => {
                                            return (
                                                roles.id > 1 ? (<option value={roles.id}>{roles.role}</option>) : null
                                            )
                                        })}
                                    </select>) :
                                    (<select value={role} onChange={onChangeRole}
                                             className='border border-gray-300 w-full'
                                             style={{borderRadius: 4, padding: '6px 12px', fontSize: 14}}>
                                        {roleData.map(roles => {
                                            return (
                                                roles.id > 2 ? (<option value={roles.id}>{roles.role}</option>) : null
                                            )
                                        })}
                                    </select>)
                                }
                            </div>
                            <div style={{margin: '48px 0px'}}/>
                            <div className='w-40 m-left' style={{float: 'right'}}>
                                <Button type="submit" size={'s'} color={'primary'} rounded="true">Submit</Button>
                            </div>
                        </form>
                    </div>
                    <div style={{margin: '120px 0px'}}/>
                    {(message.value != "") && (
                        <Snackbar
                            anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                            open={openMessage}
                            onClose={handleCloseMessage}
                            autoHideDuration={2000}>
                            <Alert onClose={handleCloseMessage} variant="filled" severity={message.severity}>
                                {message.value}
                            </Alert>
                        </Snackbar>
                    )}
                </div>
            </AppLayout>

        </>
    )
}
