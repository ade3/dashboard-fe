import Head from 'next/head'
import * as React from "react";
import { useEffect, useState, useRef } from "react";
import { useRouter } from "next/router"
import {IconButton, Alert, Snackbar} from "@mui/material";
import { isEmail } from "validator";
import axios from "axios";

import styles from '../../styles/components/dashboard.module.css'
import UserService from '../../services/user.service'
import AppLayout from '../../components/Layout/App'
import AuthService from "../../services/auth.service";
import Input from '../../components/Input'
import Button from '../../components/Button'
import Subtitle4 from '../../components/Typography/Subtitle4'
import authHeader from "../../services/auth-header";
import { BACKEND_URL } from "../../config";

export default function UpdateAccount() {
  const router = useRouter()
  const {
    query: { id },
  } = router
  const idAkun = { id };

  const form = useRef();
  const [responseStatus, setResponseStatus] = useState({ status: 500, data: "Internal Server Error" })
  const [dataAccount, setDataAccount] = useState();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [role, setRole] = useState(1);
  const [loading, setLoading] = useState(false);
  const [openMessage, setOpenMessage] = useState(false);
  const [emailCheck, setEmailCheck] = useState("");
  const [emailValid, setEmailValid] = useState(true);
  const [message, setMessage] = useState({
    value: "",
    severity: ""
  })


  const handleCloseMessage = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenMessage(false);
  };

  const handleBack = () => {
    return router.push("/accounts");
  };

  useEffect(() => {
    const user = AuthService.getCurrentUser();
    if (user === null) {
      router.push("/login")
    } else if (user.role > 2) {
      router.push("/401")
    } else {
      UserService.getAccount(idAkun.id).then(
        (response) => {
          setResponseStatus(response)
          setName(response.data.result.name)
          setEmail(response.data.result.email)
        },
        (error) => {
          setResponseStatus(error.response)
        }
      );
    }
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    setMessage("");
    setLoading(true);
    if (emailValid) {
      return axios
        .put(`${BACKEND_URL}/api/accounts/` + idAkun.id,
          { email, name },
          { headers: authHeader() }
        ).then((response) => {
          AuthService.requestResetPassword(email)

          setMessage({
            value: response.data?.message,
            severity: "success"
          })
          setName("");
          setEmail("");
          router.push("/accounts")
        },
          (error) => {
            const resMessage =
              (error.response &&
                error.response.data &&
                error.response.data.message) ||
              error.message ||
              error.toString();
            setLoading(false);
            setMessage(resMessage);
          }
        );
    }
  };

  const onChangeName = (e) => {
    setName(e.target.value);
  };

  const onChangeEmail = (e) => {
    setEmail(e.target.value);
    if (email != "" && !isEmail(email)) {
      setEmailCheck("This is not a valid email")
      setEmailValid(false)
    } else {
      setEmailCheck("")
      setEmailValid(true)
    }
  };

  return (
    <>
      <Head>
        <title>Accounts</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <AppLayout pageTitle="Update Account">
        <div className="w-2/3 p-1 m-auto bg-white rounded-lg">
          <div className='w-3/4 m-auto color-white'>
            <form onSubmit={handleSubmit} ref={form}>
              <div style={{ margin: '39px 0px' }} />
              <div className="w-full">
                <Subtitle4>
                  Name
                  <span className="text-red-500">*</span>
                </Subtitle4>
                <Input size="s"
                  className="w-full"
                  placeholder = {name}
                  type="text"
                  name="name"
                  value={name}
                  onChange={onChangeName}
                  required
                />
              </div>
              <div style={{ margin: '16px 0px' }} />
              <div className="w-full">
                <Subtitle4>
                  Email
                  <span className="text-red-500">*</span>
                </Subtitle4>
                <Input size="s"
                  className="w-full"
                  placeholder={email}
                  type="text"
                  name="email"
                  value={email}
                  onChange={onChangeEmail}
                  required
                />
                {emailCheck && (
                  <div style={{ margin: '8px 0px', color: 'red', fontSize: 12 }}>
                    {emailCheck}
                  </div>
                )}
              </div>
              <div style={{ margin: '48px 0px' }} />
              <div style={{ margin: '48px 0px' }} />
              <div className='w-40 m-left' style={{ float: 'left' }}>
                <Button onClick={handleBack} size={'s'} color={'secondary'} rounded="true">Back</Button>
              </div>
              <div className='w-40 m-left' style={{ float: 'right' }}>
                <Button onClick={handleSubmit} size={'s'} color={'primary'} rounded="true">Submit</Button>
              </div>
            </form>
          </div>
          <div style={{ margin: '120px 0px' }} />
        </div>
        <div style={{margin: '120px 0px'}}/>
                    {(message.value != "") && (
                        <Snackbar
                            anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                            open={openMessage}
                            onClose={handleCloseMessage}
                            autoHideDuration={2000}>
                            <Alert onClose={handleCloseMessage} variant="filled" severity={message.severity}>
                                {message.value}
                            </Alert>
                        </Snackbar>
                    )}
      </AppLayout>

    </>
  )
}