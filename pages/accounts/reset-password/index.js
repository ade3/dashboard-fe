import Head from 'next/head'
import React, {useState, useRef, useEffect} from "react"
import {Alert} from "@mui/material"
import {isEmail} from "validator";

import Subtitle4 from "../../../components/Typography/Subtitle4"
import Subtitle1 from '../../../components/Typography/Subtitle1'
import Dot from '../../../components/Dot'
import TextButton2 from '../../../components/Typography/TextButton2'
import Middle from '../../../components/Middle'
import Input from '../../../components/Input'
import Button from '../../../components/Button'
import AuthService from "../../../services/auth.service"

export default function RequestResetPassword(props) {
    const form = useRef()
    const [email, setEmail] = useState("")
    const [emailCheck, setEmailCheck] = useState("")
    const [emailValid, setEmailValid] = useState(true)

    const [loading, setLoading] = useState(false)
    const [message, setMessage] = useState({
        value: "",
        severity: ""
    });

    const handleSubmit = (e) => {
        e.preventDefault()
        if (emailValid) {
            console.log("valid")
            setMessage({
                value: "Please wait...",
                severity: "warning"
            })
            setLoading(true)
            AuthService.requestResetPassword(email)
                .then((response) => {
                        setMessage({
                            value: response.data,
                            severity: "success" // success, error, warning, atau info
                        })
                        AuthService.logout()
                    },
                    (error) => {
                        const resMessage =
                            (error.response &&
                                error.response.data &&
                                error.response.data.message) ||
                            error.message ||
                            error.toString()
                        setMessage({
                            value: resMessage,
                            severity: "error" // success, error, warning, atau info
                        })
                    }
                )
        } else {
            console.log("ga valid")
        }
        setLoading(false)

    }

    const onChangeEmail = (e) => {
        const inputEmail = e.target.value
        setEmail(inputEmail)
        if (inputEmail != "" && !isEmail(inputEmail)) {
            setEmailCheck("This is not a valid email")
            setEmailValid(false)
        } else if (inputEmail != "" && inputEmail.length > 50) {
            setEmailCheck("Email should consist <= 50 character")
            setEmailValid(false)
        } else {
            setEmailCheck("")
            setEmailValid(true)
        }
    }

    return (
        <>
            <Head>
                <title>Reset Password</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <div className="grid lg:grid-cols-2 xl:grid-cols-2 h-screen">
                <div
                    className="bg-blue-navy background-blur-2"
                >
                    <div className="px-40">
                        <div className="flex flex-row space-x-2 mt-8">
                            <Middle><Dot/></Middle>
                            <TextButton2 className="text-white">Badr Interactive</TextButton2>
                        </div>
                        <div
                            className="flex flex-col"
                            style={{
                                justifyContent: 'space-between'
                            }}
                        >
                            <h2 style={{marginTop: '10vh'}} className="text-white">
                                <span className="text-blue">Welcome to</span>
                                {' '}
                                Dashboard Project!
                            </h2>
                            <Subtitle1 style={{marginTop: '30vh'}} className="text-white">
                                by Badr Interactive
                            </Subtitle1>
                        </div>
                    </div>
                </div>
                <div
                    className="bg-white"
                    style={{
                        padding: '80px 0px'
                    }}
                >
                    <div className='w-96 m-auto'>
                        <div>
                            <h2>Reset Password</h2>
                            <form onSubmit={handleSubmit} ref={form}>
                                <div style={{margin: '24px 0px'}}/>
                                {(message.value != "") && (
                                    <Alert severity={message.severity}>{message.value}</Alert>
                                )}
                                <div style={{margin: '16px 0px'}}/>
                                <div style={{margin: '16px 0px'}}/>
                                <div className="w-full">
                                    <Subtitle4>
                                        Email
                                        <span className="text-red-500">*</span>
                                    </Subtitle4>
                                    <Input size="s"
                                           className="w-full"
                                           placeholder="Input email"
                                           type="text"
                                           name="email"
                                           value={email}
                                           onChange={onChangeEmail}
                                           required
                                    />
                                    {emailCheck && (
                                        <div style={{margin: '8px 0px', color: 'red', fontSize: 12}}>
                                            {emailCheck}
                                        </div>
                                    )}
                                </div>
                                <div style={{margin: '48px 0px'}}/>
                                <div className='w-40 m-left' style={{float: 'right'}}>
                                    <Button type="submit" size={'s'} color={'primary'} rounded="true">Submit</Button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
