import Head from 'next/head'

import React, {useState, useRef, useEffect} from "react"
import {useRouter} from "next/router"
import {Alert, IconButton} from "@mui/material"

import Subtitle1 from '../../../components/Typography/Subtitle1'
import Dot from '../../../components/Dot'
import TextButton2 from '../../../components/Typography/TextButton2'
import Middle from '../../../components/Middle'
import Input from '../../../components/Input'
import Button from '../../../components/Button'
import Subtitle4 from "../../../components/Typography/Subtitle4"
import AuthService from "../../../services/auth.service"

export default function ResetPassword(props) {
    const router = useRouter()
    const {token} = router.query
    const [thisUser, setThisUser] = useState("")

    useEffect(() => {
        if (!router.isReady) return

        AuthService.getUserByResetToken(token).then(
            (response) => {
                setThisUser(response.data)
            },
            (error) => {
                if (error.response == undefined) {
                    router.push('/503')
                }
                router.push('/404-error-invalid-token')
            }

        )

    }, [router.isReady])

    const form = useRef()
    const [password, setPassword] = useState("")
    const [confirmPassword, setConfirmPassword] = useState("")
    const [loading, setLoading] = useState(false)
    const [message, setMessage] = useState({
        value: "",
        severity: "" // bisa terdiri dari error, warning, info, atau success
    });
    const [passwordShown, setPasswordShown] = useState(false)
    const [confirmPasswordShown, setConfirmPasswordShown] = useState(false)
    const [passwordCheck, setPasswordCheck] = useState("")
    const [passwordMatch, setPasswordMatch] = useState("")
    const [passwordValid, setPasswordValid] = useState(true)
    const [matchValid, setMatchValid] = useState(true)

    //mengubah value button show & hide password
    const togglePassword = () => {
        setPasswordShown(!passwordShown)
    }
    const toggleConfirmPassword = () => {
        setConfirmPasswordShown(!confirmPasswordShown)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        setLoading(true)
        if (passwordValid && matchValid) {
            AuthService.resetPassword(token, password).then(
                (response) => {
                    setMessage({
                        value: response.data,
                        severity: "success"
                    })
                    AuthService.logout()
                    setTimeout(() => {
                        router.push("/login")
                    }, 2000); //will call the function after 2 secs.
                },
                (error) => {
                    const resMessage =
                        (error.response &&
                            error.response.data &&
                            error.response.data.message) ||
                        error.message ||
                        error.toString()
                    setLoading(false)
                    setMessage({
                        value: resMessage,
                        severity: "error"
                    })
                }
            )
        }
        setLoading(false)
    }

    const onChangePassword = (e) => {
        const pw = e.target.value
        setPassword(pw)
        if (pw != "" && pw.length < 5) {
            setPasswordCheck("Password should consist of >5 character")
            setPasswordValid(false)
        } else if (pw != "" && pw.length > 50) {
            setPasswordCheck("Password should consist <= 50 character")
            setPasswordValid(false)
        } else {
            setPasswordValid(true)
            setPasswordCheck("")
        }
        if (confirmPassword !== "") {
            if (confirmPassword !== pw) {
                setPasswordMatch("Password should match")
                setMatchValid(false)
            } else {
                setMatchValid(true)
                setPasswordMatch("")
            }
        } else {
            setPasswordMatch("")
        }
    }

    const onChangeConfirmPassword = (e) => {
        const confirmPw = e.target.value
        setConfirmPassword(confirmPw)
        if (password !== "") {
            if (confirmPw !== password) {
                setPasswordMatch("Password should match")
                setMatchValid(false)
            } else {
                setMatchValid(true)
                setPasswordMatch("")
            }
        } else {
            setPasswordMatch("")
        }

    }

    if (thisUser == "") return <></>

    return (
        <>
            <Head>
                <title>Reset Password</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <div className="grid lg:grid-cols-2 xl:grid-cols-2 h-screen">
                <div
                    className="bg-blue-navy background-blur-2"
                >
                    <div className="px-40">
                        <div className="flex flex-row space-x-2 mt-8">
                            <Middle><Dot/></Middle>
                            <TextButton2 className="text-white">Badr Interactive</TextButton2>
                        </div>
                        <div
                            className="flex flex-col"
                            style={{
                                justifyContent: 'space-between'
                            }}
                        >
                            <h2 style={{marginTop: '10vh'}} className="text-white">
                                <span className="text-blue">Welcome to</span>
                                {' '}
                                Dashboard Project!
                            </h2>
                            <Subtitle1 style={{marginTop: '30vh'}} className="text-white">
                                by Badr Interactive
                            </Subtitle1>
                        </div>
                    </div>
                </div>
                <div
                    className="bg-white"
                    style={{
                        padding: '80px 0px'
                    }}
                >
                    <div className='w-2/3 m-auto'>
                        <div>
                            <h2>Reset Password</h2>
                            <p><b>{thisUser?.email}</b></p>
                            <form onSubmit={handleSubmit} ref={form}>
                                <div style={{margin: '24px 0px'}}/>
                                {(message.value != "") && (
                                    <Alert severity={message.severity}>{message.value}</Alert>
                                )}
                                <div style={{margin: '16px 0px'}}/>
                                <div className="w-full">
                                    <Subtitle4>
                                        New Password
                                        <span className="text-red-500">*</span>
                                    </Subtitle4>
                                    <Input size="s"
                                           className="w-full"
                                           placeholder="Input new password"
                                           type={passwordShown ? "text" : "password"}
                                           name="password"
                                           value={password}
                                           onChange={onChangePassword}
                                           required
                                    />
                                    <IconButton onClick={togglePassword}
                                                style={{fontSize: 12, position: 'absolute', padding: 8}}>
                                        {passwordShown ? "hide" : "show"}</IconButton>
                                    {passwordCheck && (
                                        <div style={{margin: '8px 0px', color: 'red', fontSize: 12}}>
                                            {passwordCheck}
                                        </div>
                                    )}
                                </div>
                                <div style={{margin: '16px 0px'}}/>
                                <div className="w-full">
                                    <Subtitle4>
                                        Confirm Password
                                        <span className="text-red-500">*</span>
                                    </Subtitle4>
                                    <Input size="s"
                                           className="w-full"
                                           placeholder="Input confirmation password"
                                           type={confirmPasswordShown ? "text" : "password"}
                                           name="confirmPassword"
                                           value={confirmPassword}
                                           onChange={onChangeConfirmPassword}
                                           required
                                    />
                                    <IconButton onClick={toggleConfirmPassword}
                                                style={{fontSize: 12, position: 'absolute', padding: 8}}>
                                        {confirmPasswordShown ? "hide" : "show"}</IconButton>
                                    {passwordMatch && (
                                        <div style={{margin: '8px 0px', color: 'red', fontSize: 12}}>
                                            {passwordMatch}
                                        </div>
                                    )}
                                </div>
                                <div style={{margin: '48px 0px'}}/>
                                <div className='w-40 m-left' style={{float: 'right'}}>
                                    <Button type="submit" size={'s'} color={'primary'} rounded="true">Submit</Button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
