import Head from 'next/head'

import AppLayout from '../../../components/Layout/App'
import Button from '../../../components/Button'
import Subtitle4 from '../../../components/Typography/Subtitle4'
import Subtitle3 from '../../../components/Typography/Subtitle3'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React, { useEffect, useState } from "react";

import AuthService from "../../../services/auth.service";
import ReportService from '../../../services/reportIssue.service';
import { BACKEND_URL } from "../../../config/index"
import { Alert } from "@mui/material";

function DetailReportIssue() {
    const router = useRouter()
    const { pid } = router.query
    const [currentUser, setCurrentUser] = useState(null);

    const [reportIssue, setReportIssue] = useState("")
    const [issueDate, setIssueDate] = useState("");
    const [file, setFile] = useState("")

    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState("");
    const [errorMessage, setErrorMessage] = useState("");


    useEffect(() => {
        if (!router.isReady) return;

        const user = AuthService.getCurrentUser()
        if (user == null) {
            router.push('/login')
        } else {
            setCurrentUser(user)
        }
        ReportService.getReportIssue(pid).then(
            (response) => {
                setReportIssue(response.data)
                setIssueDate(response.data.createdAt.substr(0, 10))
            },
            (error) => {
                console.log(error)
                setErrorMessage(error);
                setLoading(false)
            }
        );


    }, [router.isReady]);

    if (currentUser == null) {
        return <></>
    }

    const updateStatus = (id) => {
        console.log(id)
        setLoading(true);
        return ReportService.updateStatus(reportIssue.id, id).then(
            (response) => {
                setReportIssue(response.data)
                setIssueDate(response.data.createdAt.substr(0, 10))
            },
            (error) => {
                console.log(error)
                setErrorMessage(error);
                setLoading(false)
            }
        );
    }


    return (
        <>
            <Head>
                <title>Report Detail</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <AppLayout pageTitle={'Report Detail'}>
                <div className="w-3/4 p-1 m-auto bg-white rounded-lg">
                    <div className='w-5/6 m-auto color-white'>
                        {errorMessage ?
                            <div style={{ margin: 'auto', width: '50%' }}>
                                <div style={{ margin: '16px 0px' }} />
                                <br />
                                <Alert severity="error">{errorMessage}</Alert>
                            </div>
                            : null
                        }
                        <div className="grid grid-cols-2 gap-2">
                            <div>
                                <div className="grid grid-cols-2 gap-2">
                                    <div style={{ fontWeight: "bold", fontSize: 16 }}>
                                        <br></br>
                                        <p>ID</p>
                                        <br></br>
                                        <p>Title</p>
                                        <br></br>
                                        <p>Issue Date</p>
                                        <br></br>
                                        <p>Status</p>
                                        <br></br>
                                        <p>Project</p>
                                        <br></br>
                                        <p>Document</p>
                                    </div>
                                    <div style={{ fontSize: 16 }}>
                                        <br></br>
                                        <p>: {reportIssue.id}</p>
                                        <br></br>
                                        <p>: {reportIssue.title}</p>
                                        <br></br>
                                        <p>: {issueDate}</p>
                                        <br></br>
                                        <p>: {reportIssue.status?.status}</p>
                                        <br></br>
                                        <p>: {reportIssue.project?.title}</p>
                                        <br></br>
                                        {reportIssue.file != null ? 
                                        <div style={{ width: "110px" }}>
                                            <Link rel="noopener noreferrer" href={`${BACKEND_URL}/api/file/download/file-ri/${reportIssue.file?.id}`} target="_blank">
                                                <Button className='navy' size={'s'} rounded="true">Download</Button>
                                            </Link>
                                        </div>
                                        : <p>: no file uploaded</p>}
                                        
                                    </div>
                                </div>
                            </div>
                            <div>
                                <br></br>
                                <p style={{ fontWeight: "bold", fontSize: 16 }}>Description</p>
                                <br></br>
                                <div>
                                    {reportIssue.description}
                                </div>
                            </div>
                        </div>

                        <br />
                        <hr />
                        <br />

                        <br />
                        <div className='grid gap-2 w-40 m-left' style={{ float: 'left' }}>
                            <div>
                                <Link href="/reports">
                                    <Button color={'secondary'} size={'s'} rounded="true">Back</Button>
                                </Link>
                            </div>
                        </div>

                        {/* Kondisi 1 */}
                        {currentUser?.role == 3 && reportIssue.status?.id == 1 ?
                            <>
                                <div className='grid gap-2 w-40 m-right' style={{ float: 'right' , marginLeft: 20}}>
                                    <div>
                                        <Button onClick={() => updateStatus(3)} className='navy' size={'s'} rounded="true">Evaluate</Button>
                                    </div>
                                </div>

                                <div className='grid gap-2 w-40 m-right' style={{ float: 'right' }}>
                                    <div>
                                        <Button onClick={() => updateStatus(2)} className='navy-secondary' size={'s'} rounded="true">Review</Button>
                                    </div>
                                </div>

                            </>
                            : null}

                        {/* Kondisi 2 */}
                        {currentUser?.role == 3 && reportIssue.status?.id == 2 ?
                            <>
                                <div className='grid gap-2 w-40 m-right' style={{ float: 'right' }}>
                                    <div>
                                        <Button onClick={() => updateStatus(3)} className='navy' size={'s'} rounded="true">Evaluate</Button>
                                    </div>
                                </div>
                            </>
                            : null}

                    </div>
                    <div style={{ margin: '80px 0px' }} />
                </div>

            </AppLayout>
        </>
    )
}

export default DetailReportIssue
