import Head from 'next/head'
import AppLayout from '../../components/Layout/App'
import Button from '../../components/Button'
import PopUp from '../../components/PopUp'
import Link from 'next/link'
import React, {useEffect, useRef, useState} from "react";
import {Alert, Snackbar} from "@mui/material";
import { useRouter } from "next/router"
import {BACKEND_URL} from "../../config";
import Form from "react-validation/build/form";
import TypeService from '../../services/type.service'
import CategoryService from '../../services/category.service'
import DeliverablesService from '../../services/deliverables.service';
import BrandPromiseService from '../../services/brandPromise.service';
import StandardPackageService from '../../services/standardPackage.service';
import UserService from '../../services/user.service'
import StatusService from '../../services/status.service'
import ProjectService from '../../services/project.service'
import authHeader from "../../services/auth-header";
import Input from "../../components/Input";
import ChooseFileIcon from "../../components/ChooseFileIcon";
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import Select from 'react-select';
import DateRange from "../../components/DateRange";
import axios from "axios";
import AuthService from "../../services/auth.service";


 function CreateProject() {
     //for project fields
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [type, setType] = useState("");
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(null);
    const [contractDuration, setContractDuration] = useState("");
    const [category, setCategory] = useState("");
    const [projectManager, setProjectManager] = useState("");
    const [deliverablesProjects, setDeliverablesProjects] = useState([]);
    const [status, setStatus] = useState("");
    const [listSponsors, setlistSponsors] = useState([]);
    const [schedule, setSchedule] = useState("");
    const [selectedFile, setSelectedFile] = useState(null);

    //for messages, auth , loading, etc.
    const [loading, setLoading] = useState(false);
    const [messageForm, setMessageForm] = useState("");
    const [message, setMessage] = useState({
        value: "",
        severity: ""
    });
    const [titleMessage, setTitleMessage] = useState("");
    const [descMessage, setDescMessage] = useState("");
    const [successful, setSuccessful] = useState(false);
    const [messageBrandPromise, setMessageBrandPromise] = useState("");
    const [messageContractLength, setMessageContractLength] = useState("")
    const [messageDeliverables, setMessageDeliverables] = useState("");
    const [openMessage, setOpenMessage] = useState(false);
    const [roleData, setRoleData] = useState([]);
    const [authorization, setAuthorization] = useState("");
    const [checkTitle, setCheckTitle] = useState(null);
    const [warningClass, setWarningClass] =useState("good")
    const [allowAccess, setAllowAccess] = useState(null);

    // List template
    const [listType, setListType] = useState([]);
    const [listCategory, setListCategory] = useState([]);
    const [listPM, setlistPM] = useState([]);
    const [listClient, setlistClient] = useState([]);
    const [listStatus, setListStatus] = useState([]);

    // List for brand promises and deliverables
    const [listDeliverables, setListDeliverables] = useState([]);
    const [listBrandPromises, setListBrandPromises] = useState([]);
    const [buttonPopUpDeliverables, setButtonPopUpDeliverables] = useState(false);
    const [buttonPopUpBrandPromises, setButtonPopUpBrandPromises] = useState(false);
    const [listPackages, setListPackages] = useState({});
    const [selectedPackages, setSelectedPackages] = useState({})
    const [selectedBrandPromises, setSelectedBrandPromises] = useState({})
    const [unfixDeliverables, setUnfixDeliverables] =useState([]);
    const [unfixSelectedBrandPromises, setUnfixSelectedBrandPromises] =useState({});
    const [unfixSelectedPackages, setUnfixSelectedPackages]= useState({});

    // for field validation
    const required = (value) => {
        if (!value) {
            return (
                <div className="alert alert-danger" role="alert">
                    This field is required!
                </div>
            );
        }
    };
    // for authorization handling
    const router = useRouter()
    useEffect(() => {
    const user = AuthService.getCurrentUser();
    if (user === null){
      router.push("/login")
    } else if (user.role > 2) {
        setAllowAccess(false);
      router.push("/401")
    } else {
        setAllowAccess(true);
      return axios.get(`${BACKEND_URL}/api/roles`, {headers:  authHeader()})
      .then((response) => {
        setRoleData(response.data.result);
        setAuthorization(user.role);
      });
    }
  }, []);

    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
        return;
        }
        setOpenMessage(false);
        };
    
    
    

    const openPopUpDelievrables = async (e) => {
        setButtonPopUpDeliverables(true)
        setUnfixDeliverables(deliverablesProjects)

    }

    const openPopUpBrandPromises = async (e) => {
        setButtonPopUpBrandPromises(true)
        setUnfixSelectedBrandPromises({...selectedBrandPromises})
        setUnfixSelectedPackages({...selectedPackages})
        console.log(selectedBrandPromises)
        console.log(selectedPackages)

    }

    const confirmDeliverables = async (e) => {
        setDeliverablesProjects(deliverablesProjects);
        if(deliverablesProjects.length!=0){
            setMessageDeliverables("Deliverables Added")
        }
        else{
            setMessageDeliverables("");
        }
        setButtonPopUpDeliverables(false)
    }

    const confirmBrandPromises = async (e) => {
        // setSelectedBrandPromises(selectedBrandPromises);
        // setSelectedPackages(selectedPackages);
        if(Object.keys(selectedBrandPromises).length!=0){
            setMessageBrandPromise("Brand Promises Added")
        }
        else{
            setMessageBrandPromise("")
        }
        setButtonPopUpBrandPromises(false);
    }

    const forceCloseDelievrables = async (e) => {
        if(deliverablesProjects!=null){
            setDeliverablesProjects(unfixDeliverables);
        }
        else{
            setDeliverablesProjects(null);
            setMessageDeliverables("");
        }
        setButtonPopUpDeliverables(false);
    }

    const forceCloseBrandPromises = async (e) => {
        if(selectedBrandPromises!=null){
            setSelectedBrandPromises({...unfixSelectedBrandPromises});
            setSelectedPackages({...unfixSelectedPackages});
        }
        else{
            setSelectedBrandPromises({});
            setSelectedBrandPromises({});
            setMessageBrandPromise("");
        }
        setButtonPopUpBrandPromises(false);
    }
 
  //handle submit
    const handleSubmit = async (e) => {
        setTitle(title)
        console.log(checkTitle)
        e.preventDefault();
        setMessageForm("")
        setMessage("")
        setLoading(true)
        let listSponsorTemp= []
        for (e of listSponsors){
            listSponsorTemp.push(e?.value);
        }

        let listDeliverablesTemp= []
        for (e of deliverablesProjects){
            listDeliverablesTemp.push(e?.value);
        }

        let listBrandPromiseTemp = []
        for (var key in selectedBrandPromises){
            if(selectedBrandPromises[key]==true){
                listBrandPromiseTemp.push(key)
            }
        }

        let listPackagesTemp = []
        for (var key in selectedPackages){
            if(selectedPackages[key]==true){
                listPackagesTemp.push(key)
            }
        }

        if(title.length>50){
            window.scrollTo(0,0);
        }
        
        else if(checkTitle!=null){
            setMessageForm("This project's title has been taken. Please set another title");
            setWarningClass("warning");
            window.scrollTo(0,0);
        }

        else if(description.length>255){
            window.scrollTo(0,0);
        }

        else if(contractDuration<0){
            window.scrollTo(0,0);
        }

        else if(type.value==null){
            setMessageForm("Please Select Project's Type!")
            setWarningClass("warning");
            window.scrollTo(0,0);
        }

        else if(category.value==null){
            setMessageForm("Please Select Project's Category!")
            setWarningClass("warning");
            window.scrollTo(0,0);
        }

        else if(status==""){
            setMessageForm("Please Select Project's Status!")
            setWarningClass("warning");
            window.scrollTo(0,0);
        }

        else if(startDate==null){
            setMessageForm("Please Select Project's Start Date!")
            setWarningClass("warning");
            window.scrollTo(0,0);
        }

        else if(projectManager.value==null){
            setMessageForm("Please Assign Project's Project Manager")
            setWarningClass("warning");
            window.scrollTo(0,0);
        }

        else if(listSponsorTemp.length==0){
            setMessageForm("Please Assign Project's Sponsor(s)")
            setWarningClass("warning");
            window.scrollTo(0,0); 
        }

        else{
            setWarningClass("good");
            let formData = new FormData()
            let proj = {
            title: title,
            description: description,
            type: type.value,
            category: category.value,
            status: status,
            pm: projectManager.value,
            start: startDate,
            duration: contractDuration,
            listSponsors: listSponsorTemp,
            deliverablesProjects: listDeliverablesTemp,
            brandPromises: listBrandPromiseTemp,
            standardPackages:listPackagesTemp

        }
        
        formData.append("project", proj)
        console.log(proj)
        // console.log(JSON.stringify(formData.getAll("project")[0]))
        window.scrollTo(0,0);

        const res = ProjectService.createProject(proj).then(
            (response) => {
                // Update image
                if (selectedFile !== null) {
                    let formData = new FormData();
                    formData.append("file", selectedFile);
                    ProjectService.updateImageProject(response.data?.result?.id, formData);
                }
                setMessage({
                    value: response.data?.message,
                    severity: "success"
                })
                setOpenMessage(true);
                setTitle("");
                setDescription("");
                setCategory(null);
                setType(null);
                setProjectManager(null);
                setStartDate(new Date);
                setContractDuration("");
                setlistSponsors(null);
                setSchedule("");
                setStatus(null);
                setDeliverablesProjects(null);
                setSelectedFile(null);
                setMessageDeliverables("");
                setSelectedBrandPromises({});
                setSelectedPackages({});
                setMessageBrandPromise("");
            },
            (error) => {
                const resMessage =
                    (error?.response &&
                        error?.response.data &&
                        error?.response.data.message) ||
                    error.message ||
                    error.toString();
                    setMessage({
                        value: resMessage,
                        severity: "error"
                      })
                    setOpenMessage(true)
                    setLoading(false);
            }
        )
        }
        
        
    };

    const setListStandardPackagesInternal = (index, value) => {
        setListPackages(prefdata => {
            prefdata[index]=value
            return prefdata
        })
        console.log(listPackages)
    }

    

    const onChangeTitle = (e) => {
        const title = e.target.value;
        setTitle(title);
        if(title.length>50){
            setTitleMessage("Title should only consist of <=50 characters")
        }
        else{
            setTitleMessage("")
        }
    };

    const onBlurTitle = (e) => {
        let unique = ProjectService.getProjectByTitle(title).then(
            (response) => {
                setCheckTitle(response.data.result)  
            }
        )
    }

    const onChangeDescription = (e) => {
        const description = e.target.value;
        setDescription(description);
        if(description.length>255){
            setDescMessage("Description should only consist of <=255 characters")
        }
        else{
            setDescMessage("")
        }
    };


    const onChangeDate = (s, e) => {
        setStartDate(s)
        setEndDate(e)
    };

    const onChangeContractDuration = (e) => {
        const contractDuration = e.target.value;
        setContractDuration(contractDuration);
        if(contractDuration<0){
            setMessageContractLength("Contract length must be positive");

        }else{
            setMessageContractLength("")
        }
    };

    const onChangeType = (e) => {
        setType(e);
    };

    const onChangeCategory = (e) => {
        setCategory(e);
    };

    const onChangeProjectManager = (e) => {
        setProjectManager(e);
    };

    const onChangeStatus = (e) => {
        setStatus(e.target.value);
    };

    const onChangeListSponsors = (e) => {
        let arr = []
        setlistSponsors(e);
    };

    const onChangeDeliverablesProjects = (e) => {
        let arr = []
        setDeliverablesProjects(e);
    };

    const onFileChange = e => {
        setSelectedFile(e.target.files[0])
    }

    const onChangeSelectedPackages = (e, id) => {
        console.log(e)
        setSelectedPackages(prefdata => {
            prefdata[id]=e.target.checked
            return {...prefdata}
        })
        let same=true
        let brandId = 0
        for (var key in listPackages){
            listPackages[key].forEach(element => {
                if(element.id==id){
                    brandId=key
                }
            })
        }
        console.log(brandId)
        listPackages[brandId].forEach(element => {
            if(element.id!=id && selectedPackages[element.id]!=e.target.checked){
                same=false
            }

        })
        if(same==true || e.target.checked){
            setSelectedBrandPromises(prefdata => {
                prefdata[brandId]=e.target.checked
                return {...prefdata}
            })
        }

        
    }

    const onChangeSelectedBrandPromises = (e, id) => {
        console.log(e)
        setSelectedBrandPromises(prefdata => {
            prefdata[id]=e.target.checked
            return {...prefdata}
        })
        listPackages[id].forEach(
            element => {
                setSelectedPackages(prefdata => {
                    prefdata[element.id]=e.target.checked
                    console.log(prefdata)
                    return {...prefdata}
                })
            }
        )
    }
    useEffect(() => {
        TypeService.getAllTypes().then(
            (response) => {
                console.log(response.data.result)

                setListType(response.data.result)
            },
            (error) => {
                console.log(error?.response)
            }
        );

        CategoryService.getAllCategories().then(
            (response) => {
                console.log(response.data.result)

                setListCategory(response.data.result)
            },
            (error) => {
                console.log(error?.response)
            }
        );

        DeliverablesService.getAllDeliverables().then(
            (response) => {
                console.log(response.data.result)

                setListDeliverables(response.data.result)
            },
            (error) => {
                console.log(error?.response)
            }
        );

        BrandPromiseService.getAllBrandPromises().then(
            (response) => {
                console.log(response.data.result)

                setListBrandPromises(response.data.result)
                response.data.result.forEach(element => {
                    StandardPackageService.getPackagesByBrandPromiseId(element.id).then(
                        (response) => {
                            setListStandardPackagesInternal(element.id,response.data.result)
            
                        },
                        (error) => {
                            console.log(error?.response)
                        }
                    );
                });
            },
            (error) => {
                console.log(error?.response)
            }
        );


        

        UserService.getAllProjectManagers().then(
            (response) => {
                console.log(response.data.result)

                setlistPM(response.data.result)
            },
            (error) => {
                console.log(error?.response)
            }
        );

        UserService.getAllSponsors().then(
            (response) => {
                console.log(response.data.result)

                setlistClient(response.data.result)
            },
            (error) => {
                console.log(error?.response)
            }
        );

        StatusService.getAllTypes().then(
            (response) => {
                console.log(response.data.result)
                setListStatus(response.data.result)
            },
            (error) => {
                console.log(error?.response)
            }
        );

    }, []);


    return (
        <>  {allowAccess!=null && allowAccess && <>
        <Head>
                <title>Create Project</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <AppLayout pageTitle={'Create Project'}>
                <div className="w-full p-1 m-auto bg-white rounded-lg">
                    <div className='w-3/4 m-auto color-white' >
                    <Form onSubmit={handleSubmit}>
                        {messageForm && (
                            <div className={warningClass} style={{ margin: '20px 0px auto'}}>
                                {messageForm}
                            </div>
                        )}
                        <br/>
                        <div className="grid grid-cols-2 gap-8">
                            <div>
                                <h6 style={{fontWeight:"normal"}}>Title</h6>
                                <Input size ="s" style={{ width: '460px'}}
                                    type="text"
                                    className="form-control"
                                    name="title"
                                    placeholder= "Input Title"
                                    value={title}
                                    onChange={ onChangeTitle }
                                    onBlur={ onBlurTitle }
                                    required
                                />
                                <p style={{color:"red", fontSize:"12px"}}>{titleMessage}</p>
                                <br></br>
                                <h6 style={{fontWeight:"normal"}}>Description</h6>
                                <Input size="s" style={{ width: '460px', height:'87px'}}
                                    type="textarea"
                                    className="form-control"
                                    name="description"
                                    placeholder="Input Description"
                                    value={description}
                                    onChange={ onChangeDescription }
                                    required
                                />
                                <p style={{color:"red", fontSize:"12px"}}>{descMessage}</p>
                                <br></br>
                                <br></br>
                                <h6 style={{fontWeight:"normal"}}>Project Type</h6>
                                {/*<p>{listType}</p>*/}
                                <Select size="s" style={{ width: '460px', height:'35px'}}
                                    name="type"
                                    options={listType.map((i) => { return {"value":i.id, "label":i.type } } )}
                                    classNamePrefix="select"
                                    onChange={onChangeType}
                                    value={type}
                                />
                                <br></br>
                                <h6 style={{fontWeight:"normal"}}>Start Date</h6>
                                <div>
                                    <DateRange callback={(startDate, endDate) => onChangeDate(startDate, endDate) }/>
                                </div>

                                <br></br>
                                <h6 style={{fontWeight:"normal"}}>Contract Length (days)</h6>
                                <Input size="s" style={{ width: '460px', height:'35px'}}
                                    type="number"
                                    className="form-control"
                                    name="contractDuration"
                                    placeholder="Input Contract Length"
                                    value={contractDuration}
                                    onChange={ onChangeContractDuration }
                                    required
                                />
                                <p style={{color:"red", fontSize:"12px"}}>{messageContractLength}</p>
                            </div>

                            <div>

                                <label htmlFor="file">
                                    <ChooseFileIcon  />
                                </label>
                                <div style={{ width: '460px'}}>
                                    <input accept="image/*" style={{display:"none"}} id="file" type="file" onChange={e => {
                                    onFileChange(e)
                                    }} />
                                </div>
                                {selectedFile?.name?
                                    <p style={{color:"green"}}>Upload {selectedFile?.name}</p>
                                     :
                                    <p style={{color: "red",fontSize:'12px'}}>You Haven't Choose an Image</p>
                                }
                                <br></br>
                                <h6 style={{fontWeight:"normal"}}>Category</h6>
                                <Select style={{ width: '460px', height:'35px'}}
                                    name="category"
                                    options={listCategory.map((i) => { return {"value":i.id, "label":i.category } } )}
                                    classNamePrefix="select"
                                    required
                                    onChange={onChangeCategory}
                                    value={category}
                                />
                                <br></br>
                                <h6 style={{fontWeight:"normal"}}>Project Manager</h6>
                                <Select style={{ width: '460px', height:'35px'}}
                                    name="projectManager"
                                    options={listPM.map((i) => { return {"value":i.id, "label":i.name } } )}
                                    classNamePrefix="select"
                                    required
                                    onChange={onChangeProjectManager}
                                    value={projectManager}
                                />
                            </div>
                        </div>

                        <br />
                        <hr />
                        <br />

                        <div className="grid grid-cols-4 gap-8">
                            <div>
                                <h6 style={{fontWeight:"normal"}}>Project's Deliverables</h6>
                            </div>
                            <div>
                                <Button type="button" className="navy" style={{width:"220px"}} size={'s'} rounded="true" onClick={() => openPopUpDelievrables()}>+ Add Deliverables</Button>
                                <br>
                                </br>
                            </div>
                            <br></br>
                            <p style={{color: "green",fontSize:'12px'}}>{ messageDeliverables}</p>
                            <div>
                                <h6 style={{fontWeight:"normal"}}>Brand Promise</h6>
                            </div>
                            <div>
                                <Button type="button" className="navy" style={{width:"220px"}} size={'s'} rounded="true" onClick={() => openPopUpBrandPromises()}>+ Add Brand Promise</Button>
                            </div>
                            <br></br>
                            <p style={{color: "green",fontSize:'12px'}}>{ messageBrandPromise}</p>
                        </div>
                        <div>
                        <br/>
                        <br></br>
                        <h6 style={{fontWeight:"normal"}}>Project Status</h6>
                        <br></br>
                        <FormControl component="fieldset">
                            <div>
                            <RadioGroup row aria-label="status" name="status1" value={status} onChange={onChangeStatus} >
                                {listStatus.map((item) => (
                                    <FormControlLabel value={item.id} control={<Radio />} label={item.status} />
                                ))}
                            </RadioGroup>
                            </div>
                           
                        </FormControl>


                        <br/>
                        <br></br>
                        <h6 style={{fontWeight:"normal"}}>Project Sponsor</h6>
                        <br></br>
                        <div>
                        <Select
                            isMulti
                            name="listSponsors"
                            options={listClient.map((i) => { return {"value":i.id, "label":i.name } } )}
                            className="basic-multi-select"
                            classNamePrefix="select"
                            onChange={onChangeListSponsors}
                            value={listSponsors}
                        />
                        <PopUp trigger={buttonPopUpDeliverables} setTrigger={setButtonPopUpDeliverables}>
                            <div className='popup-inner-add-deliverables'>
                                    <h5>Choose Deliverables</h5>
                                    <br></br>
                                    <Select
                                        isMulti
                                        name="deliverablesProjects"
                                        options={listDeliverables.map((i) => { return {"value":i.id, "label":i.name } } )}
                                        className="basic-multi-select"
                                        classNamePrefix="select"
                                        onChange={onChangeDeliverablesProjects}
                                        value={deliverablesProjects}
                    
                                    />
                                    <br></br>
                                    <div style={{display:'flex'}}>
                                        <Button type="button" className="navy spacingButtonRight" style={{width:"150px"}} size={'s'} rounded="true" onClick={() => confirmDeliverables()}>Confirm</Button>
                                    
                                        <Button type="button" color={'primary'} style={{width:"150px"}} size={'s'} rounded="true" onClick={() => forceCloseDelievrables()}>Cancel</Button>
                                    <br></br>
                                    </div>
                                 </div>  
                        </PopUp>
                        <PopUp trigger={buttonPopUpBrandPromises} setTrigger={setButtonPopUpBrandPromises}>
                            <div className='popup-inner-add-brand-promise'>
                                    <h6>Choose Brand Promises</h6>
                                    <br></br>
                                    <div>
                                        {listBrandPromises.length> 0 ?
                                         <table>
                                         <tr>
                                             <th className='thbpadd'>No. </th>
                                             <th className='thbpadd'>Category</th>  
                                             <th className='thbpadd'>Standard Package</th>   
                                             </tr>  
                                             {listBrandPromises?.map((prom,index) => (
                                         <tr data-index={index} className='thbpadd'>
                                             <td className='tdbpadd'>{index+1}</td>
                                             <td className='tdbpadd'><input checked={selectedBrandPromises[prom.id]} onChange = {(e) => {onChangeSelectedBrandPromises(e, prom.id)}} type="checkbox" name={prom.name}/> {prom.name}</td>
                                             <td className='tdbpadd'>
                                                 <ul>
                                                 {listPackages[prom.id]?.map((pkg) => (
                                                   <li><input type="checkbox" checked={selectedPackages[pkg.id]} onChange = {(e) => {onChangeSelectedPackages(e, pkg.id)}} name={pkg.name}/>  {pkg.name}</li>

                                                 ))}
                                                    </ul>
                                                </td>
                                         </tr>
                                         ))}
                                     </table>
                                         :
                                        <p>No Brand Promise</p>}
                                      
                                </div>
                                <br></br>
                                    <div style={{display:'flex'}}>
                                        <Button type="button" className="navy spacingButtonRight" style={{width:"150px"}} size={'s'} rounded="true" onClick={() => confirmBrandPromises()}>Confirm</Button>
                                    
                                        <Button type="button" color={'primary'}style={{width:"150px"}} size={'s'} rounded="true" onClick={() => forceCloseBrandPromises()}>Cancel</Button>
                                    <br></br>
                                    </div>
                                </div>
                        </PopUp>

                        </div>

                        <br />
                        <br />
                        </div>
                        <div className='grid grid-cols-2 gap-2 w-40 m-left' style={{ float: 'right' }}>
                            <div>
                                <Link href="/projects">
                                    <Button color={'secondary'} size={'s'} rounded="true">Back</Button>
                                </Link>
                            </div>
                            <div>
                                {/*<CheckButton style={{ display: "none" }} ref={checkBtn} />*/}
                                <Button type="submit" color={'primary'} size={'s'} rounded="true">Save</Button>
                            </div>
                        </div>
                    </Form>
                </div>
                <div style={{ margin: '120px 0px' }} />
                {(message.value != "") && (
                <Snackbar
                    anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                    open={openMessage}
                    onClose={handleCloseMessage}
                    autoHideDuration={3000}>
                    <Alert onClose={handleCloseMessage} variant="filled" severity={message.severity}>
                        {message.value}
                    </Alert>
                </Snackbar>
            )}
            </div>       
        </AppLayout>
        </>
        }
            
    </>
    )
}

export default CreateProject
