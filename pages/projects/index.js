import Head from 'next/head'
import Link from 'next/link'
import * as React from "react";
import {useEffect, useState} from "react";
import {useRouter} from "next/router"
import {DataGrid} from '@mui/x-data-grid';
import PopUp from '../../components/PopUp'
import AppLayout from '../../components/Layout/App'
import {Alert, Snackbar} from "@mui/material";
import Button from '../../components/Button'
import ProjectService from '../../services/project.service'
import AuthService from '../../services/auth.service'

function createData(id, title, type, category, startDate, endDate, status, pm, sponsor, action) {
    return {
        id,
        title,
        type,
        category,
        startDate,
        endDate,
        status,
        pm,
        sponsor,
        action
    };
}




const renderListSponsors = (params) => {
    return (
        <ul>
            {params.row.sponsor?.map(sp => <li>{sp}</li>)}
        </ul>
    )
}


function getRows(idRole, listProjects) {
    let arr = []
    if (idRole == 1 || idRole == 2) {
        for (const p of listProjects) {
            arr.push(createData(p.id, p.title, p.type?.type, p.category?.category,
                p.startDate, p.endDate, p.status?.status, p.projectManager?.name,
                p.sponsors?.map(sponsor => sponsor.name), p.id))
        }
    } else if (idRole == 3) {
        for (const p of listProjects) {
            arr.push(createData(p.id, p.title, p.type?.type, p.category?.category,
                p.startDate, p.endDate, p.status?.status, null,
                p.sponsors?.map(sponsor => sponsor.name), p.id))
        }
    } else {
        for (const p of listProjects) {
            arr.push(createData(p.id, p.title, p.type?.type, p.category?.category,
                p.startDate, p.endDate, p.status?.status, p.projectManager?.name, null, p.id))
        }
    }
    return arr
}

function ListProjects() {
    const router = useRouter()
    const [currentUser, setCurrentUser] = useState(null);
    const [buttonPopUpDelete, setButtonPopUpDelete] =useState(false);
    const [listProjects, setListProjects] = useState([]);
    const [deleteConfirmationup, setDeleteConfirmationUp] = useState(false);
    const [loading, setLoading] = useState(false);
    const [selectedId, setSelectedId] = useState(null);
    const [openMessage, setOpenMessage] =useState(false);
    const [message, setMessage] = useState({
        value:"",
        severity:""
    })

    function getColumns(idRole) {
        let columns = [
            {field: 'id', headerName: 'ID', width: 50, headerClassName: 'bg-slate-300'},
            {field: 'title', headerName: 'Title', width: 150},
            {field: 'type', headerName: "Type", width: 100},
            {field: 'category', headerName: "Category", width: 150},
            {field: 'startDate', headerName: 'Start Date', width: 120},
            {field: 'endDate', headerName: 'End Date', width: 120},
            {field: 'status', headerName: 'Status', width: 120},
        ]
        // exclude pm
        if (idRole != 3) {
            columns.push({field: 'pm', headerName: 'Project Manager', width: 150})
        }
    
        // exclude sponsor
        if (idRole != 4) {
            columns.push(
                {
                    field: 'sponsor',
                    headerName: 'Sponsor',
                    width: 150,
                    renderCell: renderListSponsors,
                }
            )
        }
        if (idRole == 1 || idRole == 2) {
            columns.push(
                {
                    field: 'action',
                    headerName: 'Action',
                    width: 120,
                    sortable: false,
                    renderCell: renderActionButtonAdmin,
                    disableClickEventBubbling: true,
                    disableColumnMenu: true
                }
            )
        } else {
            columns.push(
                {
                    field: 'action',
                    headerName: 'Action',
                    sortable: false,
                    width: 120,
                    renderCell: renderActionButtonNonAdmin,
                    disableClickEventBubbling: true,
                    disableColumnMenu: true
                }
            )
        }
    
        return columns
    }
    
    
    
    const renderActionButtonNonAdmin = (params) => {
        return (
            <Link href={`/projects/${encodeURIComponent(params.row.id)}`}>
                <Button size={'s'} className='navy' rounded="true">Detail</Button>
            </Link>
        )
    }
    const renderActionButtonAdmin = (params) => {
        return (
            <div className='flex'>
                <Link href={`/projects/${encodeURIComponent(params.row.id)}`}>
                    <Button className="buttonGambar" rounded="true">
                        <img style={{marginRight: 30}} className={'inline my-auto'} width={24} height={24}
                             src={'/icon/edit-icon.svg'}/>
                    </Button>
                </Link>
                <Button className="buttonGambar" rounded="true" onClick={() => {setButtonPopUpDelete(true); setSelectedId(params.row.id)}}>
                    <img className={'inline my-auto'} width={24} height={24} src={'/icon/delete-icon.svg'}/>
                </Button>
            </div>
        )
    }

    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
        return;
        }
        setOpenMessage(false);
        };

    const confirmDelete = async () => {
        setLoading(true)
        setMessage("")
        ProjectService.deleteProject(selectedId).then((response) => {
            setMessage({
                value: response.data,
                severity: "success"
            })
            let temp = []
            listProjects.forEach(element => {
                if(element.id!=selectedId){
                    temp.push(element)
                }
            })
            setListProjects(temp);
            setOpenMessage(true);
            setButtonPopUpDelete(false);
        }, 
        (error) => {
            const resMessage =
                (error?.response &&
                    error?.response.data &&
                    error?.response.data.message) ||
                error.message ||
                error.toString();
                setMessage({
                    value: resMessage,
                    severity: "error"
                  })
                setOpenMessage(true)
                setLoading(false);
        }

        )
        
    }

    useEffect(() => {
        if (!router.isReady) return;

        setLoading(true);

        const user = AuthService.getCurrentUser()
        if (user == null) {
            router.push('/login')
        } else {
            setCurrentUser(user)
            ProjectService.getAllProjects().then(
                (response) => {
                    setListProjects(response.data)
                },
                (error) => {
                    if (error.response == undefined) {
                        router.push('/503')
                    }
                    router.push('/404')
                }
            );
        }

        

        setLoading(false);
    }, [router.isReady]);

    if (currentUser == null) return <></>

    return (
        <>
            <Head>
                <title>List Projects</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            <AppLayout pageTitle={'List Project, ' + currentUser?.name}>
                {/* Admin or super admin */}
                {currentUser?.role == 1 || currentUser?.role == 2 ?
                    <div className="grid grid-cols-6 gap-1">
                        <div className='' style={{float: 'left'}}>
                            <Link href={`/projects/create`}>
                                <Button className="navy" size="s"
                                        rounded="true" style={{width:"180px"}}>+ Create Project</Button>
                            </Link>
                        </div>
                        <div className='' style={{float: 'left'}}>
                            <Link href={`/projects/holiday-management`}>
                                <Button className="navy" size="s"
                                        rounded="true" style={{width:"260px"}}>Manage Holiday Calendar</Button>
                            </Link>
                        </div>
                    </div>
                    :
                    null
                }
                <br/>
                <div style={{height: 500, width: '100%'}}>
                    <DataGrid
                        style={{float: 'center', paddingLeft: "1em", fontSize: '12px', fontWeight: "bold"}}
                        rows={getRows(currentUser?.role, listProjects)}
                        columns={getColumns(currentUser?.role)}
                        pageSize={7}
                        // rowsPerPageOptions={[10]}
                        disableSelectionOnClick
                        disableMultipleSelection={true}
                        // checkboxSelection
                        className="bg-white rounded-lg"
                    />
                </div>
                <PopUp trigger={buttonPopUpDelete} setTrigger={setButtonPopUpDelete}>
                            <div className='popup-inner-delete-project'>
                                    <h6 style={{textAlign:"center"}}>Are you sure you want to delete this project? </h6>
                                    <br></br>
                                    <div style={{display:'flex', justifyContent:"center"}}>
                                        <Button type="button" className="red spacingButtonRight" style={{width:"150px"}} size={'s'} rounded="true" onClick={() => confirmDelete()}>Yes</Button>
                                    
                                        <Button type="button" color={'primary'} style={{width:"150px"}} size={'s'} rounded="true" onClick={() => setButtonPopUpDelete(false)}>No</Button>
                                    <br></br>
                                    </div>
                                 </div>  
                        </PopUp>
                        {(message.value != "") && (
                <Snackbar
                    anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                    open={openMessage}
                    onClose={handleCloseMessage}
                    autoHideDuration={4000}>
                    <Alert onClose={handleCloseMessage} variant="filled" severity={message.severity}>
                        {message.value}
                    </Alert>
                </Snackbar>
            )}
            </AppLayout>
        </>
    )
}


export default ListProjects
