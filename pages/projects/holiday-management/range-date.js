import React, {useEffect, useState} from "react";
import Head from "next/head";
import {useRouter} from "next/router";
import {DataGrid} from "@mui/x-data-grid";
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import {Alert, Snackbar} from "@mui/material";
import Select from "react-select";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogActions from "@mui/material/DialogActions";

import AppLayout from "../../../components/Layout/App";
import Button from "../../../components/Button";
import DateRange from "../../../components/DateRange";
import Input from "../../../components/Input";
import AuthService from "../../../services/auth.service";
import ScheduleService from "../../../services/schedule.service";

const style = {
    position: 'absolute',
    top: '40%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '60%',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

function createData(id, name, date, type) {
    return {
        id,
        name,
        date,
        type,
    };
}

function getColumns() {
    return [
        {field: 'id', headerName: 'ID', width: 100},
        {field: 'name', headerName: 'Holiday Name', width: 400},
        {
            field: 'date', headerName: 'Date', width: 300,
            valueFormatter: (params) => params.value == null ? null : new Date(params.value).toLocaleDateString()
        },
        {field: 'type', headerName: "Type", width: 300},
    ]
}

export default function HolidayManagement() {
    const router = useRouter()

    const [currentUser, setCurrentUser] = useState(null);
    const [rows, setRows] = useState([]);
    const [selectionModel, setSelectionModel] = useState([]);

    const updateListSchedule = () => {
        ScheduleService.getAllHolidays().then(
            // use date as key in order to be more efficient
            (response) => {
                setRows(response.data)
            }
        )
    }

    useEffect(() => {
        if (!router.isReady) return;

        setLoading(true);
        const user = AuthService.getCurrentUser()
        if (user == null) {
            router.push('/login')
        } else {
            if (user.role == 4) {
                router.push(`/projects`)
                return
            }
        }
        setCurrentUser(user)
        updateListSchedule()

        /* Call this anually */
        // ScheduleService.getAllPublicIndonesiaHolidays().then(
        //     (response) => {
        //         let arr = []
        //         response.data.forEach(function (d, index) {
        //             ScheduleService.postHoliday(
        //                 {
        //                     id: index,
        //                     name: d.holiday_name,
        //                     date: new Date(d.holiday_date),
        //                     type: d.is_national_holiday ? "National":null
        //                 }
        //             )
        //         });
        //         setRows(arr)
        //     },
        //     (error) => {
        //         console.log(error)
        //         setErrorMessage(error);
        //         setLoading(false)
        //     }
        // )

    }, [router.isReady]);

    // Modal
    const [open, setOpen] = React.useState(false);
    const handleClose = () => {
        setOpen(false)
    };

    const [listHolidayTypes, setListHolidayTypes] = useState(
        [
            {label: "National", value: "National"},
            {label: "Province", value: "Province"},
            {label: "Work", value: "Work"}
        ]
    );
    const [holidayName, setHolidayName] = useState("");
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(null);
    const [holidayType, setHolidayType] = useState("");

    // Loading
    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState("");
    const [errorMessage, setErrorMessage] = useState("");
    const [openMessage, setOpenMessage] = React.useState(false);
    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenMessage(false);
    };
    const [openDeleteDialog, setOpenDeleteDialog] = React.useState(false);
    const handleCloseDialog = () => {
        setOpenDeleteDialog(false);
    };

    const handleYesDialog = async () => {
        setLoading(true)
        for (const id of selectionModel) {
            let res = await ScheduleService.deleteHolidays(id)
        }
        setLoading(false)
        setMessage("Holidays Successfully Deleted")
        setOpenMessage(true)
        setOpenDeleteDialog(false)
        updateListSchedule()
    };

    if (currentUser == null) return <></>

    return (
        <>
            <Head>
                <title>Home</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            {/* Add holidays modal */}
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <h4>Add Holiday Form</h4>

                    <h6 style={{fontWeight: "normal"}}>Holiday Name</h6>
                    <Input size="s" style={{width: '460px'}}
                           type="text"
                           className="form-control"
                           name="name"
                           placeholder="Input Holiday Name"
                           value={holidayName}
                           onChange={(e) => setHolidayName(e.target.value)}
                           required
                    />
                    <br></br>
                    <br></br>

                    <h6 style={{fontWeight: "normal"}}>Date</h6>
                    <div>
                        <DateRange isRange={true} callback={(start, end) => {
                            setStartDate(start)
                            setEndDate(end)
                        }}/>
                    </div>
                    <br></br>

                    <h6 style={{fontWeight: "normal"}}>Holiday Type</h6>
                    <Select size="s" style={{width: '460px', height: '35px'}}
                            name="type"
                            options={listHolidayTypes.map((i) => {
                                return {"value": i.value, "label": i.label}
                            })}
                            classNamePrefix="select"
                            onChange={(e) => setHolidayType(e)}
                            value={holidayType}
                    />
                    <br></br>
                    <br></br>
                    <br></br>
                    <div className='grid gap-2 w-40 m-left' style={{float: 'right'}}>
                        <div>
                            <Button type="submit" color={'primary'} size={'s'} rounded="true"
                                    onClick={() => {
                                        let startDateRange = startDate
                                        let endDateRange = endDate
                                        if (endDateRange == null) {
                                            endDateRange = startDateRange
                                                .setTime(startDateRange.getTime() + (1*60*60*1000));
                                        }
                                        for (let d = startDate; d <= endDateRange ; d.setDate(d.getDate() + 1)) {
                                            ScheduleService.postHoliday(
                                                {
                                                    name: holidayName, date: d,
                                                    type: holidayType.value
                                                }).then(
                                                (response) => {
                                                    setMessage(response.data)
                                                    setLoading(false)
                                                    setOpenMessage(true)
                                                    updateListSchedule()
                                                },
                                                (error) => {
                                                    const resMessage =
                                                        (error.response &&
                                                            error.response.data &&
                                                            error.response.data.message) ||
                                                        error.message ||
                                                        error.toString();
                                                    setLoading(false);
                                                    setErrorMessage(resMessage);
                                                    setOpenMessage(true)
                                                }
                                            )
                                        }
                                        setHolidayName("")
                                    }}
                            >Submit</Button>
                        </div>
                    </div>
                </Box>
            </Modal>

            {/* list table */}
            <AppLayout pageTitle="Holiday Management">
                {currentUser?.role == 1 || currentUser?.role == 2 ?
                    <>
                        <div className='grid grid-cols-2 gap-2 ' style={{'float': 'right'}}>
                            <div>
                                <Button color={'secondary'} size={'s'} rounded="true"
                                        onClick={() => {
                                            router.push("/projects")
                                        }}>Back</Button>
                            </div>
                            <div>
                                <Button
                                    style={{display: selectionModel.length !== 0 ? "block" : "none"}}
                                    isDelete type="submit" color={'primary'} size={'s'} rounded="true"
                                    onClick={
                                        () => {
                                            setOpenDeleteDialog(true)
                                        }
                                    }>Delete Selected Holiday</Button>
                            </div>
                        </div>
                        <div className="grid grid-cols-8 gap-2">
                            <div className='' style={{float: 'left'}}>

                                <Button color={'primary'} size="s" style={{background: "#052A49", width: "180px"}}
                                        rounded="true" onClick={() => setOpen(true)}>+ Add Holiday</Button>
                                <br/>
                                <br/>
                            </div>
                        </div>
                    </>
                    :
                    null
                }
                <div style={{height: 500, width: '100%'}}>
                    <DataGrid
                        style={{float: 'center', paddingLeft: "1em", fontSize: '12px', fontWeight: "bold"}}
                        rows={rows}
                        columns={getColumns()}
                        pageSize={7}
                        // disableSelectionOnClick
                        // disableMultipleSelection={true}
                        checkboxSelection
                        className="bg-white rounded-lg"
                        onSelectionModelChange={(newSelectionModel) => {
                            setSelectionModel(newSelectionModel)
                        }}
                    />

                    <Dialog
                        open={openDeleteDialog}
                        onClose={handleCloseDialog}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">
                            {"Are you sure?"}
                        </DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                By pressing the 'Yes' button, all selected rows will be deleted.
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={handleCloseDialog}>No</Button>
                            <Button isDelete onClick={handleYesDialog} autoFocus>
                                Yes
                            </Button>
                        </DialogActions>
                    </Dialog>

                    {(message || errorMessage) && (
                        <Snackbar
                            anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                            open={openMessage}
                            onClose={handleCloseMessage}
                            autoHideDuration={1000}>
                            <Alert onClose={handleCloseMessage}
                                   variant="filled"
                                   severity={message ? "success"
                                       : errorMessage ? "alert"
                                           : ""}>{message || errorMessage}
                            </Alert>
                        </Snackbar>
                    )}
                </div>
            </AppLayout>
        </>

    )

}
