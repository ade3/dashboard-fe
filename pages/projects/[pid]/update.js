import React, {useEffect, useState} from "react"
import Link from 'next/link'
import Head from 'next/head'
import {useRouter} from "next/router"
import Radio from '@mui/material/Radio'
import RadioGroup from '@mui/material/RadioGroup'
import FormControlLabel from '@mui/material/FormControlLabel'
import FormControl from '@mui/material/FormControl'
import {Alert, Snackbar} from "@mui/material"
import Form from "react-validation/build/form"
import Select from 'react-select'

import AppLayout from '../../../components/Layout/App'
import Button from '../../../components/Button'
import DateRange from "../../../components/DateRange"
import Input from "../../../components/Input"
import ChooseFileIcon from "../../../components/ChooseFileIcon"
import TypeService from '../../../services/type.service'
import CategoryService from '../../../services/category.service'
import UserService from '../../../services/user.service'
import StatusService from '../../../services/status.service'
import ProjectService from '../../../services/project.service'
import AuthService from "../../../services/auth.service"

function UpdateProject(props) {
    const router = useRouter()
    const {pid} = router.query
    const [currentUser, setCurrentUser] = useState(null)
    const [title, setTitle] = useState("")
    const [description, setDescription] = useState("")
    const [type, setType] = useState("")
    const [startDate, setStartDate] = useState(new Date())
    const [contractDuration, setContractDuration] = useState("")
    const [category, setCategory] = useState("")
    const [projectManager, setProjectManager] = useState("")
    const [status, setStatus] = useState("")
    const [listSponsors, setlistSponsors] = useState([])
    const [urlMockUp, setUrlMockUp] = useState("")
    const [selectedFile, setSelectedFile] = useState(null)

    const [loading, setLoading] = useState(false)
    const [openMessage, setOpenMessage] = useState(false);
    const [message, setMessage] = useState({
        value: "",
        severity: ""
    });
    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenMessage(false);
    };
    const [imageMessage, setImageMessage] = useState("You haven't choose an image!")
    const maxAllowedImageSize = 5 * 1024 * 1024; // 5MB

    // List template
    const [listType, setListType] = useState([])
    const [listCategory, setListCategory] = useState([])
    const [listPM, setlistPM] = useState([])
    const [listClient, setlistClient] = useState([])
    const [listStatus, setListStatus] = useState([])

    const handleSubmit = async (e) => {
        e.preventDefault()
        setLoading(true)

        let formData = new FormData()
        let proj = {
            title: title,
            description: description,
            type: type.value,
            category: category.value,
            status: status,
            pm: projectManager.value,
            start: startDate,
            duration: contractDuration,
            listSponsors: listSponsors.map(e => e.value),
            url: urlMockUp
        }
        formData.append("project", proj)

        const res = ProjectService.updateProject(pid, proj).then(
            (response) => {
                // Update image
                if (selectedFile !== null) {
                    let formData = new FormData()
                    formData.append("file", selectedFile)
                    ProjectService.updateImageProject(response.data?.result?.id, formData)
                }
                setMessage({
                    value: response.data.message,
                    severity: "success"
                })
                setOpenMessage(true)
                setTimeout(() => router.push(`/projects/${pid}`), 2000)

            },
            (error) => {
                const resMessage =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString()
                setMessage({
                    value: resMessage,
                    severity: "error"
                })
                setOpenMessage(true)
            }
        )
        setLoading(false)
    }

    const onChangeTitle = (e) => {
        const title = e.target.value
        setTitle(title)
    }

    const onChangeDescription = (e) => {
        const description = e.target.value
        setDescription(description)
    }

    const onChangeDate = (s, e) => {
        setStartDate(s)
    }

    const onChangeContractDuration = (e) => {
        const contractDuration = e.target.value
        setContractDuration(contractDuration)
    }

    const onChangeType = (e) => {
        setType(e)
    }

    const onChangeCategory = (e) => {
        setCategory(e)
    }

    const onChangeProjectManager = (e) => {
        setProjectManager(e)
    }

    const onChangeStatus = (e) => {
        setStatus(e.target.value)
    }

    const onChangeListSponsors = (e) => {
        setlistSponsors(e.map(e => {
            return {value: e.value, label: e.label}
        }))
    }

    const onFileChange = e => {
        if (e.target.files[0].size > maxAllowedImageSize) {
            setImageMessage("Maximum image upload size is 5MB!")
            return
        }
        setSelectedFile(e.target.files[0])
    }

    useEffect(() => {
        if (!router.isReady) return
        if (!parseInt(pid)) router.push('/404')

        setLoading(true)
        const user = AuthService.getCurrentUser()
        if (user == null) {
            router.push('/login')
        } else {
            if (user.role == 4) {
                router.push(`/projects/${pid}`)
                return
            } else if (user.role == 1 || user.role == 2) {
                Promise.all([UserService.getAllProjectManagers(), UserService.getAllSponsors()]).then(
                    function ([pms, sponsors]) {
                        setlistPM(pms.data.result)
                        setlistClient(sponsors.data.result)
                    }
                ).catch(
                    (error) => {
                        console.error(error)
                    }
                )
            }
        }
        setCurrentUser(user)

        Promise.all([ProjectService.getProject(pid), TypeService.getAllTypes(), CategoryService.getAllCategories(),
            StatusService.getAllTypes()]).then(
            function ([proj, types, categories, statuses]) {
                let p = proj.data
                setTitle(p.title)
                setDescription(p.description)
                setContractDuration(p.contractDuration)
                setStartDate(p.startDate)
                setUrlMockUp(p.urlMockUp)

                setType({value: p.type.id, label: p.type.type})
                setCategory({value: p.category.id, label: p.category.category})
                setProjectManager({value: p.projectManager.id, label: p.projectManager.name})
                setStatus(p.status.id)
                setlistSponsors(p.sponsors.map(it => {
                    return {
                        value: it.id,
                        label: it.name
                    }
                }))

                setListType(types.data.result)
                setListCategory(categories.data.result)
                setListStatus(statuses.data.result)
            }
        ).catch(
            (error) => {
                if (error.response == undefined) {
                    router.push('/503')
                }
                router.push('/404')
            }
        )
        setLoading(false)
    }, [router.isReady])

    if (currentUser == null) return <></>

    return (
        <>
            <Head>
                <title>Update Project</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            <AppLayout pageTitle={'Update Project'}>
                <div className="w-full p-1 m-auto bg-white rounded-lg">
                    <div className='w-3/4 m-auto color-white'>
                        <Form onSubmit={handleSubmit}>
                            <br/>
                            <div className="grid grid-cols-2 gap-8">
                                <div>
                                    <h6 style={{fontWeight: "normal"}}>Title</h6>
                                    <Input size="s" style={{width: '460px'}}
                                           type="text"
                                           className={"form-control"}
                                           name="title"
                                           placeholder="Input Title"
                                           value={title}
                                           onChange={onChangeTitle}
                                           required
                                    />
                                    <br></br>
                                    <br></br>
                                    <h6 style={{fontWeight: "normal"}}>Description</h6>
                                    <Input size="s" style={{width: '460px', height: '87px'}}
                                           type="textarea"
                                           className={"form-control"}
                                           name="description"
                                           placeholder="Input Description"
                                           value={description}
                                           onChange={onChangeDescription}
                                           required
                                    />
                                    <br></br>
                                    <br></br>
                                    <h6 style={{fontWeight: "normal"}}>Project Type</h6>
                                    <Select size="s" style={{width: '460px', height: '35px'}}
                                            name="type"
                                            value={type}
                                            options={listType.map((i) => {
                                                return {"value": i.id, "label": i.type}
                                            })}
                                            classNamePrefix="select"
                                            onChange={onChangeType}
                                    />
                                    <br></br>
                                    <h6 style={{fontWeight: "normal"}}>Start Date</h6>
                                    <div>
                                        <DateRange
                                                   className={"form-control"}
                                                   callback={(startDate, endDate) => onChangeDate(startDate, endDate)}/>
                                    </div>

                                    <br></br>
                                    <h6 style={{fontWeight: "normal"}}>Contract Length (days)</h6>
                                    <Input size="s" style={{width: '460px', height: '35px'}}
                                           type="number"
                                           className={"form-control"}
                                           name="contractDuration"
                                           placeholder="Input Contract Length"
                                           value={contractDuration}
                                           onChange={onChangeContractDuration}
                                           required
                                    />
                                </div>

                                <div>
                                    <label htmlFor="file">
                                        <ChooseFileIcon/>
                                    </label>
                                    <div style={{width: '460px'}}>
                                        <input accept="image/*" style={{display: "none"}} id="file" type="file"
                                               onChange={e => {
                                                   onFileChange(e)
                                               }}/>
                                    </div>
                                    {selectedFile?.name ?
                                        <p>Upload {selectedFile?.name}</p>
                                        :
                                        <p style={{color: "red", fontSize: '12px'}}>{imageMessage}</p>
                                    }
                                    <br></br>
                                    <h6 style={{fontWeight: "normal"}}>Category</h6>
                                    <Select style={{width: '460px', height: '35px'}}
                                            name="category"
                                            value={category}
                                            options={listCategory.map((i) => {
                                                return {"value": i.id, "label": i.category}
                                            })}
                                            classNamePrefix="select"
                                            required
                                            onChange={onChangeCategory}
                                    />
                                    <br></br>
                                    <h6 style={{fontWeight: "normal"}}>Project Manager</h6>
                                    <Select style={{width: '460px', height: '35px'}}
                                            name="projectManager"
                                            value={projectManager}
                                            isDisabled={currentUser?.role == 3}
                                            options={listPM.map((i) => {
                                                return {"value": i.id, "label": i.name}
                                            })}
                                            classNamePrefix="select"
                                            required
                                            onChange={onChangeProjectManager}
                                    />
                                </div>
                            </div>
                            <br></br>

                            <h6 style={{fontWeight: "normal"}}>Project Status</h6>
                            <FormControl component="fieldset">
                                <RadioGroup row aria-label="status" name="status1"
                                            value={status}
                                            onChange={onChangeStatus}
                                >
                                    {listStatus.map((item) => (
                                        <FormControlLabel value={item.id} control={<Radio/>} label={item.status}/>
                                    ))}
                                </RadioGroup>
                            </FormControl>
                            <br></br>

                            <br></br>
                            <h6 style={{fontWeight: "normal"}}>Project Sponsor</h6>
                            <Select
                                isMulti
                                name="listSponsors"
                                value={listSponsors}
                                options={listClient.map((i) => {
                                    return {"value": i.id, "label": i.name}
                                })}
                                className="basic-multi-select"
                                classNamePrefix="select"
                                onChange={onChangeListSponsors}
                                isDisabled={currentUser.role == 3 ? true: false}
                            />

                            <br></br>
                            <h6 style={{fontWeight: "normal"}}>URL Mockup</h6>
                            <Input size="s" style={{width: '460px'}}
                                   type="text"
                                   className={"form-control"}
                                   name="urlMockup"
                                   placeholder="Input URL Mockup"
                                   value={urlMockUp}
                                   onChange={(e) => {
                                       setUrlMockUp(e.target.value)
                                   }}
                            />
                            <br></br>
                            <br></br>


                            <br/>
                            <hr/>
                            <br/>

                            <div className='grid grid-cols-2 gap-2 w-40 m-left' style={{float: 'right'}}>
                                <div>
                                    <Link href={`/projects/${pid}`}>
                                        <Button color={'secondary'} size={'s'} rounded="true">Back</Button>
                                    </Link>
                                </div>
                                <div>
                                    {/*<CheckButton style={{ display: "none" }} ref={checkBtn} />*/}
                                    <Button type="submit" color={'primary'} size={'s'} rounded="true">Save</Button>
                                </div>
                            </div>
                        </Form>

                        {(message.value != "") && (
                            <Snackbar
                                anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                                open={openMessage}
                                onClose={handleCloseMessage}
                                autoHideDuration={2000}>
                                <Alert onClose={handleCloseMessage} variant="filled" severity={message.severity}>
                                    {message.value}
                                </Alert>
                            </Snackbar>
                        )}
                    </div>

                    <div style={{margin: '120px 0px'}}/>
                </div>
            </AppLayout>
        </>
    )
}

export default UpdateProject
