import React, {useEffect, useState} from "react";
import dynamic from "next/dynamic";
import Head from "next/head";
import moment from 'moment'
import {useRouter} from "next/router";
const ApexCharts = dynamic(() => import("apexcharts"), {ssr: false});
const ReactApexChart = dynamic(() => import("react-apexcharts"), {ssr: false});

import AppLayout from "../../../../components/Layout/App";
import AuthService from "../../../../services/auth.service"
import ProjectService from "../../../../services/project.service"

export default function ScheduleGantt() {
    const router = useRouter()
    const {pid} = router.query

    const [currentUser, setCurrentUser] = useState(null);

    const [loading, setLoading] = useState(false);
    const [timeline, setTimeline] = useState(null)

    useEffect(() => {
        if (!router.isReady) return;

        setLoading(true);
        const user = AuthService.getCurrentUser()
        if (user == null) {
            router.push('/login')
        }
        setCurrentUser(user)

        let newData = {}
        ProjectService.getProject(pid).then(
            (response) => {
                let listEvents = response?.data?.schedule?.events
                let dataPlan = []
                let dataActual = []

                if (listEvents != null) {
                    for (const event of listEvents) {
                        if (event.startDatePlan && event.endDatePlan) {
                            dataPlan.push(
                                {
                                    x: event.name,
                                    y: [
                                        new Date(event.startDatePlan).getTime(),
                                        new Date(event.endDatePlan).getTime(),
                                    ]
                                }
                            )
                        }
                        if (event.startDateActual && event.endDateActual) {
                            dataActual.push(
                                {
                                    x: event.name,
                                    y: [
                                        new Date(event.startDateActual).getTime(),
                                        new Date(event.endDateActual).getTime(),
                                    ]
                                }
                            )
                        }
                    }
                }
                newData.series = [
                    {
                        name: 'Plan',
                        data: dataPlan
                    },
                    {
                        name: 'Actual',
                        data: dataActual
                    },
                ]
                newData.options = {
                    chart: {
                        height: 'auto',
                            type: 'rangeBar'
                    },
                    plotOptions: {
                        bar: {
                            horizontal: true,
                        }
                    },
                    dataLabels: {
                        enabled: true,
                        formatter: function(val) {
                            console.log(val)
                            let a = moment(val[0])
                            let b = moment(val[1])
                            let diff = b.diff(a, 'days')
                            return diff + 1 + (diff > 0 ? ' days' : ' day')
                        },
                        background: {
                            enabled: true,
                            foreColor: 'black',
                        }
                    },
                    xaxis: {
                        type: 'datetime'
                    },
                    stroke: {
                        width: 1
                    },
                    fill: {
                        type: 'gradient',
                        gradient: {
                            shade: 'light',
                            type: 'vertical',
                            shadeIntensity: 0.25,
                            gradientToColors: undefined,
                            inverseColors: true,
                            opacityFrom: 1,
                            opacityTo: 1,
                            stops: [50, 0, 100, 100]
                        }
                    },
                    legend: {
                        position: 'top',
                            horizontalAlign: 'left'
                    }
                }
                setTimeline(newData)
            },
            (error) => {
                if (error.response == undefined) {
                    router.push('/503')
                }
                router.push('/404')
            }
        )
    }, [router.isReady]);

    if (currentUser == null || timeline == null) return <></>

    return (
        <>
            <Head>
                <title>Home</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>

            <AppLayout pageTitle="Timeline Charts">
                <div id="chart">
                    <ReactApexChart options={timeline.options} series={timeline.series} type="rangeBar"
                                    height={'auto'}/>
                </div>
            </AppLayout>
        </>
    )
}
