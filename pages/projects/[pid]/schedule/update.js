import * as React from 'react';
import Head from 'next/head'
import {useState, useEffect, useCallback} from "react";
import {useRouter} from "next/router"
import AddIcon from '@mui/icons-material/Add';
import {DataGrid} from '@mui/x-data-grid';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import {Alert, Snackbar} from "@mui/material";

import AppLayout from '../../../../components/Layout/App'
import Button from '../../../../components/Button'
import AuthService from '../../../../services/auth.service'
import ProjectService from '../../../../services/project.service'
import ScheduleService from '../../../../services/schedule.service'


function createData(id, name, startDatePlan, mandaysPlan, endDatePlan, startDateActual, mandaysActual, endDateActual, action) {
    return {
        id, name, startDatePlan, mandaysPlan, endDatePlan, startDateActual, mandaysActual, endDateActual, action
    };
}

function UpdateSchedule() {
    const router = useRouter()
    const {pid} = router.query

    const [currentUser, setCurrentUser] = useState(null);
    const [rows, setRows] = useState([]);
    const [editRowsModel, setEditRowsModel] = useState({});
    const [holidaysDateList, setHolidaysDateList] = useState([])
    const [loading, setLoading] = useState(false);
    const [openMessage, setOpenMessage] = useState(false);
    const [message, setMessage] = useState({
        value: "",
        severity: ""
    });
    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenMessage(false);
    };

    const [openBackDialog, setOpenBackDialog] = React.useState(false);
    const handleClickBack = () => {
        setOpenBackDialog(true);
    };
    const handleCloseDialog = () => {
        setOpenBackDialog(false);
    };

    const handleYesDialog = () => {
        router.push(`/projects/${pid}`)
    };

    useEffect(() => {
        if (!router.isReady) return;
        if (!parseInt(pid)) router.push('/404')

        setLoading(true);
        const user = AuthService.getCurrentUser()
        if (user == null) {
            router.push('/login')
        } else {
            if (user.role==2 || user.role==4) {
                router.push(`/projects/${pid}`)
                return
            }
        }
        setCurrentUser(user)

        ProjectService.getProject(pid).then(
            (response) => {
                let listEvents = response?.data?.schedule?.events

                if (listEvents != null) {
                    setRows(listEvents.map((ev, index) => {
                        return createData(index, ev.name, ev.startDatePlan, ev.mandaysPlan, ev.endDatePlan,
                            ev.startDateActual, ev.mandaysActual, ev.endDateActual, index)
                    }))
                }
            },
            (error) => {
                if (error.response == undefined) {
                    router.push('/503')
                }
                router.push('/404')
            }
        )

        ScheduleService.getAllHolidays(pid).then(
            (response) => {
                setHolidaysDateList(response.data.map(e => new Date(e.date)))
            },
            (error) => {
                if (error.response == undefined) {
                    router.push('/503')
                }
                router.push('/404')
            }
        )

        // // Implementasi ini untuk mengambil event dari master data
        // ScheduleService.getAllEventsMaster().then(
        //     (response) => {
        //     },
        //     (error) => {
        //         if (error.response == undefined) {
        //             router.push('/503')
        //         }
        //         router.push('/404')
        //     }
        // )
    }, [router.isReady]);

    const handleAddEvent = () => {
        let id;
        if (rows.length > 0) {
            id = rows.length
        } else {
            id = 0
        }
        setRows(
            [...rows, createData(id, null, null, null, null, null, null, null, id)]
        )
    }

    // field onChange
    const handleEditRowsModelChange = useCallback((model) => {
        setEditRowsModel(model)
        if (model !== {}) {
            // update state
            Object.keys(model).forEach(function (key, index) {
                let copyRows = [...rows]
                const itemIndex = copyRows.findIndex(o => o.id == key);

                let newEvent = {...model[key]}
                Object.keys(newEvent).forEach(function (newEventKey, index2) {
                    newEvent[newEventKey] = newEvent[newEventKey].value
                })

                newEvent.id = key
                if (newEvent.startDatePlan != null || newEvent.mandaysPlan != null ) {
                    newEvent.endDatePlan = addDaysHoliday(newEvent.startDatePlan, newEvent.mandaysPlan)
                }
                if (newEvent.startDateActual != null || newEvent.mandaysActual != null ) {
                    newEvent.endDateActual = addDaysHoliday(newEvent.startDateActual, newEvent.mandaysActual)
                }

                if (itemIndex > -1) {
                    copyRows[itemIndex] = newEvent;
                } else {
                    copyRows = copyRows.push(newEvent);
                }
                setRows(copyRows);
            });
        }

    }, [rows]);

    const handleRemoveRow = (id) => {
        setTimeout(() => {
            setRows((prevRows) => prevRows.filter((row) => row.id !== id));
        });
    }

    const handleClearAllRows = () => {
        setTimeout(() => {
            setRows([]);
        });
    }

    const isHoliday = (date) => {
        for (const holiday of holidaysDateList) {
            // let holiday = new Date(dateElement)
            if (holiday.getFullYear() == date.getFullYear() &&
                holiday.getMonth() == date.getMonth() &&
                holiday.getDate() == date.getDate()
            ) {
                return true
            }
        }
        return false
    }

    function addDaysHoliday(date, workdays) {
        const saturday = 6
        const sunday = 0
        let result = new Date(date)
        let addedDays = 0

        while (result.getDay() == saturday || result.getDay() == sunday || isHoliday(result)) {
            result.setDate(result.getDate() + 1);
        }

        while (addedDays < workdays-1) {
            result.setDate(result.getDate() + 1);
            if (result.getDay() != saturday && result.getDay() != sunday && !isHoliday(result)) {
                addedDays++
            }
        }

        return result;
    }


    const renderActionButton = (params) => {
        return (
            <div className='flex'>
                <img className={'inline my-auto'} width={24} height={24} src={'/icon/delete-icon.svg'}
                     onClick={() => handleRemoveRow(params.row.id)}/>
            </div>
        )
    }

    const columns = [
        // {field: 'no', headerName: 'No.', width: 50, headerClassName: 'bg-slate-300', editable: false},
        {
            field: 'name', headerName: 'Event',
            width: 200, editable: true, sortable: false, disableClickEventBubbling: true
        },
        {
            field: 'startDatePlan', headerName: "From(Planned)", type: 'date',
            width: 180, editable: true, sortable: false, disableClickEventBubbling: true,
            valueFormatter: (params) => params.value == null ? null : new Date(params.value).toLocaleDateString(),
        },
        {
            field: 'mandaysPlan', headerName: "Mandays(Planned)",
            width: 100, type: 'number', min: 1, editable: true, sortable: false, disableClickEventBubbling: true,
            valueParser: (value) => parseInt(value) > 0 ? parseInt(value) : 1,
        },
        {
            field: 'endDatePlan', headerName: 'To(Planned)', type: 'date',
            width: 180, sortable: false, disableClickEventBubbling: true,
            // editable: true,
            valueFormatter: (params) => params.value == null ? null : new Date(params.value).toLocaleDateString(),
        },
        {
            field: 'startDateActual', headerName: 'From(Actual)', type: 'date',
            width: 180, editable: true, sortable: false, disableClickEventBubbling: true,
            valueFormatter: (params) => params.value == null ? null : new Date(params.value).toLocaleDateString(),
        },
        {
            field: 'mandaysActual', headerName: 'Mandays(Actual)',
            width: 100, type: 'number', min: 1, editable: true, sortable: false, disableClickEventBubbling: true,
            valueParser: (value) => parseInt(value) > 0 ? parseInt(value) : 1,
        },
        {
            field: 'endDateActual', headerName: 'To(Actual)', type: 'date',
            width: 180, sortable: false, disableClickEventBubbling: true,
            // editable: true,
            valueFormatter: (params) => params.value == null ? null : new Date(params.value).toLocaleDateString(),
        },
        {
            field: 'actions',
            type: 'actions',
            headerName: 'Actions',
            width: 100,
            cellClassName: 'actions',
            renderCell: renderActionButton,
            disableClickEventBubbling: true,
        }
    ]

    const handleUpdateEventSubmit = () => {
        // need to have this fixDate func because of datagrid-mui bugs
        const fixDate = (date) => {
            if (date == null) return null
            let result = new Date(date);
            result.setHours(12)
            return result
        }

        let formData = rows.map((ev) => {
            return {
                name: ev.name,
                startDatePlan: fixDate(ev.startDatePlan),
                mandaysPlan: ev.mandaysPlan,
                endDatePlan: fixDate(ev.endDatePlan),
                startDateActual: fixDate(ev.startDateActual),
                mandaysActual: ev.mandaysActual,
                endDateActual: fixDate(ev.endDateActual)
            }
        });

        setLoading(true)
        ScheduleService.updateScheduleProjectById(pid, formData).then(
            (response) => {
                setMessage({
                    value: response.data,
                    severity: "success"
                })
                setOpenMessage(true)
                setLoading(false)
                setTimeout(() => router.push(`/projects/${pid}`), 2000);
            },
            (error) => {
                const resMessage =
                    (error.response &&
                        error.response.data &&
                        error.response.data.message) ||
                    error.message ||
                    error.toString()
                setMessage({
                    value: resMessage,
                    severity: "error"
                })
                setOpenMessage(true)
                setLoading(false)
            }
        )
    }

    if (currentUser == null) return <></>

    return (
        <>
            <Head>
                <title>Schedule</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            <AppLayout pageTitle={'Update Schedule'}>
                <div className='grid grid-cols-3 gap-2 ' style={{'float': 'right'}}>
                    <div>
                        <Button color={'secondary'} size={'s'} rounded="true"
                                onClick={handleClickBack}>Back</Button>
                    </div>
                    <div>
                        <Button isDelete color={'primary'} size={'s'} rounded="true"
                                onClick={handleClearAllRows}>Clear All</Button>
                    </div>
                    <div>
                        <Button type="submit" color={'primary'} size={'s'} rounded="true"
                                onClick={handleUpdateEventSubmit}>Update Schedule</Button>
                    </div>
                </div>
                <div style={{height: 400, width: '100%'}}>
                    <div className="grid grid-cols-8 gap-2">
                        <div className='' style={{float: 'left'}}>
                            <Button startIcon={<AddIcon/>} onClick={handleAddEvent}
                                    color={'primary'} size="s"
                                    className='navy'
                                    style={{width: "180px"}}
                                    rounded="true">+ Add Event
                            </Button>
                            <br/>
                            <br/>
                        </div>
                    </div>
                    <DataGrid
                        rows={rows}
                        columns={columns}
                        editMode="row"
                        editRowsModel={editRowsModel}
                        onEditRowsModelChange={handleEditRowsModelChange}
                        disableColumnMenu={true}
                        className="bg-white rounded-lg"
                    />
                    <div className={"mb-2 px-5"}>
                        <div style={{"font-size": "14px"}}>
                            *Notes:
                            <p>- Click the "+ Add Event" button and then double click cell to edit.</p>
                            <p>
                                - The "To" field is obtained from the sum of "From" with "Mandays" and
                                the number of Holidays in that range.
                            </p>
                        </div>
                        <a target="_blank" rel="noreferrer" href={'/projects/holiday-management'}>
                            <b>Click here to see holidays</b>
                        </a>
                    </div>

                    <Dialog
                        open={openBackDialog}
                        onClose={handleCloseDialog}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">
                            {"Are you sure?"}
                        </DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                By pressing the 'Yes' button, all changes will not be saved.
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={handleCloseDialog}>No</Button>
                            <Button isDelete onClick={handleYesDialog} autoFocus>
                                Yes
                            </Button>
                        </DialogActions>
                    </Dialog>
                    {(message.value != "") && (
                        <Snackbar
                            anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                            open={openMessage}
                            onClose={handleCloseMessage}
                            autoHideDuration={2000}>
                            <Alert onClose={handleCloseMessage} variant="filled" severity={message.severity}>
                                {message.value}
                            </Alert>
                        </Snackbar>
                    )}
                </div>
                <br/>
            </AppLayout>
        </>
    )
}


export default UpdateSchedule

