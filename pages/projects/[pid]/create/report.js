import Head from 'next/head'

import AppLayout from '../../../../components/Layout/App'
import AuthService from "../../../../services/auth.service";
import React, { useState, useEffect, useRef } from "react";
import Input from '../../../../components/Input'
import Button from '../../../../components/Button'
import Subtitle4 from '../../../../components/Typography/Subtitle4'
import {Alert, Snackbar} from "@mui/material";
import { useRouter } from "next/router"
import ReportIssueService from '../../../../services/reportIssue.service'
import TextArea from '../../../../components/TextArea';

export default function CreateReport(props) {
  const form = useRef();
  const router = useRouter()
  const {pid} = router.query
  
  const [title, setTitle] = useState("");
  const [titleValid, setTitleValid] = useState(true);
  const [titleCheck, setTitleCheck] = useState("");
  const [description, setDescription] = useState("");
  const [selectedFile, setSelectedFile] = useState(null)
  const [fileCheck, setFileCheck] = useState("");
  const [fileValid, setFileValid] = useState(true);
  const [loading, setLoading] = useState(false);
  const [openMessage, setOpenMessage] = useState(false);
  const [message, setMessage] = useState({
      value: "",
      severity: ""
  });
  const handleCloseMessage = (event, reason) => {
      if (reason === 'clickaway') {
          return;
      }
      setOpenMessage(false);
  };

  useEffect(() => {
    const user = AuthService.getCurrentUser();
    if (user === null){
      router.push("/login")
    } else if (user.role != 4) {
      router.push("/401")
    } 
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    setMessage("");
    setLoading(true);
    if(titleValid && fileValid) {
      return ReportIssueService.createReportIssue(pid, title, description).then((response) => {
        // Upload file
        if (selectedFile !== null) {
            let formData = new FormData();
            formData.append("file", selectedFile);
            ReportIssueService.uploadDocument(response.data?.result?.id, formData).then((res) => {
              console.log(res.data)
            });
        }
        setMessage({
          value: response.data?.message,
          severity: "success"
        })
        setOpenMessage(true);
        setTitle("");
        setDescription("");
        setSelectedFile(null);
      },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
          setMessage({
            value: resMessage,
            severity: "error"
          })
          setOpenMessage(true)
          setLoading(false);
        }
      );
    } 

  };

  const onChangeTitle = (e) => {
    const inputTitle = e.target.value;
    setTitle(inputTitle);
    if (inputTitle != "" && inputTitle.length > 50) {
      setTitleCheck("Title should consist <= 50 character")
      setTitleValid(false)
    } else {
      setTitleValid(true)
      setTitleCheck("")
    }
    
  };

  const onChangeDescription = (e) => {
    setDescription(e.target.value)
  };

  const onChangeDocument = (e) => {
    const file = e.target.files[0]
    setSelectedFile(file)
    if (file.size > 2097152) {
        setFileCheck("File size should not > 2Mb")
        setFileValid(false)
    } else {
        setFileValid(true)
        setFileCheck("")
      }

    
  };
  

  return (
    <>
      <Head>
        <title>Accounts</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <AppLayout pageTitle="Report Issue">
        <div className="w-2/3 p-1 m-auto bg-white rounded-lg">
          <div className='w-3/4 m-auto color-white'>
            <form onSubmit={handleSubmit} ref={form}>
              <div style={{ margin: '24px 0px' }} />
              <div className="w-full">
                <Subtitle4>
                  Title
                  <span className="text-red-500">*</span>
                </Subtitle4>
                <Input size = "s"
                  className="w-full"
                  placeholder="Input title"
                  type="text"
                  name="title"
                  value={title}
                  onChange={onChangeTitle}
                  required
                />
                {titleCheck && (
                  <div style={{ margin: '8px 0px', color: 'red', fontSize: 12}}>
                    {titleCheck}
                  </div>
                )}
              </div>
              <div style={{ margin: '16px 0px' }} />
                <div className="w-full">
                  <Subtitle4>
                    Description
                    <span className="text-red-500">*</span>
                  </Subtitle4>
                  <TextArea size = "s" 
                    className="w-full"
                    placeholder="Input description"
                    type="text"
                    name="description"
                    value={description}
                    onChange={onChangeDescription}
                    required/>
                </div>
                <div style={{ margin: '16px 0px' }} />
                <div className="w-full">
                  <Subtitle4>
                    Document
                  </Subtitle4>
                  {/* <div style={{ width: '460px'}}>
                    <input 
                      type="file"
                      accept="image/*,.pdf"
                      onChange={onChangeDocument}
                  />
                  </div> */}
                  <div class="flex w-full items-center justify-center bg-grey-lighter">
                    <label class="w-full flex flex-col items-center px-4 py-6 text-grey rounded-lg tracking-wide border border-grey border-dashed cursor-pointer hover:bg-blue hover:text-white"
                    >
                        <svg class="w-8 h-8" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                            <path d="M16.88 9.1A4 4 0 0 1 16 17H5a5 5 0 0 1-1-9.9V7a3 3 0 0 1 4.52-2.59A4.98 4.98 0 0 1 17 8c0 .38-.04.74-.12 1.1zM11 11h3l-4-4-4 4h3v3h2v-3z" />
                        </svg>
                        
                        {selectedFile?.name?
                                    <span class="mt-2 text-base leading-normal">{selectedFile?.name}</span>
                                     :
                                     <span class="mt-2 text-base leading-normal">Choose File</span>
                                }
                        <input type='file' class="hidden" accept="image/*,.pdf" onChange={onChangeDocument}/>
                    </label>
                </div>
                  <div style={{ margin: '8px 0px', color: 'red', fontSize: 12}}>
                    {fileCheck}
                  </div>
                {/* <FileManager
                    files={this.state.files}
                >
                    {this.uploadFiles}
                </FileManager> */}
                </div>
            
              <div style={{ margin: '48px 0px' }} />
                <div className='w-40 m-right float-right' >
                  <Button type="submit" size={'s'} className='navy' rounded="true">Submit</Button>
                </div>
              <div style={{ margin: '48px 0px' }} />
            </form>
            </div>
            <div style={{ margin: '120px 0px' }} />
            {(message.value != "") && (
                <Snackbar
                    anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                    open={openMessage}
                    onClose={handleCloseMessage}
                    autoHideDuration={2000}>
                    <Alert onClose={handleCloseMessage} variant="filled" severity={message.severity}>
                        {message.value}
                    </Alert>
                </Snackbar>
            )}
        </div>
      </AppLayout>
      
    </>
  )
}
