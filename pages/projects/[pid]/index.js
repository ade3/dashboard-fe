import React, {useEffect, useState} from "react";
import Link from 'next/link'
import Head from 'next/head'
import {useRouter} from 'next/router'
import {Alert, Snackbar} from "@mui/material";
import Select from 'react-select';
import {DataGrid} from "@mui/x-data-grid";
import PopUp from '../../../components/PopUp';
import AppLayout from '../../../components/Layout/App'
import Button from '../../../components/Button'
import ProjectService from "../../../services/project.service";
import AuthService from "../../../services/auth.service";
import BrandPromiseService from '../../../services/brandPromise.service';
import Input from "../../../components/Input";
import DeliverablesService from '../../../services/deliverables.service';


function createData(no, id, name, startDatePlan, mandaysPlan, endDatePlan, startDateActual, mandaysActual, endDateActual) {
    return {
        no, id, name, startDatePlan, mandaysPlan, endDatePlan, startDateActual, mandaysActual, endDateActual
    };
}


function DetailProject() {
    const router = useRouter()
    const [listDeliverables, setListDeliverables] = useState([])
    const {pid} = router.query
    const [currentUser, setCurrentUser] = useState(null);
    const [buttonPopUpDeliverables, setButtonPopUpDeliverables] = useState(false);
    const [project, setProject] = useState("")
    const [rows, setRows] = useState([]);
    const columns = [
        {field: 'no', headerName: 'No.', width: 50, headerClassName: 'bg-slate-300', editable: false},
        {

            field: 'name', headerAlign: 'center', headerName: 'Event',
            width: 180, sortable: false, disableClickEventBubbling: true
        },
        {
            field: 'startDatePlan', headerAlign: 'center', headerName: "From(Planned)", type: 'date',
            width: 120, sortable: false, disableClickEventBubbling: true,

            valueFormatter: (params) => params.value == null ? null : new Date(params.value).toLocaleDateString(),
        },
        {
            field: 'mandaysPlan', headerAlign: 'center', headerName: "Mandays(Planned)",
            width: 100, type: 'number', min: 1, sortable: false, disableClickEventBubbling: true,
            valueParser: (value) => parseInt(value) > 0 ? parseInt(value) : 1,
        },
        {
            field: 'endDatePlan', headerAlign: 'center', headerName: 'To(Planned)', type: 'date',
            width: 120, sortable: false, disableClickEventBubbling: true,

            valueFormatter: (params) => params.value == null ? null : new Date(params.value).toLocaleDateString(),
            // valueGetter: getEndDatePlan,
        },
        {
            field: 'startDateActual', headerAlign: 'center', headerName: 'From(Actual)', type: 'date',
            width: 120, sortable: false, disableClickEventBubbling: true,

            valueFormatter: (params) => params.value == null ? null : new Date(params.value).toLocaleDateString(),
        },
        {
            field: 'mandaysActual', headerAlign: 'center', headerName: 'Mandays(Actual)',
            width: 100, type: 'number', min: 1, sortable: false, disableClickEventBubbling: true,
            valueParser: (value) => parseInt(value) > 0 ? parseInt(value) : 1,
        },
        {
            field: 'endDateActual', headerAlign: 'center', headerName: 'To(Actual)', type: 'date',
            width: 120, sortable: false, disableClickEventBubbling: true,
            valueFormatter: (params) => params.value == null ? null : new Date(params.value).toLocaleDateString(),
            // valueGetter: getEndDateActual,
        },
    ]
    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState({
        value: "",
        severity: ""
    });
    const [openMessage, setOpenMessage] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");
    const [deliverables, setDeliverables] = useState([]);
    const [brandPromises, setBrandPromises] =useState({});
    const [standardPackages, setStandardPackages] =useState({});
    const [isEdit,setIsEdit]= useState(false);
    const [tempStandardPackages, setTempStandardPackages] = useState({})

    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenMessage(false);
    };

    const editBrandPromises = () => {
        setIsEdit(true)
        let temp = {}
        for(var key in standardPackages){
            console.log(key)
            let pkgTemp = []
            standardPackages[key].forEach(element => {
                pkgTemp.push({...element})
            })
            temp[key]=pkgTemp
           
        }
        setTempStandardPackages(temp)
    }
    const hanldeUpdateDeliverables = async (e) => {
        console.log(deliverables)
        setMessage("")
        setLoading(true)
        let deliverablesTemp = []
        let detailsTemp = []
        deliverables.map((del, index) => {
            deliverablesTemp.push(del.deliverables.id);
            detailsTemp.push(del.detail);
        })
        let formData = new FormData()
        let newDeliverables = {
            idProject:pid,
            idDeliverables:deliverablesTemp,
            details:detailsTemp

        }
        console.log(newDeliverables)
        formData.append("newDeliverables", newDeliverables)
        return ProjectService.updateDeliverables(newDeliverables).then((response) => {
            setMessage({
                value: response.data?.message,
                severity: "success"
            })
            setOpenMessage(true);
            setProject({...project, deliverablesProjects : response.data.result})
          },
            (error) => {
              const resMessage =
                (error.response &&
                  error.response.data &&
                  error.response.data.message) ||
                error.message ||
                error.toString();
                setMessage({
                    value: resMessage,
                    severity: "error"
                  })
                  setOpenMessage(true)
                  setLoading(false);
            }
        )
    };

    const hanldeUpdateNotes = async (e) => {
        setIsEdit(false)
        setLoading(true)
        setMessage("")
        let notesTemp = []
        let listBrandPromiseTemp = []
        let listStandardPackagesTemp = []
        for (var key in standardPackages){     
            standardPackages[key].forEach(element => {
                listBrandPromiseTemp.push(key)
                notesTemp.push(element.notes)
                listStandardPackagesTemp.push(element.id)
            })
        }
        let formData = new FormData()
        let newNotes = {
            idProject:pid,
            idBrandPromises:listBrandPromiseTemp,
            idStandardPackages:listStandardPackagesTemp,
            notes:notesTemp

        }
        formData.append("newNotes", newNotes)
        return ProjectService.updateNotes(newNotes).then((response) => {
            setMessage({
                value: response.data?.message,
                severity: "success"
            })
            setOpenMessage(true);
            setProject({...project, brandPromises : response.data.result})
          },
            (error) => {
              const resMessage =
                (error.response &&
                  error.response.data &&
                  error.response.data.message) ||
                error.message ||
                error.toString();
                setMessage({
                    value: resMessage,
                    severity: "error"
                  })
                  setOpenMessage(true)
                  setLoading(false);
            }
        )
    };

    const onChangeNotes = (e, value, index) => {
        let standardPackageTemp= [...standardPackages[value]]
        standardPackageTemp[index].notes=e.target.value
        setStandardPackages({...standardPackages,[value]:standardPackageTemp})
    }

    const handleCancelNotes = () => {
        console.log(tempStandardPackages)
        let temp = {}
        for(var key in tempStandardPackages){
            let pkgTemp = []
            tempStandardPackages[key].forEach(element => {
                pkgTemp.push({...element})
            })
            temp[key]=pkgTemp
           
        }
        setStandardPackages(temp)
        setIsEdit(false)
    }

    const onChangeDeliverables = (e) => {
        let deliverableTemp =[];
        deliverables.map((del) => {
            deliverableTemp.push(del);
        })
        // console.log(deliverables);
        e.map((choice) => {
            // console.log(choice);
            let isFound= false;
            deliverableTemp.map((value) => {
                // console.log(value)
                if (value.deliverables.id==choice.value) {
                    isFound=true;
                }
            })
            console.log(isFound)
            if(!isFound){
                deliverableTemp.push({ id:0, deliverables:{id:choice.value, name:choice.label}, detail:"Not Published Yet"})
            }
        })
        console.log(deliverableTemp)
        let deliverableTemp2=deliverableTemp.filter((del) => {
            let isFound=false;
            e.map((choice) => {
                // console.log(choice)
                if (choice.value==del.deliverables.id){
                    isFound=true
                }
            })
            console.log(isFound)
            return isFound
            })

        console.log(deliverableTemp2);
        setDeliverables(deliverableTemp2);
        }
    

    const onChangeDetails = (e, index) => {
        let temp = [...deliverables]
        let temp2 = {...deliverables[index]}
        temp2.detail=e.target.value;
        temp[index]=temp2;
        setDeliverables(temp);
        console.log(temp);
    }

    const [formValues, setFormValues] = useState({
        name: '',
        email: '',
        phone: '',
        password: ''
    });
    useEffect(() => {
        if (!router.isReady) return;
        if (!parseInt(pid)) router.push('/404')

        const user = AuthService.getCurrentUser()
        if (user == null) {
            router.push('/login')
        } else {
            setCurrentUser(user)
        }

        ProjectService.getProject(pid).then(
            (response) => {
                setProject(response.data);
                let listEvents = response?.data?.schedule?.events
                if (listEvents != null) {
                    setRows(listEvents.map((ev, index) => {
                        return createData(index + 1, index, ev.name, ev.startDatePlan, ev.mandaysPlan, ev.endDatePlan,
                            ev.startDateActual, ev.mandaysActual, ev.endDateActual)
                    }))
                }
            },
            (error) => {
                if (error.response == undefined) {
                    router.push('/503')
                }
                router.push('/404')
            }
        );

    }, [router.isReady]);

    useEffect(() => {
        DeliverablesService.getAllDeliverables().then(
            (response) => {
              setListDeliverables(response.data.result)
            },
            (error) => {
                console.log(error?.response)
            }
        );
        
        if(pid!=undefined){
            BrandPromiseService.getBpByProject(pid).then(
                (response) => {
                    let temp ={}
                    let temp2={}
                    response.data.result.forEach(
                        element => {
                            if (!(element.brandPromise.id in temp)){
                                temp[element.brandPromise.id]=element.brandPromise
                            }
                            if (!(element.brandPromise.id in temp2)){
                                temp2[element.brandPromise.id]=[{...element.standardPackage,notes:element.notes}]
                            }
                            else{
                                temp2[element.brandPromise.id].push({...element.standardPackage,notes:element.notes})
                            }

                        }
                    )
                setBrandPromises(temp)
                setStandardPackages(temp2)
                console.log(temp)
                console.log(temp2)
                }
            )
        }
        
    },[pid]);

    if (currentUser == null || project == "") {
        return <></>
    }

    return (
        <>
            <Head>
                <title>Project Detail</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            <AppLayout pageTitle={'Project Detail'}>
                <div className="w-full p-1 m-auto bg-white rounded-lg">
                    <br></br>
                    <div className='w-3/4 m-auto color-white'>
                        <div className="grid grid-cols-2 gap-2">
                            <div>
                                <h5 style={{fontWeight:"600"}}>Title</h5>
                                <p>{project.title}</p>
                                <br></br>

                                <h5 style={{fontWeight:"600"}}>Description</h5>
                                <p>{project.description}</p>
                                <br></br>

                                <h5 style={{fontWeight:"600"}}>Project Type</h5>
                                <p>{project.type?.type}</p>
                                <br></br>

                                <h5 style={{fontWeight:"600"}}>Project Date</h5>
                                <div className="grid grid-cols-2 gap-2">
                                    <div>
                                        <p>Start Date</p>
                                    </div>
                                    <div>
                                        <p>{project.startDate}</p>
                                    </div>
                                </div>
                                <div className="grid grid-cols-2 gap-2">
                                    <div>
                                        <p>End Date</p>
                                    </div>
                                    <div>
                                        <p>{project.endDate}</p>
                                    </div>
                                </div>
                                <div className="grid grid-cols-2 gap-2">
                                    <div>
                                        <p>Contract Length (days)</p>
                                    </div>
                                    <div>
                                        <p>{project.contractDuration}</p>
                                    </div>
                                </div>
                                <br></br>

                                <h5 style={{fontWeight:"600"}}>Project Status</h5>
                                <p>{project.status?.status}</p>
                                <br></br>

                                <h5 style={{fontWeight:"600"}}>URL Mockup</h5>
                                {project.urlMockUp == "Not Staged Yet"?
                                    <p>Not Published Yet</p> :
                                    <a href={project.urlMockUp} target="_blank" rel="noopener noreferrer" className="link">Click here</a>
                                }
                                <br></br>
                            </div>

                            <div>
                                <div>
                                    {project.image != null ?
                                        <img className="h-64 hover:scale-125 transition-all" width="300" height="150"
                                             src={`data:image/png;base64,${project.image}`}></img> :
                                        null
                                    }
                                </div>


                                <h5 style={{fontWeight:"600"}}>Category</h5>
                                <p>{project.category?.category}</p>
                                <br></br>

                                <h5 style={{fontWeight:"600"}}>Project Manager</h5>
                                <p>{project.projectManager?.name}</p>
                                <br></br>

                                <h5 style={{fontWeight:"600"}}>Project Sponsor</h5>
                                <ul>
                                    {project.sponsors?.map((sponsor) => (
                                        <li key={sponsor.id}>{sponsor.name}</li>
                                    ))}
                                </ul>
                                <br></br>

                                {currentUser?.role==1 || currentUser?.role==3?
                                    <Link href={`/projects/${project.id}/update`}>
                                        <Button type="button" className="navy" style={{width:"220px"}} size={'s'} rounded="true">Edit</Button>
                                    </Link> : null
                                }
                            </div>
                        </div>

                        <br/>
                        <hr/>
                        <br/>

                        <div style={{display:"flex"}}>
                            <h5 style={{fontWeight:"600", marginRight:'25px'}}>Project Deliverables</h5>
                            
                            {currentUser?.role==1 || currentUser?.role==3?
                                <Button className="buttonEdit" onClick={() =>{ setButtonPopUpDeliverables(true); setDeliverables(project.deliverablesProjects); console.log(buttonPopUpDeliverables)}}>
                                <img  className={'inline my-auto'} width={24} height={24} src={'/icon/edit-icon.svg'} />
                                </Button>
                                :
                                null
                            }

                        </div>
                        <br />
                        <div className="grid grid-cols-2 gap-2">
                            {project.deliverablesProjects?.length > 0?
                                <div>
                                    <table className="table-deliverables">
                                        <tr>
                                            <th>No. </th>
                                            <th>Deliverables</th>  
                                            <th>Detail</th>   
                                            </tr>  
                                            {project.deliverablesProjects?.map((del,index) => (
                                        <tr data-index={index}>
                                            <td>{index+1}</td>
                                            <td>{del.deliverables.name}</td>
                                            {del.detail == "Not Published Yet"?
                                            <td>Not Published Yet</td>
                                            : <td><a href={del.detail} target="_blank" rel="noopener noreferrer" className="link">{del.detail}</a></td>}
                                        </tr>
                                        ))}
                                    </table>   
                                </div>
                            : <p>Not yet Set</p>}
                        </div>

                        <br/>
                        <hr/>
                        <br/>

                        <div style={{display:"flex"}}>
                            <h5 style={{fontWeight:"600",  marginRight:'25px'}}>Brand Promise</h5>
                            {((currentUser?.role==1 || currentUser?.role==3) && Object.keys(brandPromises).length!=0)?
                            
                            <Button className="buttonEdit" onClick={()=> {editBrandPromises()}}>
                            <img  className={'inline my-auto'} width={24} height={24} src={'/icon/edit-icon.svg'}/>
                            </Button>
                                :
                                null
                            }
                        </div>

                        <br/>

                        {Object.keys(standardPackages).length > 0?
                            <table>
                                <tr>
                                    <th className='thbp'style={{width:"50px"}}>No</th>
                                    <th className='thbp'>Name</th>
                                    <th className='thbp' style={{width:"200px"}}>Standard Package</th>
                                    <th className='thbp'>Notes</th>
                                </tr>
                                {Object.keys(standardPackages)?.map((value, index) => (
                                <tr data-index={index}>
                                    <td className='tdbp' style={{width:"50px"}}>{index+1}</td>
                                    <td className='tdbp'>{brandPromises[value].name}</td>
                                    <td className='tdbp' >
                                        <ul style={{listStyleType:"disc"}}>
                                            {standardPackages[value].map((pkg) => (
                                                <li>{pkg.name}</li>
                                            ))}
                                        </ul>
                                    </td>
                                    {isEdit ? 
                                    <td className='tdbp'>
                                    {standardPackages[value].map((pkg, index)=> (
                                         <Input size ="s" style={{ width: '200px'}}
                                         type="text"
                                         className="form-control"
                                         name="notes"
                                         placeholder= "Set Notes"
                                         value={pkg.notes}
                                         onChange={ (e) => onChangeNotes(e,value, index)}
                                         required/>
                                    ))}
                                   
                                    </td>: 
                                    <td className='tdbp'>
                                        <ul>
                                        {standardPackages[value].map((pkg)=> (
                                        <li>{pkg.notes}</li>
                                    ))}
                                        </ul>
                                        
                                    </td>}
                                    
                                </tr>  
                                ))}
                            </table> :
                            <p>Not Yet Set</p>
                        }
                        <br></br>
                        {isEdit==true? 
                        <div style={{display:"flex"}}>
                            <Button type="button" className="navy" style={{width:"120px", marginRight:"30px"}} size={'s'} rounded="true" onClick={() => { hanldeUpdateNotes()}}>Save</Button>
                            <Button type="button" color={'primary'} style={{width:"120px"}} size={'s'} rounded="true" onClick={() => {handleCancelNotes()}}>Cancel</Button>
                        </div>
                       
                        : null}
                        

                        <br/>
                        <hr/>
                        <br/>

                        <div style={{display:"flex"}}>
                            <h5 style={{fontWeight:"600", marginRight:'25px'}}>Project Schedule</h5>
                            {currentUser?.role==1 || currentUser?.role==3?
                                <Link href={`/projects/${pid}/schedule/update`}>
                                    <Button className="buttonEdit">
                                <img  className={'inline my-auto'} width={24} height={24} src={'/icon/edit-icon.svg'}/>
                                </Button>
                                </Link>
                                :
                                null
                            }
                        </div>
                        <br/>
                        
                        {project?.schedule != null && project?.schedule?.events?.length != 0 ?
                            <>
                                <div className={'flex flex-col'}>
                                    <div className='m-auto' style={{float: 'left'}}>
                                        <a target="_blank" href={`/projects/${project.id}/schedule`}
                                           rel="noopener noreferrer">
                                            <Button type="button" className="navy" style={{width:"220px"}} size={'s'} rounded="true">Open Timeline Charts</Button>
                                        </a>
                                    </div>
                                    <div>
                                        <div style={{height: 500, width: '100%'}}>
                                            <DataGrid
                                                sx={{m: 2, color: '#052A49', fontWeight: 'bold'}}
                                                rows={rows}
                                                columns={columns}
                                                disableSelectionOnClick
                                                disableMultipleSelection={true}
                                                disableColumnMenu={true}
                                                className=" rounded-lg"
                                            />
                                        </div>
                                    
                                    </div>
                                </div>
                            </>
                            :
                            <p>Not Yet Set</p>
                        }
                        <PopUp trigger={buttonPopUpDeliverables} setTrigger={setButtonPopUpDeliverables}>
                                        <div className="popup-inner-update-deliverables">
                                    <h5>Update Project Deliverables</h5>
                                    <br></br>
                                    <Select
                                        isMulti
                                        name="deliverables"
                                        options={listDeliverables.map((i) => { return {"value":i.id, "label":i.name } } )}
                                        className="basic-multi-select"
                                        classNamePrefix="select"
                                        onChange={onChangeDeliverables}
                                        // value={deliverables.map((i) => { return {"value":i.deliverables.id, "label":i.deliverables.name } } )}
                                        defaultValue={project.deliverablesProjects?.map((i) => { return {"value":i.deliverables.id, "label":i.deliverables.name } } )}
                    
                                    />
                                    <br></br>
                                    <div>
                                    {deliverables?.length != 0 ?
                                    <table className="table-deliverables">
                                    <tr>
                                        <th>Deliverables</th>  
                                        <th>Detail</th>   
                                        </tr>  
                                        {deliverables?.map((del,index) => (
                                    <tr data-index={index}>
                                        <td>{del.deliverables.name}</td>
                                        <td> <Input size ="s" style={{ width: '420px'}}
                                                type="url"
                                                className="form-control"
                                                name="details"
                                                placeholder= "Input Deliverables Link"
                                                value={del.detail}
                                                onChange={ (e) => onChangeDetails(e,index)}
                                                // defaultValue={del.detail}
                                                required
                                            /></td>
                                    </tr>
                                    ))}
                                </table>   :null}
                                    
                                    </div>
                                    <br></br>
                                    <div style={{display:'flex'}}>
                                        <Button type="button" className="navy spacingButtonRight" style={{width:"150px"}} size={'s'} rounded="true" onClick={() => {setButtonPopUpDeliverables(false);hanldeUpdateDeliverables();}}>Confirm</Button>
                                            
                                        <Button type="button" color={'primary'} style={{width:"150px"}} size={'s'} rounded="true" onClick={() =>{setButtonPopUpDeliverables(false); setDeliverables(project.deliverablesProjects)}}>Cancel</Button>
                                    <br></br>
                                    </div>
                                    </div> 
                        </PopUp>
                        <div style={{ margin: '80px 0px' }} />
                        <br/>
                        <div className='grid gap-2 w-40 m-left' style={{ float: 'left' }}>
                            <div>
                                <Link href="/projects">
                                    <Button color={'secondary'} size={'s'} rounded="true">Back</Button>
                                </Link>
                            </div>

                        </div>
                        {currentUser?.role == 4?
                            <div className='w-50 m-left' style={{ float: 'right' , display: 'flex'}}>
                                <div className='grid gap-2 w-40 m-left' style={{ marginRight: 20}}>
                                    <Link href={`/projects/${pid}/create/report`}>
                                        <Button isDelete={true} color={'primary'} size={'s'} rounded="true" className="red">Report</Button>
                                    </Link>  
                                </div>
                                {project.status?.status == "Initiating" || project.status?.status == "Development" ? 
                                    <div>
                                        <Link href={`/projects/${pid}/create/change-request`}>
                                            <Button color={'primary'} size={'s'} rounded="true" className="navy">Change Request</Button>
                                        </Link>  
                                    </div>
                                : null}
                                {project.status?.status == "Maintainance" ? 
                                    <div>
                                        <Link href={`/projects/${pid}/create/ticket-support`}>
                                            <Button color={'primary'} size={'s'} rounded="true" className="navy">Ticketing Support</Button>
                                        </Link>  
                                    </div>
                                : null}
                            </div> 
                            
                            : null
                        }   

                    </div>
                    <div style={{margin: '120px 0px'}}/>
                    {(message.value != "") && (
                <Snackbar
                    anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                    open={openMessage}
                    onClose={handleCloseMessage}
                    autoHideDuration={3000}>
                    <Alert onClose={handleCloseMessage} variant="filled" severity={message.severity}>
                        {message.value}
                    </Alert>
                </Snackbar>
            )}
                </div>
            
            </AppLayout>
        </>
    )

}

export default DetailProject
