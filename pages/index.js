import Head from 'next/head'
import dynamic from "next/dynamic";
import { useRouter } from "next/router"
import React, { useEffect, useState } from "react";
const ApexCharts = dynamic(() => import("apexcharts"), { ssr: false });
const ReactApexChart = dynamic(() => import("react-apexcharts"), { ssr: false });

import ProjectService from "../services/project.service";
import AuthService from "../services/auth.service";
import AppLayout from '../components/Layout/App'
import styles from '../styles/components/dashboard.module.css'

export default function Home(props) {
    const router = useRouter()
    const [currentUser, setCurrentUser] = useState(undefined);

    const [pies, setPies] = useState(null);

    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);

        const user = AuthService.getCurrentUser();
        if (user === null) {
            router.push("/login")
        } else if (user.role > 2) {
            router.push("/projects")
        } else {
            setCurrentUser(user);

            ProjectService.getSummaryAllProjects().then(
                (response) => {
                    let pieObjects = {}

                    let options = {
                        chart: {
                            width: 700,
                            type: 'pie',
                        },
                    }
                    let statSeries = []
                    let statLabels = []
                    response?.data?.status?.forEach((st) => {
                        statLabels.push(st.status)
                        statSeries.push(st.count)
                    })
                    options.labels = statLabels
                    pieObjects.statusPie = { series: statSeries, options: options }

                    let options2 = {
                        chart: {
                            width: 700,
                            type: 'pie',
                        },
                        plotOptions: {
                            pie: {
                                customScale: 1.05
                            }
                        }
                    }
                    let lateProjectsSeries = []
                    let lateProjectsLabels = []
                    let lateProjects = response?.data?.lateProjects
                    Object.keys(lateProjects).forEach(function (key) {
                        lateProjectsSeries.push(lateProjects[key])
                        lateProjectsLabels.push(key)
                    })
                    options2.labels = lateProjectsLabels
                    pieObjects.lateProjectPie = { series: lateProjectsSeries, options: options2 }

                    let options3 = {
                        chart: {
                            width: 700,
                            type: 'pie',
                        },
                        plotOptions: {
                            pie: {
                                customScale: 1.05
                            }
                        }
                    }
                    let typeSeries = []
                    let typeLabels = []
                    response?.data?.type?.forEach((st) => {
                        typeLabels.push(st.type)
                        typeSeries.push(st.count)
                    })

                    options3.labels = typeLabels
                    pieObjects.typePie = { series: typeSeries, options: options3 }

                    let options4 = {
                        chart: {
                            width: 700,
                            type: 'pie',
                        },
                    }
                    let categorySerries = []
                    let categoryLabels = []
                    response?.data?.category?.forEach((st) => {
                        categoryLabels.push(st.category)
                        categorySerries.push(st.count)
                    })

                    options4.labels = categoryLabels
                    pieObjects.categoryPie = { series: categorySerries, options: options4 }


                    let options5 = {
                        chart: {
                            width: 700,
                            type: 'pie',
                        },
                    }
                    let qualitySeries = []
                    let qualityLabels = []
                    let qualityProjects = response?.data?.qualityProjects
                    Object.keys(qualityProjects).forEach(function (key) {
                        qualitySeries.push(qualityProjects[key])
                        qualityLabels.push(key)
                    })

                    options5.labels = qualityLabels
                    pieObjects.qualityPie = { series: qualitySeries, options: options5 }

                    setPies(pieObjects);
                },
                (error) => {
                    console.log(error)
                }
            );
            setLoading(false)

        }
    }, []);

    if (currentUser == null || pies == null) return <></>


    return (
        <>
            <Head>
                <title>Home</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <AppLayout pageTitle="Statistics All Projects">
                {/*<div style={{marginBottom: 24}}>*/}
                {/*    <div className={styles.container}>*/}
                {/*        <div className={'p-2'}>*/}
                {/*            <div className={'w-full'}>*/}
                {/*                /!*<Subtitle3>Date filter</Subtitle3>*!/*/}
                {/*                /!*<DateRange/>*!/*/}
                {/*            </div>*/}
                {/*        </div>*/}
                {/*    </div>*/}
                {/*</div>*/}
                {/*<div style={{marginBottom: 24}} className={'flex'}>*/}
                {/*    <Summary />*/}
                {/*</div>*/}

                <div style={{ marginBottom: 24 }} className={'grid grid-cols-2 p-4 gap-4'}>
                    <div className={'p-2'}>
                        <div className={'bg-white shadow rounded-lg p-4 '}>
                            <h6>Status</h6>
                            {pies.statusPie.series.reduce((partialSum, a) => partialSum + a, 0) == 0 ?
                                <div width={350}> {/* className={'bg-white w-full rounded '} */}
                                    <p>No projects </p>
                                </div> :
                                <div width={350}> {/* className={'bg-white w-full rounded '} */}
                                    <div className={styles.summaryItem}>
                                        <ReactApexChart options={pies.statusPie.options} series={pies.statusPie.series}
                                            type="pie" width="100%" />
                                    </div>
                                </div>
                            }
                        </div>
                    </div>

                    <div className={'p-2'}>
                        <div className={'bg-white shadow rounded-lg p-4 '}>
                            <h6>Type</h6>
                            {pies.typePie.series.reduce((partialSum, a) => partialSum + a, 0) == 0 ?
                                <div width={350}> {/* className={'bg-white w-full rounded '} */}
                                    <p>No projects </p>
                                </div> :
                                <div width={350}> {/* className={'bg-white w-full rounded '} */}
                                    <div className={styles.summaryItem}>
                                        <ReactApexChart options={pies.typePie.options} series={pies.typePie.series} type="pie" />
                                    </div>
                                </div>
                            }
                        </div>
                    </div>

                    <div className={'p-2'}>

                        <div className={'bg-white shadow rounded-lg p-4 '}>
                            <h6>Output Quality</h6>
                            {pies.qualityPie.series.reduce((partialSum, a) => partialSum + a, 0) == 0 ?
                                <div width={350}> {/* className={'bg-white w-full rounded '} */}
                                    <p>No projects</p>
                                </div> :
                                <div width={350}> {/* className={'bg-white w-full rounded '} */}
                                    <div className={styles.summaryItem}>
                                        <ReactApexChart options={pies.qualityPie.options}
                                            series={pies.qualityPie.series} type="pie" />
                                    </div>
                                </div>
                            }
                        </div>
                    </div>

                    <div className={'p-2'}>
                        <div className={'bg-white shadow rounded-lg p-4 '}>
                            <h6>Timing</h6>
                            {pies.lateProjectPie.series.reduce((partialSum, a) => partialSum + a, 0) == 0 ?
                                <div width={350}> {/* className={'bg-white w-full rounded '} */}
                                    <p>No projects have been completed yet</p>
                                    <div className='h-auto'></div>
                                </div> :
                                <div width={350}> {/* className={'bg-white w-full rounded '} */}
                                    <div className={styles.summaryItem}>
                                        <ReactApexChart options={pies.lateProjectPie.options}
                                            series={pies.lateProjectPie.series} type="pie" />
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                    <div className={'p-2'}>
                        <div className={'bg-white shadow rounded-lg p-4 '}>
                            <h6>Category</h6>
                            {pies.categoryPie.series.reduce((partialSum, a) => partialSum + a, 0) == 0 ?
                                <div width={700}> {/* className={'bg-white w-full rounded '} */}
                                    <p>No projects </p>
                                </div> :
                                <div width={700}> {/* className={'bg-white w-full rounded '} */}
                                    <div className={styles.summaryItem}>
                                        <ReactApexChart options={pies.categoryPie.options} series={pies.categoryPie.series} type="pie" />
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </AppLayout>
        </>
    )
}