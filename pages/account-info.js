import Head from 'next/head'
import styles from '../styles/components/dashboard.module.css'
import UserService from '../services/user.service'

import AppLayout from '../components/Layout/App'
import Subtitle3 from '../components/Typography/Subtitle3'
import AuthService from '../services/auth.service';
import React, { useState, useEffect, useRef } from "react";
import Input from '../components/Input'
import Button from '../components/Button'
import Subtitle4 from '../components/Typography/Subtitle4'
import { IconButton } from "@mui/material";
import { isEmail } from "validator";
import axios from "axios";
import authHeader from "../services/auth-header";
import { useRouter } from "next/router"

import { BACKEND_URL } from "../config";
import { Subtitles } from '@mui/icons-material'

export default function DetailAccount() {
    const router = useRouter()
    const user = AuthService.getCurrentUser();
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [roleUser, setRole] = useState("");
    const [createdAt, setCreatedAt] = useState("");

    const form = useRef();
    const [responseStatus, setResponseStatus] = useState({ status: 500, data: "Internal Server Error" })
    const [dataAccount, setDataAccount] = useState();
    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState("");

    const [roleData, setRoleData] = useState([]);
    const [authorization, setAuthorization] = useState("");

    const handleBack = () => {
        return router.push("/");
    };

    useEffect(() => {
        console.log(user)
        if (user === null) {
            router.push("/login")
        } else {
            setName(user.name)
            setEmail(user.email)
            setRole(user.role.roleUser)
            setCreatedAt(user.created_at)
        }
    }, []);

    return (
        <>
            <Head>
                <title>Account Information Data</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <AppLayout pageTitle="Account Information Data">
                <div className="w-2/3 p-1 m-auto bg-white rounded-lg">
                    <div className='w-3/4 m-auto color-white'>

                        {message && (
                            <div className="form-group" style={{ margin: '20px 0px auto', color: 'green' }}>
                                {message}
                            </div>
                        )}
                        <div style={{ margin: '39px 0px' }} />
                        <div className="w-full">
                            <Subtitle3>
                                Name
                            </Subtitle3>
                            <div>
                            </div>
                            <Subtitle4>
                                {name}
                            </Subtitle4>
                        </div>
                        <div style={{ margin: '16px 0px' }} />
                        <div className="w-full">
                            <Subtitle3>
                                Email
                            </Subtitle3>
                            <Subtitle4>
                                {email}
                            </Subtitle4>
                        </div>
                        <div style={{ margin: '16px 0px' }} />
                        <div className="w-full">
                            <Subtitle3>
                                Role
                            </Subtitle3>
                            {user?.role == 1?
                                <Subtitle4>
                                    Super Admin
                                </Subtitle4>
                                :
                                null
                            }
                            {user?.role == 2?
                                <Subtitle4>
                                    Admin
                                </Subtitle4>
                                :
                                null
                            }
                            {user?.role == 3?
                                <Subtitle4>
                                    Project Manager
                                </Subtitle4>
                                :
                                null
                            }
                            {user?.role == 4?
                                <Subtitle4>
                                    Client
                                </Subtitle4>
                                :
                                null
                            }
                        </div>
                        <div style={{ margin: '48px 0px' }} />
                        <div className='w-40 m-left' style={{ float: 'left' }}>
                            <Button onClick={handleBack} size={'s'} color={'secondary'} rounded="true">Back</Button>
                        </div>
                    </div>
                    <div style={{ margin: '120px 0px' }} />
                </div>
            </AppLayout>

        </>
    )
}