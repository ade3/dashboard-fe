import Head from 'next/head'
// import _ from 'lodash'
import styles from '../../styles/components/dashboard.module.css'

import AppLayout from '../../components/Layout/App'
import Subtitle3 from '../../components/Typography/Subtitle3'
import DateRange from '../../components/DateRange'
import Summary from '../../components/Pages/Dashboard/Summary'
import AuthService from "../../services/auth.service";
import React, { useState, useEffect } from "react";

export default function Dashboard(props) {
  useEffect(() => {
    const user = AuthService.getCurrentUser();
    // if (user === null){
    //   router.push("/login")
    // }
  }, []);
  return (
    <>
      <Head>
        <title>Home</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <AppLayout pageTitle="Home">
        {/* <div style={{ marginBottom: 24 }}>
          <div className={styles.container}>
            <div className={'p-2'}>
              <div className={'w-full'}>
                <Subtitle3>Date filter</Subtitle3>
                <DateRange />
              </div>
            </div>
          </div>
        </div> */}
        {/* <div style={{ marginBottom: 24 }} className={'flex items-center justify-center'}>
          <Summary />
        </div> */}
      </AppLayout>
    </>
  )
}
