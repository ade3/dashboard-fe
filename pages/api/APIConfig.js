import axios from "axios";
const APIConfig = axios.create({
    baseURL: `${BACKEND_URL}/api/`,
});
export default APIConfig;