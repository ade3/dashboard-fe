import { useRouter } from "next/router"
export default function Custom401() {
    const router = useRouter()
    setTimeout(() => {
        router.push("/")
     }, 2000); //will call the function after 2 secs.
    return (
      <>
        <h4 className="m-auto" style={{ textAlign:'center', paddingTop:'20px'}}>401 - You're not Authorized</h4>
        <img width={"35%"} height={"35%"} className="m-auto"
            src={'/image/401 Error Unauthorized-amico.svg'}
        />
        <h4 className="loading" style={{ textAlign:'center'}}>Redirecting</h4>
      </>
    )

  }