import Head from 'next/head'
import Link from 'next/link'
import * as React from "react";
import {useEffect, useState} from "react";
import {useRouter} from "next/router"
import {DataGrid} from '@mui/x-data-grid';
import {Alert, Snackbar} from "@mui/material";

import AppLayout from '../../components/Layout/App'
import Button from '../../components/Button'
import AuthService from '../../services/auth.service'
import TicketSupportService from '../../services/ticketSupport.service'

function createData(id, title, createdAt, project, statusTicketIssue, severityLevel, fixDays ) {
    let timestamp = createdAt
    let issueDate = timestamp.substr(0,10)
    return {
        id,
        title,
        issueDate,
        project,
        severityLevel,
        statusTicketIssue,
        fixDays
    };
}

function getColumns(idRole) {
    let columns = [
        {field: 'id', headerName: 'ID', width: 100, headerClassName: 'bg-slate-300'},
        {field: 'title', headerName: 'Title', width: 300},
        {field: 'severityLevel', headerName: "Severity Level", width: 150},
        {field: 'issueDate', headerName: "Issue Date", width: 150},
        {field: 'project', headerName: "Project", width: 170},
        {field: 'statusTicketIssue', headerName: 'Status', width: 150},
        {field: 'fixDays', headerName: 'fixDays', width: 150}
    ]
    // exclude superadmin
    if (idRole != 1) {
        columns.push(
            {
                field: 'action', 
                headerName: 'Action', 
                width: 100, 
                sortable: false,
                renderCell: renderActionButton,
                disableClickEventBubbling: true
            }
        )
    }
    return columns
}

const renderActionButton = (params) => {
    return (
        <div className='flex'>
            <Link href={`/ticket-support/${encodeURIComponent(params.row.id)}`}>
                <Button size={'s'} color={'primary'} className={'navy'} rounded="true">Detail</Button>
            </Link>
        </div>
    )
}

function getRows(listTi) {
    let arr = []
    if (listTi != null) {
        for (const ti of listTi) {
            arr.push(createData(ti.id, ti.title, ti.createdAt, ti.project.title, ti.statusTicketIssue.status, ti.severityLevel.level, ti.fixDays))
        }
    }
    return arr
}

function ListTicketIssue() {
    const router = useRouter()
    const [currentUser, setCurrentUser] = useState(null);

    const [listTicketIssue, setListTicketIssue] = useState(null);

    const [loading, setLoading] = useState(false);
    const [openMessage, setOpenMessage] = useState(false);
    const [message, setMessage] = useState({
        value: "",
        severity: ""
    });
    const handleCloseMessage = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenMessage(false);
    };

    useEffect(() => {
        if(!router.isReady) return;

        setMessage("");
        setLoading(true);

        TicketSupportService.getAllTicketIssue().then(
            (response) => {
                if(Array.isArray(response.data)) {
                    setListTicketIssue(response.data)
                } else {
                    setMessage({
                        value: response?.data,
                        severity: "error"
                    })
                    setOpenMessage(true);
                }
            },
            (error) => {
                setLoading(false);
                setMessage({
                    value: error.response?.data,
                    severity: "error"
                })
                setOpenMessage(true);
            }
        );

        const user = AuthService.getCurrentUser()
        if (user == null) {
            router.push('/login')
        } else if (user.role == 2) {
            router.push("/401")
        } else {
            setCurrentUser(user)
        }
    }, [router.isReady]);

    if (currentUser == null) return <></>

    return (
        <>
            <Head>
                <title>Ticket Support</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            <AppLayout pageTitle={'List Ticket Support'}>
                <div style={{height: 500, width: '90%', margin: 'auto', backgroundColor: "white" }}>
                    
                    <DataGrid
                        style={{float: 'center', paddingLeft: "1em", fontSize: '14px', fontWeight: "bold"}}
                        rows={getRows(listTicketIssue)}
                        columns={getColumns(currentUser?.role)}
                        pageSize={10}
                        // rowsPerPageOptions={[10]}
                        disableSelectionOnClick
                        disableMultipleSelection={true}
                        // checkboxSelection
                        className="bg-white rounded-lg"
                    />
                        
                    
                </div>
                {(message.value != "") && (
                    <Snackbar
                        anchorOrigin={{vertical: 'top', horizontal: 'center'}}
                        open={openMessage}
                        onClose={handleCloseMessage}
                        autoHideDuration={2000}>
                        <Alert onClose={handleCloseMessage} variant="filled" severity={message.severity}>
                            {message.value}
                        </Alert>
                    </Snackbar>
                )}
            </AppLayout>
        </>
    )
}


export default ListTicketIssue
