import Head from 'next/head'
import PopUp from '../../../components/PopUp'
import AppLayout from '../../../components/Layout/App'
import Button from '../../../components/Button'
import Input from "../../../components/Input"
import Subtitle4 from '../../../components/Typography/Subtitle4'
import Subtitle3 from '../../../components/Typography/Subtitle3'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React, {useEffect, useState} from "react";
import AuthService from "../../../services/auth.service";
// import Error from "../../custom-error";
import { BACKEND_URL } from "../../../config/index"
import {Alert} from "@mui/material";
import TicketSupportService from '../../../services/ticketSupport.service'

function DetailTicketIssue() {
    const router = useRouter()
    const { pid } = router.query
    const [currentUser, setCurrentUser] = useState(null);

    const [ticketSupport, setTicketSupport] = useState("")
    const [issueDate, setIssueDate] = useState("");
    const [file, setFile] = useState("")

    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState("");
    const [errorMessage, setErrorMessage] = useState("");
    const [buttonPopUpStatus, setButtonPopUpStatus] = useState(false);
    const [fixingDays, setFixingDays] =  useState("");

    useEffect(() => {
        if(!router.isReady) return;

        const user = AuthService.getCurrentUser()
        if (user == null) {
            router.push('/login')
        } else {
            setCurrentUser(user)
        }
        TicketSupportService.getTicketIssue(pid).then(
            (response) => {
                setTicketSupport(response.data)
                setIssueDate(response.data.createdAt.substr(0, 10))
            },
            (error) => {
                console.log(error)
                setErrorMessage(error);
                setLoading(false)
            }
        );


    }, [router.isReady]);

    // if (errorMessage != "") {
    //     return <Error errorResponse={errorMessage?.response} redirectTo={`/change-requests`} />
    // }
    if (currentUser == null) {
        return <></>
    }

    const openPopUpStatus= async () => {
        setButtonPopUpStatus(true);
    }

    const onChangeEndDate = (e) => {
        console.log(e)
       setFixingDays(e.target.value);
        
    }

    const updateEndDate = () => {
        setLoading(true);
    return TicketSupportService.updateEndDate(ticketSupport.id, fixingDays).then(
            (response) => {
                setMessage("Success added fixing days")
                setLoading(false);
            },
            (error) => {
                console.log(error)
                // setErrorMessage(error.response.data);
                setLoading(false)
            }
        );
    }




    const updateStatus = (id) => {
        console.log(id)
        setLoading(true);
        return TicketSupportService.updateStatus(ticketSupport.id, id).then(
            (response) => {
                setTicketSupport(response.data)
                setIssueDate(response.data.createdAt.substr(0, 10))
            },
            (error) => {
                console.log(error)
                setErrorMessage(error);
                setLoading(false)
            }
        );
    }
    

    return (
        <>
            <Head>
                <title>Ticket Issue Detail</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <AppLayout pageTitle={'Ticket Support Detail'}>
                <div className="w-3/4 p-1 m-auto bg-white rounded-lg">
                    <div className='w-5/6 m-auto color-white'>
                        {errorMessage? 
                            <div style={{margin: 'auto', width: '50%'}}>
                                <div style={{ margin: '16px 0px' }} />
                                <br/>
                                <Alert severity="error">{errorMessage}</Alert>
                            </div>
                            : null
                        }
                        <div className="grid grid-cols-2 gap-2">
                            <div>
                                <div className="grid grid-cols-2 gap-2">
                                    <div style={{fontWeight: "bold", fontSize: 16}}>
                                        <br></br>
                                        <p>ID</p>
                                        <br></br>
                                        <p>Title</p>
                                        <br></br>
                                        <p>Severity Level</p>
                                        <br></br>
                                        { ticketSupport.statusTicketIssue?.status == 2 ? <>
                                        <p>Fixing Days</p>
                                        <br></br>
                                        </>
                                        :null}
                                        <p>Issue Date</p>
                                        <br></br>
                                        <p>Status</p>
                                        <br></br>
                                        <p>Project</p>
                                        <br></br>
                                        <p>Document</p>
                                    </div>
                                    <div style={{fontSize: 16}}>
                                        <br></br>
                                        <p>: {ticketSupport.id}</p>
                                        <br></br>
                                        <p>: {ticketSupport.title}</p>
                                        <br></br>
                                        <p>: {ticketSupport.severityLevel?.level}</p>
                                        <br></br>
                                        { ticketSupport.statusTicketIssue?.status == 2 ? <>
                                        <p>: {ticketSupport.fixDays}</p>
                                        <br></br>
                                        </>
                                        :null}
                                        <p>: {issueDate}</p>
                                        <br></br>
                                        <p>: {ticketSupport.statusTicketIssue?.status}</p>
                                        <br></br>
                                        <p>: {ticketSupport.project?.title}</p>
                                        <br></br>
                                        <div style={{width: "110px"}}>
                                            <Link rel="noopener noreferrer" href={`${BACKEND_URL}/api/file/download/file-ts/${ticketSupport.file?.id}`} target="_blank">
                                                <Button color={'primary'} size={'s'} rounded="true">Download</Button>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <br></br>
                                <p style={{fontWeight: "bold", fontSize: 16}}>Description</p>
                                <br></br>
                                <div>
                                    {ticketSupport.description}
                                </div>
                            </div>
                        </div>

                        <br />
                        <hr />
                        <br />

                        <br/>
                        <div className='grid gap-2 w-40 m-left' style={{ float: 'left' }}>
                            <div>
                                <Link href="/ticket-support">
                                    <Button color={'secondary'} size={'s'} rounded="true">Back</Button>
                                </Link>
                            </div>
                        </div>  
                        {currentUser?.role == 3? 
                            <>
                            {ticketSupport.statusTicketIssue?.id == 1? 
                            <div className='w-50 m-left' style={{ float: 'right' , display: 'flex'}}>
                                <div className='grid gap-2 w-40 m-right' style={{ marginRight: 20}}>
                                    <div>
                                        <Button onClick={() => updateStatus(3)} color={'primary'} size={'s'} className= 'red' isDelete={true} rounded="true">Reject</Button>
                                    </div>
                                    
                                </div>
                                <div className='grid gap-2 w-40 m-right'>
                                    <div>
                                        <Button onClick={() => { openPopUpStatus()}} size={'s'} className='navy' rounded="true">Accept</Button>
                                        <PopUp trigger={buttonPopUpStatus}>
                                        <div className='popup-inner-update-status-ticket'>
                                    <h6 style={{fontWeight:"normal"}}>Fix Days Length (days)</h6>
                                    <Input size="s" style={{ width: '460px', height:'35px'}}
                                    
                                        type="number"
                                        className="form-control"
                                        name="fixingDays"
                                        placeholder="Input Fixing Days"
                                        value={fixingDays}
                                        onChange={ (e) => onChangeEndDate(e) }

                                    />
    
                                     <br></br>
                                    <div style={{display:'flex'}}> 
                                         <Button type="button" className="navy spacingButtonRight" style={{width:"150px"}} size={'s'} rounded="true" onClick={() =>{ updateStatus(2); updateEndDate()}}>Confirm</Button> 
                                    
                                        <Button type="button" color={'primary'} style={{width:"150px"}} size={'s'} rounded="true" onClick={() => setButtonPopUpStatus(false)}>Cancel</Button>
                                    <br></br>
                                    </div>
                                    </div>
                                    </PopUp>
                                    </div>
                                </div> 
                                
                                
                            </div>
                            : null}
                            </>
                        : null}

                    </div>
                    <div style={{ margin: '80px 0px' }} />
                </div>
            
            </AppLayout>
        </>
    )
}

export default DetailTicketIssue
