import { useRouter } from "next/router"
export default function Custom503() {
    const router = useRouter()
    setTimeout(() => {
        router.push("/")
     }, 2000); //will call the function after 2 secs.
    return (
      <>
        <h4 className="m-auto" style={{ textAlign:'center', paddingTop:'20px'}}>503 - Internal Server Error</h4>
        <img width={"35%"} height={"35%"} className="m-auto"
            src={'/image/503 Error Service Unavailable-pana.svg'}
        />
        <h4 className="loading" style={{ textAlign:'center'}}>Redirecting</h4>
      </>
    )

  }
