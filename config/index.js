export const BACKEND_URL =
    process.env.NEXT_PUBLIC_BACKEND_URL || "http://dev.badr.co.id:8080/project-dashboard";
//checks to see if its hosted on another website or localhost:1337 (strapi)

export const NEXT_URL =
    process.env.NEXT_PUBLIC_FRONTEND_URL || "http://dev.badr.co.id:3000";

// provide holidayapi key
const HOLYDAY_API_KEY =  process.env.HOLYDAY_API_KEY || "99720384-8aa6-458e-bd4c-27906e1ab86e" // add at the env or add manually here
export const HOLIDAY_API_URL = `https://holidayapi.com/v1/holidays?pretty&country=ID&year=2021&key=${HOLYDAY_API_KEY}`;
