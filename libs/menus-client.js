const sampleIconUrl = 'https://badr.co.id/theme_badr/static/src/img/vision/goodness-icon.png'
const menusClient = [
  {
    url: '/projects',
    title: 'Home',
    Icon: () => (<img className={'inline my-auto'} width={18} height={18} src={'/icon/overview-white.svg'}/>)
  },
  {
    url: '/reports',
    title: 'Reports',
    Icon: () => (<img className={'inline my-auto'} width={18} height={18} src={'/icon/plan-white.svg'}/>)
  },
  {
    url: '/change-requests',
    title: 'Change Requests',
    Icon: () => (<img className={'inline my-auto'} width={18} height={18} src={'/icon/master-white.svg'}/>)
  },
  {
    url: '/ticket-support',
    title: 'Ticketing Support',
    Icon: () => (<img className={'inline my-auto'} width={18} height={18} src={'/icon/master-white.svg'}/>)
  }
]

export default menusClient
  // {
  //   url: '#',
  //   title: 'Master',
  //   Icon: () => (<img className={'inline my-auto'} width={18} height={18} src={'/icon/master-white.svg'}/>),
  //   isParent: true,
  //   child: [
  //     {
  //       url: '#',
  //       title: 'Child 1',
  //       Icon: () => (<i className={'fa fa-dashboard'} />)
  //     }
  //   ],
  // },
