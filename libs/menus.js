import AuthService from "../services/auth.service";

const sampleIconUrl = 'https://badr.co.id/theme_badr/static/src/img/vision/goodness-icon.png'
const user = AuthService.getCurrentUser();
const menus = [
  {
    url: '/',
    title: 'Home',
    Icon: () => (<img className={'inline my-auto'} width={18} height={18} src={'/icon/overview-white.svg'}/>)
  },
  {
    url: '/accounts',
    title: 'Accounts',
    Icon: () => (<img className={'inline my-auto'} width={18} height={18} src={'/icon/account-white.svg'}/>)
  },
  {
    url: '/projects',
    title: 'Projects',
    Icon: () => (<img className={'inline my-auto'} width={18} height={18} src={'/icon/plan-white.svg'}/>)
  },
  {
    url: '/reports',
    title: 'Reports',
    Icon: () => (<img className={'inline my-auto'} width={18} height={18} src={'/icon/plan-white.svg'}/>)
  },
  {
    url: '/change-requests',
    title: 'Change Requests',
    Icon: () => (<img className={'inline my-auto'} width={18} height={18} src={'/icon/master-white.svg'}/>)
  },
  {
    url: '/ticket-support',
    title: 'Ticketing Support',
    Icon: () => (<img className={'inline my-auto'} width={18} height={18} src={'/icon/master-white.svg'}/>)
  }
]

export default menus
  // {
  //   url: '#',
  //   title: 'Master',
  //   Icon: () => (<img className={'inline my-auto'} width={18} height={18} src={'/icon/master-white.svg'}/>),
  //   isParent: true,
  //   child: [
  //     {
  //       url: '#',
  //       title: 'Child 1',
  //       Icon: () => (<i className={'fa fa-dashboard'} />)
  //     }
  //   ],
  // },