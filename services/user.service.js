import axios from "axios";
import authHeader from "./auth-header";
import { BACKEND_URL } from "../config/index"
import { ApiError } from "next/dist/server/api-utils";

const API_URL = `${BACKEND_URL}/api/accounts`;

const getAllAccounts = () => {
    return axios.get(API_URL+"/list", { headers: authHeader()});
}

const getAllAdmins = () => {
    return axios.get(API_URL+"/list-admin", { headers: authHeader()});
}

const getAllProjectManagers = () => {
    return axios.get(API_URL+"/list-pm", { headers: authHeader() });
}

const getAllSponsors = () => {
    return axios.get(API_URL+"/list-sponsor", { headers: authHeader() });
}

const createAccount = (formData) => {
    return axios.post(
        API_URL + "/create",
        formData,
        { headers: authHeader() }
    )
}

const updateAccount = (formData) => {
    return axios.put(
        API_URL + "/" + id,
        formData,
        { headers: authHeader() }
    )
}

const deleteAccount = (id) => {
    console.log(id)
    return axios.delete(
        API_URL + "/" + id,
        { headers: authHeader() }
    )
}

const getAccount = (id) => {
    return axios.get(
        API_URL + "/" + id,
        {headers: authHeader()}
    )
}

const getAccountByEmail = (email) => {
    return axios.get(
        API_URL + "/" + email,
        {headers: authHeader()}
    )
}

const getNotifications = () => {
    return axios
        .get(`${API_URL}/notifications`, {headers: authHeader()})
}

const deleteNotifications = () => {
    return axios
        .delete(`${API_URL}/notifications`, {headers: authHeader()})
}

const deleteNotificationById = (id) => {
    return axios
        .delete(`${API_URL}/notifications/${id}`, {headers: authHeader(),})
}

export default {
    getAllAccounts,
    getAllAdmins,
    getAllProjectManagers,
    getAllSponsors,
    createAccount,
    updateAccount,
    deleteAccount,
    getAccount,
    getAccountByEmail,

    getNotifications,
    deleteNotifications,
    deleteNotificationById
}
