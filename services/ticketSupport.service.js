import axios from "axios";
import authHeader from "./auth-header";
import { BACKEND_URL } from "../config/index"

const API_URL = `${BACKEND_URL}/api/ticket-support`;

const getAllTicketIssue = () => {
    return axios.get(API_URL + "/list", { headers: authHeader() });
}

const getTicketIssue = (id) => {
    return axios.get(API_URL + "/" + id , { headers: authHeader() });
}

const uploadDocument = (id, formData) => {
    return axios.post(
        API_URL + `/${id}/upload-document`,
        formData,
        { headers: authHeader() });
}

const download = (id) => {
    return axios.post(`${BACKEND_URL}/api/file/download/file-ts/${id}`,
        { headers: authHeader() });
}

const createTicketIssue= (idProject, title, description, severityLevel) => {
    return axios.post(
        API_URL + `/create/${idProject}`,
        {title: title, description: description, severity_level : severityLevel},
        { headers: authHeader() }
    );
}

const updateStatus = (id, status) => {
    return axios.put(
        API_URL + `/update-status/${id}`,
        {status: status},
        { headers: authHeader() }
    );
}


const updateEndDate = (id, fixDays) => {
    return axios.put(
        API_URL + `/update-endDate/${id}`,
        {fixDays: fixDays},
        { headers: authHeader() }
    );
}

export default {
    uploadDocument,
    createTicketIssue,
    getAllTicketIssue,
    getTicketIssue,
    download,
    updateStatus,
    updateEndDate
}
