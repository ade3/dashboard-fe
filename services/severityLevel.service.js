import axios from "axios";
import authHeader from "./auth-header";
import { BACKEND_URL } from "../config/index"

const API_URL = `${BACKEND_URL}/api/severity-level/`;

const getAllSeverity = () => {

    return axios.get(API_URL, { headers: authHeader() });
}

export default {
    getAllSeverity,
}
