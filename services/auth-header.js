import AuthService from '../services/auth.service'

export default function authHeader() {
    const user = JSON.parse(localStorage.getItem('user'));
    return { Authorization: 'Bearer ' + user?.accessToken };
  }
