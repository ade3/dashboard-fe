import axios from "axios";
import authHeader from "./auth-header";
import { BACKEND_URL } from "../config/index"

const API_URL = `${BACKEND_URL}/api/roles`;

const getAllRoles = () => {
    return axios.get(API_URL, { headers: authHeader()});
}

export default {
    getAllRoles,
}