import axios from "axios";
import {BACKEND_URL} from "../config/index"
const API_URL = `${BACKEND_URL}/api/auth/`;

const login = (email, password) => {
    return axios
        .post(API_URL + "login", {
            email,
            password,
        })
        .then((response) => {
            if (response.data.result.accessToken) {
                localStorage.setItem("user", JSON.stringify(response.data.result));
            }
            return response.data.result;
        });
};

const logout = () => {
    localStorage.removeItem("user");
};

const getCurrentUser = () => {
    if (typeof window !== 'undefined') {
        return JSON.parse(localStorage.getItem("user"));
    }
};

const getUserByResetToken = (token) => {
    return axios
        .get(`${API_URL}reset-password?token=${token}`)
}

const requestResetPassword = (email) => {
    return axios
        .post(`${API_URL}request-reset-password`, null, {
            params: {
                email
            }
        })
};

const resetPassword = (token, password) => {
    return axios
        .post(`${API_URL}reset-password`, null, {
            params: {
                token,
                password
            }
        })
};

export default {
    login,
    logout,
    getCurrentUser,
    getUserByResetToken,
    requestResetPassword,
    resetPassword
};
