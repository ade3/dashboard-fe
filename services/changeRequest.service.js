import axios from "axios";
import authHeader from "./auth-header";
import { BACKEND_URL } from "../config/index"

const API_URL = `${BACKEND_URL}/api/change-requests`;

const getAllChangeRequests = () => {
    return axios.get(API_URL + "/list", { headers: authHeader() });
}

const getChangeRequest = (id) => {
    return axios.get(API_URL + "/" + id , { headers: authHeader() });
}

const uploadDocument = (id, formData) => {
    return axios.post(
        API_URL + `/${id}/upload-document`,
        formData,
        { headers: authHeader() });
}

const download = (id) => {
    return axios.post(`${BACKEND_URL}/api/file/download/file-cr/${id}`,
        { headers: authHeader() });
}

const createChangeRequest = (idProject, title, description) => {
    return axios.post(
        API_URL + `/create/${idProject}`,
        {title: title, description: description},
        { headers: authHeader() }
    );
}

const updateStatus = (id, status) => {
    return axios.put(
        API_URL + `/update-status/${id}`,
        {status: status},
        { headers: authHeader() }
    );
}
export default {
    uploadDocument,
    createChangeRequest,
    getAllChangeRequests,
    getChangeRequest,
    download,
    updateStatus
}
