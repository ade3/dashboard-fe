import axios from "axios";
import authHeader from "./auth-header";
import { BACKEND_URL } from "../config/index"

const API_URL = `${BACKEND_URL}/api/projects`;

const getAllProjects = () => {
    return axios.get(API_URL + "/list", { headers: authHeader() });
}

const getProject = (id) => {
    return axios.get(API_URL + "/" + id , { headers: authHeader() });
}

const getProjectByTitle = (title) => {
    console.log(title)
    return axios.get(
        API_URL + "/getByTitle/" + title,
        {headers: authHeader()}
    )
}

const updateImageProject = (id, formData) => {
    return axios.post(
        API_URL + `/${id}/upload-image`,
        formData,
        { headers: authHeader() });
}

const createProject = (formData) => {
    return axios.post(
        API_URL + "/create",
        formData,
        { headers: authHeader() }
    )
}

const updateDeliverables = (formData) => {
    return axios.post(
        API_URL + "/update-deliverables",
        formData,
        { headers: authHeader() }
    )
}

const updateNotes = (formData) => {
    return axios.post(
        API_URL + "/update-brand-promise-notes",
        formData,
        { headers: authHeader() }
    )
}

const updateProject = (id, formData) => {
    return axios.put(
        API_URL + "/" + id,
        formData,
        { headers: authHeader() }
    )
}

const deleteProject = (id) => {
    return axios.delete(
        API_URL + "/" + id,
        { headers: authHeader() }
    )
}

const getSummaryAllProjects = () => {
    return axios.get(
        `${BACKEND_URL}/api/analytic`,
        { headers: authHeader() }
    )
}

export default {
    getAllProjects,
    getProject,
    updateImageProject,
    createProject,
    updateProject,
    updateNotes,
    deleteProject,
    updateDeliverables,
    getSummaryAllProjects,
    getProjectByTitle
}

