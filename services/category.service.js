import axios from "axios";
import authHeader from "./auth-header";
import { BACKEND_URL } from "../config/index"

const API_URL = `${BACKEND_URL}/api/categories/`;

const getAllCategories = () => {
    return axios.get(API_URL+"list", { headers: authHeader() });
}

export default {
    getAllCategories,
}
