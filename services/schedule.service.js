import axios from "axios";
import authHeader from "./auth-header";
import { BACKEND_URL, HOLIDAY_API_URL } from "../config/index"

const API_URL = `${BACKEND_URL}/api/projects`;

const getAllPublicIndonesiaHolidays = () => {
    return axios.get(
        "https://api-harilibur.vercel.app/api"
    );
}

const updateScheduleProjectById = (id, formData) => {
    return axios.put(
        API_URL + "/" + id + "/schedule",
        formData,
        {headers: authHeader()}
    )
}

const getAllHolidays = () => {
    return axios.get(`${BACKEND_URL}/api/holidays`, { headers: authHeader() });
}

const postHoliday = (formData) => {
    return axios.post(
        `${BACKEND_URL}/api/holidays`,
        formData,
        {headers: authHeader()}
    )
}

const deleteHolidays = (id) => {
    return axios.delete(
        `${BACKEND_URL}/api/holidays/${id}`,
        {headers: authHeader()}
    )
}

const getAllEventsMaster = () => {
    return axios.get(
        `${BACKEND_URL}/api/events`,
        {headers: authHeader()}
    )

}

export default {
    getAllEventsMaster,
    updateScheduleProjectById,
    getAllHolidays,
    postHoliday,
    deleteHolidays,
    getAllPublicIndonesiaHolidays,

}

