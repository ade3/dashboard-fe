import axios from "axios";
import authHeader from "./auth-header";
import { BACKEND_URL } from "../config/index"

const API_URL = `${BACKEND_URL}/api/report-issues`;

const getAllReportIssues = () => {
    return axios.get(API_URL + "/list", { headers: authHeader() });
}

const getReportIssue = (id) => {
    return axios.get(API_URL + "/" + id , { headers: authHeader() });
}

const uploadDocument = (id, formData) => {
    return axios.post(
        API_URL + `/${id}/upload-document`,
        formData,
        { headers: authHeader() });
}

const download = (id) => {
    return axios.post(`${BACKEND_URL}/api/file/download/file-ri/${id}`,
        { headers: authHeader() });
}

const createReportIssue = (idProject, title, description) => {
    return axios.post(
        API_URL + `/create/${idProject}`,
        {title: title, description: description},
        { headers: authHeader() }
    );
}

const updateStatus = (id, status) => {
    return axios.put(
        API_URL + `/update-status/${id}`,
        {status: status},
        { headers: authHeader() }
    );
}
export default {
    uploadDocument,
    createReportIssue,
    getAllReportIssues,
    getReportIssue,
    download,
    updateStatus
}