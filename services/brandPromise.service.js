import axios from "axios";
import authHeader from "./auth-header";
import { BACKEND_URL } from "../config/index"

const API_URL = `${BACKEND_URL}/api/brand-promises/`;

const getAllBrandPromises = () => {

    return axios.get(API_URL+"list", { headers: authHeader() });
}

const getBpByProject = (id) => {

    return axios.get(API_URL + "get-by-project/" + id, { headers: authHeader() });
}

export default {
    getAllBrandPromises,
    getBpByProject
}
