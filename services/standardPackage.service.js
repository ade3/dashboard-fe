import axios from "axios";
import authHeader from "./auth-header";
import { BACKEND_URL } from "../config/index"

const API_URL = `${BACKEND_URL}/api/standard-packages`;

const getPackagesByBrandPromiseId = (id) => {
    return axios.get(API_URL+"/" + id, { headers: authHeader() });
}


export default {
    getPackagesByBrandPromiseId
}
