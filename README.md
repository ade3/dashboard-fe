This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.

## Development Guideline

### 1. Import Guide
#### Urutan import
- External package
- New line
- Internal package : component
- Internal package : service

Contoh
```javascript
import Head from 'next/head'
import Link from 'next/link'
import * as React from "react";
import {useEffect, useState} from "react";
import {useRouter} from "next/router"
import {DataGrid} from '@mui/x-data-grid';

import AppLayout from '../../components/Layout/App'
import Button from '../../components/Button'
import ProjectService from '../../services/project.service'
import AuthService from '../../services/auth.service'
```

### 2. Redirect
#### Get Resource
Apabila terdapat internal server error (e.g. network error), maka page redirect ke `/503`.
Selain itu, resource not found redirect ke `/404`
```javascript
(error) => {
                if (error.response == undefined) {
                    router.push('/503')
                }
                router.push('/404')
            }
```

### 3. Snackbar Message
Snackbar message digunakan saat ingin menampilkan notifikasi setelah hit API. Severity terdiri dari `success, error, warning, info`

Contoh
```javascript
// import
import {Alert, Snackbar} from "@mui/material";

// 1. Inisiasi state
const [openMessage, setOpenMessage] = useState(false);
const [message, setMessage] = useState({
        value: "",
        severity: "" 
    });
const handleCloseMessage = (event, reason) => {
    if (reason === 'clickaway') {
        return;
    }
    setOpenMessage(false);
};

// 2. set state message
setMessage({
    value: "deskripsi message",
    severity: "success"
})
setOpenMessage(true)

// 3. Render
return (
    {(message.value != "") && (
        <Snackbar
            anchorOrigin={{vertical: 'top', horizontal: 'center'}}
            open={openMessage}
            onClose={handleCloseMessage}
            autoHideDuration={2000}>
            <Alert onClose={handleCloseMessage} variant="filled" severity={message.severity}>
                {message.value}
            </Alert>
        </Snackbar>
    )}
)
```

### 4. Message in row
Digunakan saat ingin menampilkan notifikasi setelah hit API. Perbedaan dengan snackbar message adalah notifikasi ini ditampilkan pada baris halaman.
Severity terdiri dari `success, error, warning, info`

Contoh
```javascript
// import
import {Alert} from "@mui/material";

// 1. Inisiasi state
const [message, setMessage] = useState({
        value: "",
        severity: "" // bisa terdiri dari error, warning, info, atau success 
    });

// 2. set state message
setMessage({
    value: "deskripsi message",
    severity: "success"
})

// 3. Render
return (
    {(message.value != "") && (
        <Alert severity={message.severity}>{message.value}</Alert>
    )}
)
```









