import { useEffect } from "react";
import { withRouter } from "next/router";
import AuthService from "../services/auth.service";

const parseJwt = (token) => {
  try {
    return JSON.parse(atob(token.split('.')[1]));
  } catch (e) {
    return null;
  }
};
function AuthVerify(props) {
    useEffect(() => {
      const user = AuthService.getCurrentUser();
      if (user) {
        const decodedJwt = parseJwt(user.accessToken);
        if (decodedJwt.exp * 1000 < Date.now()) {
          props.logOut();
        }
      }
    });
    return <div></div>;
}

export default withRouter(AuthVerify);
